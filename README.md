### **Description**

Web page for presenting public parts of my personal GitLab.  
Data are prepared by my other GitLab project [https://gp.znoj.cz/Znoj-WEB/gitlabprojects](https://gp.znoj.cz/Znoj-WEB/gitlabprojects) (link on GitLab: [https://gitlab.com/Znoj-Desktop/pythongitlabscript](https://gitlab.com/Znoj-Desktop/pythongitlabscript)).

CLI: build and deploy to Firebase.

---

### **Link**

served with Firebase:  
[https://gp.znoj.cz](https://gp.znoj.cz)  
[https://gp3.znoj.cz](https://gp3.znoj.cz)
[https://gitlabprojects-705b8.firebaseapp.com/](https://gitlabprojects-705b8.firebaseapp.com/)

served with Netfly:  
[https://flamboyant-albattani-bc57c7.netlify.com](https://flamboyant-albattani-bc57c7.netlify.com)  
[https://gp2.znoj.cz](https://gp2.znoj.cz)

---

### **Technology**

JavaScript, Ant Design, React, Firebase, GitLab CI/CD, CSS

---

### **Year**

2019

---

### **Screenshots**

![](./README/main.png)

![](README/1.png)

![](README/2.png)

![](README/3.png)

![](README/4.png)

![](README/5.png)

![](README/6.png)

![](README/7.png)

![](README/8.png)

![](README/9.png)
