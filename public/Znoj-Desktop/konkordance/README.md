### **Description**

Konkordance (z latinského concordia = "shoda", "svornost") je shoda určitých sledovaných znaků. V literární vědě, zejména biblistice, se tak zpravidla označují i knihy obsahující seznam nebo soupis věcných nebo slovních podobností v různých částech knihy nebo různých knihách. Obvykle jsou konkordance slov sepisovány jako slovníky, tj. podle výrazů v abecedním pořádku. U každého z nich je udáno, kde jej v knize lze nalézt. Vaším úkolem bude takovou konkordanci sestavit. Jinak řečeno pro zadaný textový soubor sestavíte seznam slov a ke každému slovu vypíšete seznam řádků na kterých se toto slovo vyskytuje. Slova jsou v konkordanci setříděna podle abecedy, řádky s výskyty jsou uváděny vzestupně. Vstupní text budete číst z textového souboru, výslednou konkordanci vypíšete opět do textového souboru.

---

### **Technology**

C++

---

### **Year**

2012

---

### **Implementation**

- Vstupní text je v angličtině, znaky s diakritikou se nebudou vyskytovat.
- Slovo je definováno jako souvislá posloupnost znaků z množiny {a,...,z,A,...,Z}.
- Písmena ve slovech je nutné převést na malá, lze využít funkci tolower z hlavičkového souboru ctype.h, viz zde <http://msdn.microsoft.com/en-us/library/8h19t214.aspx> .
- Číslování řádků začíná od 0.
- Pokud se slovo vyskytuje na jednom řádku vícekrát, počítá se jen jeden výskyt.
- Jméno vstupního textového souboru zadáte z klávesnice. Jméno výstupního textového souboru zadáte z klávesnice.
- Ze zadání je patrné, že prvním problémem je implementace algoritmu, který bude postupně číst znaky z textového souboru, převádět je na malá písmena a sestavovat z nich slova. Pokud načte znak konce řádku, zvýší počítadlo řádku o jedna. Druhou částí programu bude vhodná datová struktura, kde se budou uchovávat nalezená slova a ke každému slovu seznam řádků kde se dané slovo vyskytuje. Zároveò je tu požadavek vypsat slova setříděná podle abecedy. Tomuto požadavku vyhovuje binární vyhledávací strom, kde v uzlu stromu bude uloženo slovo z konkordance a, nejlépe, objekt reprezentující seznam řádků s výskytem slova. Seznam (implementováno jako spojový seznam tj. s využitím pointerů) řádků má být, pro dané slovo, vypsán vzestupně. Protože jsou řádky číslovány vzestupně, stačí nové řádky do příslušných seznamů vkládat nakonec { tím máme rovnou zajištěno, že se seznam bude udržovat setříděný, tak jak je specifikováno v zadání.
- Pro čtení a zápis do souboru můžete použít třídy ifstream (čtení) a ofstream (zápis) z hlavičkového souboru fstream.

---

### **Screenshot**

<div align="center">
  <img src="./README/console.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">console</img>
</div>

Algorithm:  
<div align="center">
  <img src="./README/algorithm.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">console</img>
</div>

---

### **Presentations**  

<div align="center">
  <img src="./README/zno0011_algIII_5.pptx" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">presentation pptx</img>
</div>
  
<div align="center">
  <img src="./README/zno0011_algIII_5.pdf" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">presentation pdf</img>
</div>

###

---
**Example**

#### input

computer monitor.  
computer, mouse; mouse  
mouse monitor. computer  

#### output

computer - 0, 1, 2  
monitor - 0, 2  
mouse - 1, 2  
