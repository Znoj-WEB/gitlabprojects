### **Description**

In combinatorial mathematics, the Catalan numbers form a sequence of natural numbers that occur in various counting problems, often involving recursively-defined objects.

1) Develop a recursive function that allows the calculation Catalan(n)
2) Record and analyze the number of multiplications carried out by recursive algorithm
3) Do a formal analysis of the complexity of recursive algorithm.
4) Compare the results obtained in the previous two tasks.
5) Develop a repeating function to calculate Catalan(n) using the dynamic programming.
6) Record and analyze the number of multiplications carried out by non-recursive algorithm
7) Do a formal analysis of complexity of non-recursive algorithm.
8) Compare the results obtained in the previous two tasks.
9) Compare the number of multiplications incurred by recursive and non-recursive algorithms.

---

### **Technology**

C

---

### **Year**

2014

---

### **Implementation**

#### **Recursive algorithm**

```
int M_Rec = 0; 
int CatalanR(int n){
 if(n == 0){
  return 1;
 }
 int sum = 0;
 int i;
 for(i=0; i<n; i++){
  M_Rec++;
  sum += (CatalanR(i)*CatalanR(n-i-1));
 }
 return sum;
}
```

#### **Non-recursive algorithm**

```
int M_D = 0; 
int CatalanD(int n){
 int i = 0;
 int j = 0; 
 int array[n+1];
 for(i=0; i<n+1; i++){
  array[i] = 0; 
 }
 array[0] = 1;
 for(i=1; i<n+1; i++){
  for(j=0; j<i; j++){
   M_D++;
   array[i]+=array[j]*array[i-j-1];
  }
 }
 return array[n];
}
```

---

### **Explanation of the dynamic programming strategy**

The main idea is, that instead of computing previous results again and again, the algorithm saves every result into the field. So basically this algorithm computes every number just one time and if is needed (for calculating bigger Catalan number), than instead of computing Catalan(x) is the result of Catalan(x) calling from the array.

---

### **Table with comparison performance of the two implemented algorithms**

<div align="center">
  <img src="./README/table.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Analysis of complexity → expressions number of multiplications**

<div align="center">
  <img src="./README/complexity.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Screenshot**

<div align="center">
  <img src="./README/programScreen.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Report**  

<div align="center">
  <img src="./README/Report.docx" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">Report docx</img>
</div>
  
<div align="center">
  <img src="./README/Report.pdf" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">Report pdf</img>
</div>
