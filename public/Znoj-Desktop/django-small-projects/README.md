### **Description**

Django small projects

---

### **Technology**

Python

---

### **Year**

2012

---

## **Léčebna**

### **Diagram**

<div align="center">
  <img src="./README/lecebnaDiagram.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Tables**

<div align="center">
  <img src="./README/lecebnaTabulky.jpg" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

## **Liga mistrů**

### **Diagram**

<div align="center">
  <img src="./README/ligaDiagram.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Tables**

<div align="center">
  <img src="./README/ligaTabulky.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/ligaTabulkyAtributy.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

## **Nemocnice**

---

## **Sklad knih**

---

## **Zápasy**

---

### **Diagram**

<div align="center">
  <img src="./README/zapasyDiagram.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Tables**

<div align="center">
  <img src="./README/zapasyTabulky.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

## **Zkoušky**

---

### **Tables**

<div align="center">
  <img src="./README/zkouskyTabulky.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---
