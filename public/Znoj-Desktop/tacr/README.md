### **Description**

Implementign WebAPI and Notification for TACR (Technology Agency of the Czech
Republic) project during 816 working hours.  

---

### **Technology**

C#, ASP.NET

---

### **Year**

2017  

---

### **Work**

#### 2017/01 (64 hours)  

Seznámení se s projektem, týmem, technologiemi s programem Postman, s LDAP AD a
jeho správou v prostředí Windows server), nastavení pracovního prostředí
(VS, IIS, SQL db, VPN), celkové rozjetí projektu.  

#### 2017/02 (64 hours)  

Seznámení se s Gitem na TFS v prostředí VS, zorientování se ve zdrojovém kódu
(.NET, architektura), prozkoumání aktuálního stavu dotazů v programu Postman,
funkčnosti stávajících metod API, seznamování se se strukturou uživatelů AD,
doimplemetnace některých načrtnutých metod, označení funkčních a nefunkčních.  

#### 2017/03 (64 hours)  

Práce na API pro správu uživatelů (vrácení všech uživatelů, vrácení uživatele
podle ID, podle jména, vrácení detailů současného uživatele, přejmenování
uživatele volajícího tuto metodu, přejmenování uživatele zadaného id,
skrytí / obnovení uživatele, mazání uživatele, dotaz na stav zámku uživatele,
nastavení typu sítě uživatele, zobrazení typu sítě uživatele).  

#### 2017/04 (64 hours)  

Práce na API skupin - kompletní návrh architektury, práce se skupinami,
vytvoření omezení pro operace nad skupinami, implementace metod (vrácení skupin
daného uživatele, vrácení všech skupin, zobrazení detailů skupiny podle ID,
zobrazení detailů skupiny podle zadaného jména, vytvoření skupiny,
smazání skupiny, přidání uživatel do skupiny a odebírání uživatel ze skupiny),
kontrola a doplnění dokumentace k doposud vyhotovenému API a jeho otestování.  

#### 2017/05 (64 hours)  

Práce na API (uživatelé) - vytvoření uživatele, odemknutí uživaele, reset hesla
uživatele, příprava metody pro aktualizaci informací o uživateli. práce na API
(push notifikace) - změna metody pro notifikaci uživatele, vytvoření nových
metod pro registraci zařízení, odebrání konkrétního zařízení, odebrání všech
zařízení, zobrazení detailů vybraného zařízení. Práce na adaptaci API na nový
poloprodukční server CHIMERA. Četná emailová komunikace s panem Konečným,
úprava API dle potřeb aplikace a doplnění informací do online dokumentace.  

#### 2017/06 (64 hours)  

Práce na verzování skupin a update verze při jakékoli změně skupin,
jejích členů, update všech skupin ve kterých je uživatel při jakékoli jeho
změně. Krom vlastníků (autorů) skupin může všechny operace se skupinami
provádět i administrátor (úprava všech souvisejících metod). Vytvořena
metoda pro detekci administrátora. Otestování kompletní sady API na serveru
CHIMÉRA. změna API pro atribut pushtacr, aby umožňoval uložení i typu zařízení.
S tím související změna třídy UserMobileDevice a veškeré metody pracující
s touto třídou byly upraveny. Systém notifikací je nyní připraven i na jiné
zařízení než s iOS.  

#### 2017/07 (64 hours)  

Práce s mySQL db, přepis konfiguračních parametrů do XML Web.config pro snažší
migraci na jiný server. Sepsání dokumentace k parametrům, které je třeba
nastavit při nasazení na jiný server. Nastudování problematiky, výběr vhodného
řešení a implementace triggeru pro automatické notifikace pouze takového
uživatele, který je offline.  

#### 2017/08 (64 hours)  

Implementace a otestování metod pro práci s veřejným klíčem:
/api/adusers/getUserPublicKeyGet, /api/adusers/getUserPublicKey/{id},
/api/adusers/updateUserPublicKeyUpdate. Implementace a otestování metod
pro práci s tokenem k VoIP komunikaci:
/api/PushNotification/registerVoipTokenRegisters,
/api/PushNotification/removeVoipToken/{voipToken},
/api/PushNotification/removeAllVoipTokens, /api/PushNotification/getVoipTokens,
parametrizace a vytvoření dokumentace k MySQL triggeru.  

#### 2017/09 (64 hours)  

Aktualizace metody vracející všechny uživatele pro potřeby CodeCreateru.
Aktualizace metody pro automatickou aktualizaci, testování aplikace na více
zařízeních současně. Přidáno datum k UserMobileDevices a UserVoipDevices,
úprava metod pro správu (voip, device) tokenů.  

#### 2017/10 (80 hours)  

Mazání tokenů (starších 31 dnů). Kontrola probíhá pouze tehdy, pokud je přidáno
2. a další zařízení. Intenzivní testování aplikace (především notifikací),
oprava notifikací a optimalizace (přepsání) kódu. Aktualizována metoda pro
vytvoření uživatele v případě, že uživatel již existuje. Sbírání a předání
informací firmě Satturn.  

#### 2017/11 (80 hours)  

Restrukturalizace správy notifikací a přesun do odděleného projektu z důvodu
uspávání serveru při nečinnosti, testování API, studium APNS pro Android - náčrt
řešení, rozšíření struktury API o sekci "config", vytvořeno API pro ukládání a
zobrazování konfigurace serveru (Get api/config Post api/config). Úprava
notifikací aby zobrazovaly jméno odesílatele zprávy + titulek podle AD.  

#### 2017/12 (80 hours)  

Vytvoření metody pro VoIP notifikace, zapracování certifikátu pro VoIP do
konfigurace a na server, aktualizace certifikátu pro PUSH notifikace.
Implementace API notifyUserVoIP/{id} pro vyvolání VoIP notifikace. Nahrazení
VoIP notifikací PUSH notifikacemi pro potřeby demonstrace aplikace,
troubleshooting, nahrazení VoIP certifikátu, úprava konfigurace, fix chyby o
nevalidním certifikátu a zpět nahrazení PUSH certifikátu novým VoIP. Průzkum
VoIP knihoven pro android, vyhledání examplů a doporučení knihoven + sepsání
návod testování pro firmu Satturn. Rzšíření VoIP push notifikací o jméno
volajícího + otestování.  

---

### **Documentation**

## WebAPI  

Web API is fully secured with JWT (OAuth) tokens. It means that access to the
API is secured by authentication token. Users will receive the token if contact
Authentication server with a valid user credentials stored in Active Directory.
For every request it is necessary put in valid access token to the HTTP
Authorization header. There is a limitation of validity of the token set
to 60 minutes.  

All of this functionality is provided by second developed application called
VSB.TACR.OAuthServer (STS server). It is connected to the Active Directory.
The WebAPI provides bunch of function placed into 10 categories. These
functions could be found in online documentation.  

<div align="center">
  <img src="./README/webAPI.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/webAPI2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Along with an online documentation postman’s dump file is provided for testing
purposes (with testing data).  
<div align="center">
  <img src="./README/docPostman.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
For method with writing rights, only administrator (user from group
Administrators) or user just for itself could change data.  
Except of Users category, also Groups category were implemented, so it is
possible to do many operations with group. There are also rights to write only
for administrator or group owner (the one who created the group).  
Groups has for example version number, so client only needs to compare version
of group to find out, if group was changed.  
Another category of APIs is “Config”. There are methods for reading or changing
server’s (XMPP, Asterisk, API) IP and ports.  

We can demonstrate the following basic methods:  

- Method `GET /api/ADUsers` - it returns a list of all users and their allowed
attributes listed in Active Directory. It returns this data only to users
placed in a particular organizational unit.  

```
[
  {
    "id": "string",
    "name": "string",
    "password": "string",
    "displayName": "string",
    "givenName": "string",
    "sn": "string",
    "thumbnailPhoto": "string",
    "userPrincipalName": "string",
    "typeofmobilenetwork": "string",
    "codepage": "string",
    "objectcategory": "string",
    "dscorepropagationdata": "string",
    "usnchanged": "string",
    "instancetype": "string",
    "logoncount": "string",
    "badpasswordtime": "string",
    "pwdlastset": "string",
    "objectclass": "string",
    "badpwdcount": "string",
    "samaccounttype": "string",
    "lastlogontimestamp": "string",
    "usncreated": "string",
    "objectGuid": "string",
    "memberOf": [
      "string"
    ],
    "whencreated": "string",
    "useraccountcontrol": "string",
    "cn": "string",
    "countrycode": "string",
    "primarygroupid": "string",
    "whenchanged": "string",
    "lastlogon": "string",
    "distinguishedname": "string",
    "samaccountname": "string",
    "objectsid": "string",
    "lastlogoff": "string",
    "accountexpires": "2018-01-15T15:03:55.280Z",
    "userprincipalname": "string",
    "path": "string",
    "dn": "string",
    "mobileDeviceTokens": [
      "string"
    ],
    "pushtacrvoip": [
      "string"
    ],
    "userPublicKey": "string"
  }
] 
```

<div align="center">
  <img src="./README/webAPIdetail.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
- GET `/api/ADUsers/{id}` – it returns the attributes of a particular user
identified via the parameter "id". Result is similar like in previous method
but only for specific user.  
- POST `/api/PushNotification/registerDevice` - With this method, it is possible
to put into the Active Directory token of mobile applications. This is very
important because we have to deal with Apple Push Notification Server or Google
Cloud Messaging. We are using it for contact user’s application when application
is in idle mode. If we deliver the token to the Apple or Google server, the user
will be notified, and he can start application which update data content from
the server side.
- GET `/api/PushNotification/notifyUser/{id}` - with this method we can notify a
specific user. User’s tokens are stored in Active Directory, but it is not
necessary to know that. Application will send only users id in format
(listed though previous method):  
`"id": "0eb2c726-af84-468a-b01a-bc529f7b842c"`  
In case of calling this method, the Apple Push Notification Server or Google
Cloud Messaging will send notification to the user devices.  

## Notifications  

Every user has 2 types of tokens. Tokens for messaging and tokens for VoIP
calls. Tokens are periodically send to the server from the client side and
controlled on the server side. Old unused tokens are removed from the server
for security purposes.  

<div align="center">
  <img src="./README/screenshot.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Notifications for messages are send automatically from the server. On the
server side, MySQL database (filled by XMPP) is periodically controlled and
when new unreceived, unread message appeared, notification is send to all
user’s registered devices.  

<div align="center">
  <img src="./README/Tacr_2017_znoj_cz.docx" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">TACR doc in cz</img>
</div>
  
<div align="center">
  <img src="./README/Tacr_2017_znoj_en.docx" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">TACR doc in en</img>
</div>
  
<div align="center">
  <img src="./README/Dokumentace_zno0011_WebAPI.docx" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">What has to be set docx</img>
</div>
  
<div align="center">
  <img src="./README/Dokumentace_zno0011_WebAPI.pdf" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">What has to be set pdf</img>
</div>
  
---

### **Screenshots**

<div align="center">
  <img src="./README/postman.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/postman2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  