### **Description**

Scripts for download, update and modified all my gitlab groups and projects.
Scripts for creating public content from gitlab projects to be presented in my personal web site.  

Install:  
`pip3 install gitpython`
`pip3 install requests`
Setup:  

- put private token into `config.py`  
Start with command:  
`python3 run.py`  
with result:  

---
########################### options ###########################  
  
Choose one of following options:  
  
0. 'help' or 'h' or '--help' or '-h' or '0'  
1. 'create' or 'json' or '1'  
2. 'clone' or 'pull' or '2'  
3. 'commit' or '3'  
4. 'readme' or '4'  
5. 'edit' or 'editImages' or 'images' or '5'  
6. 'copy' or '6'  
7. 'backup' or '7'  
8. 'exit' or 'q' or '8'  
  
########################### options ###########################  
---

0. 'help' or 'h' or '--help' or '-h' or '0'  
Print help menu with description.  

1. 'create' or 'json' or '1'  
Create file 'groups.json' with all groups (and details) matching 'urlGroups' value in 'config.py'.  
Create file for every group from 'groups.json' except groups in 'skipGroups' set in 'config.py' with all containing projects (max 100).  

2. 'clone' or 'pull' or '2'  
For all projects and groups except those mentioned in 'skipGroups' variable or 'skipProjects' variable sets in 'config.py', clone repository when not exists or pull if git repository already exists in structure begin with folder 'repoRootPath' set in 'config.py', inside this folder is folder with group name of this project and inside this folder is one with the name of this project containing repository.  

3. 'commit' or '3'  
For all projects inside folder set in variable 'repoRootPath' in 'config.py', pull all changes from server, detect local changes and commit changes with 'commitMessage' set in 'config.py'.  

4. 'readme' or '4'  
For all projects inside folder set in variable 'repoRootPath' in 'config.py' having readme file with name defined in 'config.py' as 'readmeFileName', create folder with name as value of variable 'readmeFolderName' in 'config.py' and copy all files mentioned in 'readmeFileName' into this folder, rename all these files to point into 'readmeFolderName' and copy icon with name 'iconName' defined in 'config.py' if exists and not copied yet.  

5. 'edit' or 'editImages' or 'images' or '5'  
Edit 'readmeFileName' from all projects inside folder set in variable 'repoRootPath' in 'config.py' to set max size off all images and align it to center.  

6. 'copy' or '6'  
Copy 'readmeFolderName' and 'readmeFileName' from all projects inside folder set in variable 'repoRootPath' in 'config.py' to new folder defined as 'newPublicDir' in 'config.py'.  
All group config files are slightly modified and imported into global file 'groups.js'. This file is exported as config js file to 'newConfigDir'.  

7. 'backup' or '7'
Copy projects from 'repoRootPath' folder to 'newBackupDir' folder.  
All folders or files match pattern in 'backupFoldersToSkip' will be skipped.  
I don't need to backup whole GIT history and 'npm_modules' folders, so I am using this script.  

8. 'exit' or 'q' or '8'  
Exit program, can be used only after printing menu as well as other options.  

---

### **Technology**

Python

---

### **Year**

2019

---

### **Screenshots**

<div align="center">
  <img src="./README/menu.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

'help' or 'h' or '--help' or '-h' or '0'  
<div align="center">
  <img src="./README/menuDesc.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
'create' or 'json' or '1'  
<div align="center">
  <img src="./README/create.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

'clone' or 'pull' or '2'  
<div align="center">
  <img src="./README/clone.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
'commit' or '3'  
<div align="center">
  <img src="./README/commit.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
'edit' or 'editImages' or 'images' or '5'  
<div align="center">
  <img src="./README/edit.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
'copy' or '6'  
<div align="center">
  <img src="./README/copy.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
'backup' or '7'  
<div align="center">
  <img src="./README/backup.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  