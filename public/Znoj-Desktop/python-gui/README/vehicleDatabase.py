﻿#------------------------------------------------------------------------------#
#------Projekt Databaze vozidel - ZNO0011                                   #
#------------------------------------------------------------------------------#
# -*- coding: utf-8 -*- 
from Tkinter import *
import datetime
import Tix
import MultiListbox as table

class myApp:

    def novy_okno_app(self):
        about = Toplevel()
        about.title('O aplikaci')
        self.fram = Frame(about)
        self.fram.pack()
        Message(self.fram, text=u"Uživatelská rozhraní", width=200).pack()
        Message(self.fram, text=u"Databáze vozidel", width=200).pack()
        Message(self.fram, text=u"letní semestr", width=200).pack()
        Message(self.fram, text=u"2013/2014", width=200).pack()
        Message(self.fram, text=u"ZNO0011", width=200).pack()
        Button(self.fram, text="Konec", command=about.destroy).pack(side=RIGHT)

    def novy_okno_author(self):
        about2 = Toplevel()
        about2.title('O autorovi')
        self.fram2 = Frame(about2)
        self.fram2.pack()
        Message(self.fram2, text=u"email: jiri.znoj.st@vsb.cz", width=200).pack()
        Message(self.fram2, text=u"Jméno: Jiří Znoj", width=200).pack()
        Message(self.fram2, text=u"login: ZNO0011", width=200).pack()
        Message(self.fram2, text=u"ročník: 2.", width=200).pack()
        Button(self.fram2, text="Konec", command=about2.destroy).pack(side=RIGHT)

    def novy_okno_verze(self):
        about3 = Toplevel()
        about3.title('Verze')
        self.fram3 = Frame(about3)
        self.fram3.pack()
        Message(self.fram3, text=u"Verze: 1.0a", width=200).pack()
        Message(self.fram3, text=u"datum: 31. 3. 2014", width=200).pack()
        Button(self.fram3, text="Konec", command=about3.destroy).pack(side=RIGHT)
        
    def databaze_vozidel(self):
        #v konstruktoru mlb se posila tuple obsahujici tuple o 2 polozkach - prvni ja nazev sloupce, druha je minimalni sirka (v poctu pismen)
        self.mlb = table.MultiListbox(self.db_frame, (('Vozidlo', 1), ('Karoserie', 1), ('Objem', 1), ('Výkon', 1), ('Rok vyroby', 1), ('Počet dveří', 1), ('Palivo', 1), ('Barva', 1), ('Poznamky', 1)))
        for i in range(len(self.pole_db)):
            #prirazeni jednotlivych polozek pole do patricnych sloupcu tak aby to korespondovalo s navrhem
            self.mlb.insert(END, (self.pole_db[i][0], self.pole_db[i][1], self.pole_db[i][5], self.pole_db[i][6], self.pole_db[i][4], self.pole_db[i][3], self.pole_db[i][2], self.pole_db[i][16], self.pole_db[i][22]))
        #expand - prvek vyuzije volne misto ktere mu rodic/nadrazeny prvek/velikost okna poskytne
        #fill - prvek se bdue roztahovat do stran i nahoru a dolu (fill=X jen do stran, fill=Y jen nahoru a dolu)
        #padx=2 - okraj 2 (tusim px) vlevo i vpravo
        #pady - okraj nahore i dole
        self.mlb.pack(expand=YES,fill=BOTH, padx=2, pady=2)
        #vola se funkce radio_vyber_zaznam_db(row), row je cislo aktualne vybraneho radku
        self.mlb.subscribe( lambda row: self.radio_vyber_zaznam_db(row) ) 
    
    def vymaz_zakladni(self):
        #vlozi prazdny retezec
        self.var_vyrobce.set('')
        self.var_karoserie.set('')
        self.var_palivo.set('')
        #smaze textbox od 0. znaku po konec -> takze smaze textbox
        self.dvere.delete(0, END)
        self.rok_vyroby.delete(0, END)
        self.objem.delete(0, END)
        self.vykon.delete(0, END)
        #vse ok - smaze pripadnou chybovou hlasku a nahradi ji mezerou
        self.var_sb.set(' ')
    
    def vymaz_rozsirene(self):
        #pri ladeni jsem pouzival print ___ pro vypis do konzole
        #print self.var_abs.get()
        #metoda deselect() zavolana na checkbuttonu "jej prepne na false"
		self.abs.deselect()
		self.airbag.deselect()
		self.autoalarm.deselect()
		self.prevodovka.deselect()
		self.autoradio.deselect()
		self.central.deselect()
		self.ovladani_oken_z.deselect()
		self.ovladani_oken_p.deselect()
		self.klimatizace.deselect()
		self.barva.delete(0, END)
		self.posilovac.deselect()
		self.protiprokluzovy_system.deselect()
		self.stresni_okno.deselect()
		self.tazne_zarizeni.deselect()
		self.xenony.deselect()
        #vse ok
		self.var_sb.set(' ')

    def vymaz_poznamku(self):
        self.poznamky.delete(1.0, END)
        #vse ok
        self.var_sb.set('')
        
    def vymaz_zaznam(self):
        self.vymaz_poznamku()
        try:
            #smazani poznamky z pole
            self.pole_db.pop(int(self.mlb.curselection()[0]))
            #smazani poznamky z mlb (MultiListBoxu)
            #self.mlb.curselection() je prave vybrany zaznam v mlb - prace s mlb podle prikladu z http://www.cs.vsb.cz/navratil/uro/
            self.mlb.delete(self.mlb.curselection()[0],self.mlb.curselection()[0])
        except Exception:
            print 'Neni oznaceno nic co by slo smazat'
            #promenna.set -> nastavuji text v teto promenne, ktera je pak spojena se status barem
            self.var_sb.set('Neni oznaceno nic co by slo smazat')
        
    def konec(self):
        quit()
        
    def radio_vyber_zaznam_db(self, row):
        #vypisuju si jaky radek je vybran
        print row
        self.vymaz_poznamku()
        self.vymaz_zakladni()
        self.vymaz_rozsirene()
        #self.pole_db[row][0] -> dvourozmerne pole, prvni hodnota je radek, druha hodnota 0 je sloupec vyrobce...
        self.var_vyrobce.set(self.pole_db[row][0])
        self.var_karoserie.set(self.pole_db[row][1])
        self.var_palivo.set(self.pole_db[row][2])
        self.dvere.insert(END, self.pole_db[row][3])
        self.rok_vyroby.insert(END, self.pole_db[row][4])
        self.objem.insert(END, self.pole_db[row][5])
        self.vykon.insert(END, self.pole_db[row][6])
        #int() pretypuje pole na 1, je to proto, protoze po aktualizaci to na text '1' nechtelo fungovat. Takze testuju jestli je v poli
        #ulozeni cislo 1, pokud ano checkbutton oznacim, pokud ne, tak jej odoznacim
        if(int(self.pole_db[row][7]) == 1):
            self.abs.select()
        else:
            self.abs.deselect()
        if(int(self.pole_db[row][8]) == 1):
            self.airbag.select()
        else:
            self.airbag.deselect()
        if(int(self.pole_db[row][9]) == 1):
            self.autoalarm.select()
        else:
            self.autoalarm.deselect()
        if(int(self.pole_db[row][10]) == 1):
            self.prevodovka.select()
        else:
            self.prevodovka.deselect()
        if(int(self.pole_db[row][11]) == 1):
            self.autoradio.select()
        else:
            self.autoradio.deselect()
        if(int(self.pole_db[row][12]) == 1):
            self.central.select()
        else:
            self.central.deselect()
        if(int(self.pole_db[row][13]) == 1):
            self.ovladani_oken_z.select()
        else:
            self.ovladani_oken_z.deselect()
        if(int(self.pole_db[row][14]) == 1):
            self.ovladani_oken_p.select()
        else:
            self.ovladani_oken_p.deselect()
        if(int(self.pole_db[row][15]) == 1):
            self.klimatizace.select()
        else:
            self.klimatizace.deselect()
        self.barva.insert(END, self.pole_db[row][16])
        if(int(self.pole_db[row][17]) == 1):
            self.posilovac.select()
        else:
            self.posilovac.deselect()
        if(int(self.pole_db[row][18]) == 1):
            self.protiprokluzovy_system.select()
        else:
            self.protiprokluzovy_system.deselect()
        if(int(self.pole_db[row][19]) == 1):
            self.stresni_okno.select()
        else:
            self.stresni_okno.deselect()
        if(int(self.pole_db[row][20]) == 1):
            self.tazne_zarizeni.select()
        else:
            self.tazne_zarizeni.deselect()
        if(int(self.pole_db[row][21]) == 1):
            self.xenony.select()
        else:
            self.xenony.deselect()
        self.poznamky.insert(END, self.pole_db[row][22])
        
    def pridat_zaznam(self):
        #kontrola jestli nejaka polozka neni '' - tedy prazdna, nevyplnena
        if(self.var_vyrobce.get() == '' or self.var_karoserie.get() == '' or self.dvere.get() == '' or self.var_palivo.get() == '' or self.rok_vyroby.get() == '' or self.objem.get() == '' or self.vykon.get() == ''):
            print 'Chyba - je potreba zadat povinne polozky'
            self.var_sb.set('Chyba - je potřeba zadat povinné položky (vše v záložce "základní" musí být vyplněno)')
            #vracim nulu - pri aktualizaci potrebuju vedet jestli byl zaznam pridan ci ne
            return 0
        #kontrola jestli se jedna o cislo
        elif(self.objem.get().isdigit() and self.vykon.get().isdigit() and self.dvere.get().isdigit()):
            #kontrola jestli je rok > 1000 a < 2020
            if int(self.rok_vyroby.get()) < 1000 or int(self.rok_vyroby.get()) > 2020:
                self.var_sb.set('Chyba - je potřeba zadat validní rok')
                return 0
            #je potreba pridat zaznam do MultilistBoxu - PORADI je dulezity - je prehazeny aby korespondovalo s viditelnymi sloupci
            self.mlb.insert(END, [self.var_vyrobce.get(),
                                       self.var_karoserie.get(),
                                       self.objem.get(),
                                       self.vykon.get(),
                                       self.rok_vyroby.get(),
                                       self.dvere.get(),
                                       self.var_palivo.get(),
                                       self.barva.get(),
                                       self.poznamky.get(1.0, END),
                                       self.var_abs.get(),
                                       self.var_airbag.get(),
                                       self.var_autoalarm.get(),
                                       self.var_prevodovka.get(),
                                       self.var_autoradio.get(),
                                       self.var_central.get(),
                                       self.var_ovladani_oken_z.get(),
                                       self.var_ovladani_oken_p.get(),
                                       self.var_klimatizace.get(),
                                       self.var_posilovac.get(),
                                       self.var_protiprokluzovy_system.get(),
                                       self.var_stresni_okno.get(),
                                       self.var_tazne_zarizeni.get(),
                                       self.var_xenony.get()
                                       ])
            #je potreba zaznam ulozit do pole
            self.pole_db.append([self.var_vyrobce.get(),
                                       self.var_karoserie.get(),
                                       self.var_palivo.get(),
                                       self.dvere.get(),
                                       self.rok_vyroby.get(),
                                       self.objem.get(),
                                       self.vykon.get(),
                                       self.var_abs.get(),
                                       self.var_airbag.get(),
                                       self.var_autoalarm.get(),
                                       self.var_prevodovka.get(),
                                       self.var_autoradio.get(),
                                       self.var_central.get(),
                                       self.var_ovladani_oken_z.get(),
                                       self.var_ovladani_oken_p.get(),
                                       self.var_klimatizace.get(),
                                       self.barva.get(),
                                       self.var_posilovac.get(),
                                       self.var_protiprokluzovy_system.get(),
                                       self.var_stresni_okno.get(),
                                       self.var_tazne_zarizeni.get(),
                                       self.var_xenony.get(),
                                       self.poznamky.get(1.0, END)
                                       ])
            #vymazani vsech poli - coz slouzi jako reakce na ulozeni zaznamu
            self.vymaz_zakladni()
            self.vymaz_rozsirene()
            self.vymaz_poznamku()
            #vse ok
            self.var_sb.set(' ')
            return 1
        else:
            self.var_sb.set('Chyba - objem, výkon, dveře musí být čísla')
            return 0
            
    def upravit_zaznam(self):
        #jen pokud lze zaznam pridat, tak po pridani novyho smazu ten stary
        if self.pridat_zaznam() == 1:
            self.vymaz_zaznam()
            
#init----------------------------
    def __init__(self, root):

        #toto pole slouzi jako databaze - nejedna se tedy o perzistentni uloziste
        self.pole_db = [
            ['Vyrobce', 'Karoserie', 'diesel', '3', '2001', '1900', '70', '1', '0', '0', '1', '0', '0', '0', '1', '0', 'cervena', '0', '1', '0', '0', '1', 'poznamka'],
            ['Vyrobce2', 'Karoserie2', 'benzin', '5', '1990', '3000', '100', '0', '1', '1', '0', '0', '0', '0', '1', '0', 'modra', '1', '0', '0', '1', '0', 'poznamka2']
        ]
        
        #promenne pro praci s OptionMenu, checkboxy, status barem
        self.var_vyrobce = StringVar(root)
        self.var_karoserie = StringVar(root)
        self.var_palivo = StringVar(root)
        self.var_abs = IntVar()
        self.var_airbag = IntVar()
        self.var_autoalarm = IntVar()
        self.var_prevodovka = IntVar()
        self.var_autoradio = IntVar()
        self.var_central = IntVar()
        self.var_ovladani_oken_z = IntVar()
        self.var_ovladani_oken_p = IntVar()
        self.var_klimatizace = IntVar()
        self.var_posilovac = IntVar()
        self.var_protiprokluzovy_system = IntVar()
        self.var_stresni_okno = IntVar()
        self.var_tazne_zarizeni = IntVar()
        self.var_xenony = IntVar()
        self.var_sb = StringVar(root)
        self.var_sb.set("")
        
        #nadpis okna
        root.title('Databáze vozidel')
        
#menu
        self.menubar = Menu(root)
        
        self.filemenu = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="Soubor", menu=self.filemenu)
        self.filemenu.add_command(label="Otevřít")
        self.filemenu.add_command(label="Uložit jako")
        self.filemenu.add_command(label="Tisk")
        self.filemenu.add_command(label="Konec", command=self.konec)
        
        self.settingsmenu = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="Nastavení", menu=self.settingsmenu)
        self.settingsmenu.add_command(label="Import")
        self.settingsmenu.add_command(label="Export")
        
        self.menubar.add_cascade(label="Databáze vozidel")
        
        self.helpmenu = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="Nápověda", menu=self.helpmenu)
        self.helpmenu.add_command(label="Verze", command=self.novy_okno_verze)
        self.helpmenu.add_command(label="Web")
        self.helpmenu.add_command(label="O aplikaci", command=self.novy_okno_app)
        self.helpmenu.add_command(label="O autorovi", command=self.novy_okno_author)
        root.config(menu=self.menubar)
        
#Zalozky
        self.zalozky = Tix.NoteBook(root, height=330, width=640)
        self.zalozky.add("1", label="Základní")
        self.zz = self.zalozky.subwidget_list["1"]
        self.zalozky.add("2", label="Rozšířené")
        self.zr = self.zalozky.subwidget_list["2"]
        self.zalozky.add("3", label="Poznámky")
        self.zp = self.zalozky.subwidget_list["3"]
        self.zalozky.pack(fill=X, expand=0)

#Spodni frame
		#db vozidel
        #borderwidth -> sirka okraje, relief=typ zobrazeni okraje
        self.db_frame = Frame(root, borderwidth=2, relief=GROOVE)
        #pack je metoda ktera vykresli prvek do framu, 
        #side=BOTTOM -> prichytava se dolu, TOP - prichytava se nahoru, LEFT, RIGHT
        self.db_frame.pack(fill=BOTH, expand=1, side=BOTTOM, padx=2, pady=0)
        
		#status bar
        #rodic framu self.sb je self.db_frame, takze self.sb bude uvnitr self.db_frame
        self.sb=Frame(self.db_frame, borderwidth=2, height=20, relief=GROOVE)
        self.sb.pack(fill=X, expand=0, side=BOTTOM)
        
        self.sb_text=Label(self.sb, anchor=W, textvariable=self.var_sb, fg="red")
        self.sb_text.pack(side=LEFT, padx=4)
		
        #prace s datumem
        dnes = datetime.datetime.now()
        self.v_dnes = StringVar()
        self.v_dnes.set(dnes.strftime("%d. %m. %Y %A"))
        self.sb_datum = Entry(self.sb, textvariable=self.v_dnes, width=22)
        self.sb_datum.pack(side=RIGHT, padx=4)
		
        self.databaze_vozidel()
        
#Zakladni informace o vozidle
        #horni frame
        self.zakladni = Frame(self.zz, borderwidth=2, relief=GROOVE)
        self.zakladni.pack(fill=BOTH, expand=1, side=LEFT, padx=1, pady=1)
        #anchor - zarovnani pisma uvnitr Labelu/Navesti. anchor=N = Sever = Uprostred
        self.la_zakladni=Label(self.zakladni, anchor=N, text="Základní informace o vozidle", font="Arial 12 bold")
        self.la_zakladni.pack(fill=BOTH, expand=0, side=TOP, padx=4, pady=0)
        
            #vyrobce
        self.frvyrobce=Frame(self.zakladni, borderwidth=2, relief=GROOVE)
        #anchor=W -> West=vlevo
        self.frvyrobce.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        #width = minimali sirka prvku
        self.lavyrobce=Label(self.frvyrobce, anchor=W, text="Výrobce: ", width=16)
        self.lavyrobce.pack(fill=BOTH, expand=0, side=LEFT, padx=4, pady=0)
        self.vyrobce = OptionMenu(self.frvyrobce, self.var_vyrobce, "Audi", "BMW", "Ferrari", "Škoda", "Volkswagen")
        self.vyrobce.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=0)
        
            #karoserie
        self.frkaroserie=Frame(self.zakladni, borderwidth=2, relief=GROOVE)
        self.frkaroserie.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lakaroserie=Label(self.frkaroserie, anchor=W, text="Karoserie: ", width=16)
        self.lakaroserie.pack(fill=BOTH, expand=0, side=LEFT, padx=4, pady=0)
        self.karoserie = OptionMenu(self.frkaroserie, self.var_karoserie, "Sedan", "Nesedan")
        self.karoserie.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=0)
		
			#palivo
        self.frpalivo=Frame(self.zakladni, borderwidth=2, relief=GROOVE)
        self.frpalivo.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lapalivo=Label(self.frpalivo, anchor=W, text="Palivo: ", width=16)
        self.lapalivo.pack(fill=BOTH, expand=0, side=LEFT, padx=4, pady=0)
        self.palivo = OptionMenu(self.frpalivo, self.var_palivo, "Benzín", "Nafta", "Plyn")
        self.palivo.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=0)
            
            #dvere
        self.frdvere=Frame(self.zakladni, borderwidth=2, relief=GROOVE)
        self.frdvere.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.ladvere=Label(self.frdvere, anchor=W, text="Počet dveří: ", width=16)
        self.ladvere.pack(fill=BOTH, expand=0, side=LEFT, padx=4, pady=2)
        self.dvere = Spinbox(self.frdvere, values=(2,3,4,5), width=14)
        self.dvere.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=2)
        
            #rok vyroby
        self.frrok_vyroby=Frame(self.zakladni, borderwidth=2, relief=GROOVE)
        self.frrok_vyroby.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.larok_vyroby=Label(self.frrok_vyroby, anchor=W, text="Rok výroby: ", width=16)
        self.larok_vyroby.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=2)
        self.rok_vyroby = Entry(self.frrok_vyroby, width=16)
        self.rok_vyroby.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=2)
        self.larok_vyroby=Label(self.frrok_vyroby, anchor=W, text="Vložte pouze číslo", font="Arial 12 italic underline")
        self.larok_vyroby.pack(fill=BOTH, expand=1, side=LEFT, padx=2, pady=2)
        
            #objem
        self.frobjem=Frame(self.zakladni, borderwidth=2, relief=GROOVE)
        self.frobjem.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.laobjem=Label(self.frobjem, anchor=W, text="Objem motoru {cm3}: ", width=16)
        self.laobjem.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=2)
        self.objem = Entry(self.frobjem, width=16)
        self.objem.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=2)
        self.laobjem=Label(self.frobjem, anchor=W, text="Vložte pouze číslo", font="Arial 12 italic underline")
        self.laobjem.pack(fill=BOTH, expand=1, side=LEFT, padx=2, pady=2)
        
            #vykon motoru
        self.frvykon=Frame(self.zakladni, borderwidth=2, relief=GROOVE)
        self.frvykon.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lavykon=Label(self.frvykon, anchor=W, text="Výkon motoru {kW}: ", width=16)
        self.lavykon.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=2)
        self.vykon = Entry(self.frvykon, width=16)
        self.vykon.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=2)
        self.lavykon=Label(self.frvykon, anchor=W, text="Vložte pouze číslo", font="Arial 12 italic underline")
        self.lavykon.pack(fill=BOTH, expand=1, side=LEFT, padx=2, pady=2)
        
            #tlacitka upravit a smazat
        self.tlacitka_zakladni = Frame(self.zakladni, borderwidth=2, relief=GROOVE)
        self.tlacitka_zakladni.pack(side=BOTTOM, padx=4, pady=1)
        self.zVymazat_pole = Button(self.tlacitka_zakladni, text='Vymazat pole', width=16, height=1, command=self.vymaz_zakladni)
        self.zVymazat_pole.pack(side=LEFT, padx=4, pady=1)
        self.zVymazat_zaznam = Button(self.tlacitka_zakladni, text='Vymazat záznam', width=16, height=1, command=self.vymaz_zaznam)
        self.zVymazat_zaznam.pack(side=LEFT, padx=4, pady=1)
        self.zUpravit_zaznam = Button(self.tlacitka_zakladni, text='Uložit změny', width=16, height=1, command=self.upravit_zaznam)
        self.zUpravit_zaznam.pack(side=LEFT, padx=4, pady=1)
        self.zUpravit_zaznam = Button(self.tlacitka_zakladni, text='Uložit nový záznam', width=16, height=1, command=self.pridat_zaznam)
        self.zUpravit_zaznam.pack(side=LEFT, padx=4, pady=1)

#rozsirene informace o vozidle
        self.rozsirene = Frame(self.zr, borderwidth=2, relief=GROOVE)
        self.rozsirene.pack(fill=BOTH, expand=1, side=LEFT, padx=1, pady=1)
        self.la_rozsirene=Label(self.rozsirene, anchor=N, text="Rozšířené informace o vozidle", font="Arial 12 bold")
        self.la_rozsirene.pack(fill=BOTH, expand=0, side=TOP, padx=4, pady=0)
        
        self.rozsireneTop = Frame(self.rozsirene, borderwidth=2, relief=GROOVE)
        self.rozsireneTop.pack(fill=BOTH, expand=1, side=TOP, padx=0, pady=0)
        
        self.rozsireneL = Frame(self.rozsireneTop, borderwidth=2, relief=GROOVE)
        self.rozsireneL.pack(fill=BOTH, expand=1, side=LEFT, padx=0, pady=0)
        
        self.rozsireneR = Frame(self.rozsireneTop, borderwidth=2, relief=GROOVE)
        self.rozsireneR.pack(fill=BOTH, expand=1, side=RIGHT, padx=0, pady=0)
        
    #levy sloupec/frame
            #abs
        self.frabs=Frame(self.rozsireneL, borderwidth=2, relief=GROOVE)
        self.frabs.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.labs=Label(self.frabs, anchor=W, text="ABS: ", width=20)
        self.labs.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.abs = Checkbutton(self.frabs, variable=self.var_abs)
        self.abs.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
            #airbag
        self.frairbag=Frame(self.rozsireneL, borderwidth=2, relief=GROOVE)
        self.frairbag.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lairbag=Label(self.frairbag, anchor=W, text="Airbag: ", width=20)
        self.lairbag.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.airbag = Checkbutton(self.frairbag, variable=self.var_airbag)
        self.airbag.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
            #autoalarm
        self.frautoalarm=Frame(self.rozsireneL, borderwidth=2, relief=GROOVE)
        self.frautoalarm.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lautoalarm=Label(self.frautoalarm, anchor=W, text="Autoalarm: ", width=20)
        self.lautoalarm.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.autoalarm = Checkbutton(self.frautoalarm, variable=self.var_autoalarm)
        self.autoalarm.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
            #prevodovka
        self.frprevodovka=Frame(self.rozsireneL, borderwidth=2, relief=GROOVE)
        self.frprevodovka.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lprevodovka=Label(self.frprevodovka, anchor=W, text="Převodovka: ", width=20)
        self.lprevodovka.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.prevodovka = Checkbutton(self.frprevodovka, variable=self.var_prevodovka)
        self.prevodovka.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
            #autoradio
        self.frautoradio=Frame(self.rozsireneL, borderwidth=2, relief=GROOVE)
        self.frautoradio.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lautoradio=Label(self.frautoradio, anchor=W, text="Autorádio: ", width=20)
        self.lautoradio.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.autoradio = Checkbutton(self.frautoradio, variable=self.var_autoradio)
        self.autoradio.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
            #central
        self.frcentral=Frame(self.rozsireneL, borderwidth=2, relief=GROOVE)
        self.frcentral.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lcentral=Label(self.frcentral, anchor=W, text="Centrál: ", width=20)
        self.lcentral.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.central = Checkbutton(self.frcentral, variable=self.var_central)
        self.central.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
            #ovladani oken
        self.frovladani_oken=Frame(self.rozsireneL, borderwidth=2, relief=GROOVE)
        self.frovladani_oken.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lovladani_oken=Label(self.frovladani_oken, anchor=W, text="Ovládání oken: ", width=12)
        self.lovladani_oken.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.laovladani_oken_z=Label(self.frovladani_oken, anchor=W, text="Zadní")
        self.laovladani_oken_z.pack(fill=BOTH, expand=1, side=LEFT, padx=1, pady=1)
        self.ovladani_oken_z = Checkbutton(self.frovladani_oken, variable=self.var_ovladani_oken_z)
        self.ovladani_oken_z.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.laovladani_oken_p=Label(self.frovladani_oken, anchor=W, text="Přední")
        self.laovladani_oken_p.pack(fill=BOTH, expand=1, side=LEFT, padx=1, pady=1)
        self.ovladani_oken_p = Checkbutton(self.frovladani_oken, variable=self.var_ovladani_oken_p)
        self.ovladani_oken_p.pack(fill=BOTH, expand=1, side=BOTTOM, padx=4, pady=1)
        
    #pravy sloupec/frame
            #klimatizace
        self.frklimatizace=Frame(self.rozsireneR, borderwidth=2, relief=GROOVE)
        self.frklimatizace.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lklimatizace=Label(self.frklimatizace, anchor=W, text="Klimatizace: ", width=18)
        self.lklimatizace.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.klimatizace = Checkbutton(self.frklimatizace, variable=self.var_klimatizace)
        self.klimatizace.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
            #barva
        self.frbarva=Frame(self.rozsireneR, borderwidth=2, relief=GROOVE)
        self.frbarva.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lbarva=Label(self.frbarva, anchor=W, text="Barva: ", width=6)
        self.lbarva.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.barva = Entry(self.frbarva, width=18)
        self.barva.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
            #posilovac
        self.frposilovac=Frame(self.rozsireneR, borderwidth=2, relief=GROOVE)
        self.frposilovac.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lposilovac=Label(self.frposilovac, anchor=W, text="Posilovač: ", width=18)
        self.lposilovac.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.posilovac = Checkbutton(self.frposilovac, variable=self.var_posilovac)
        self.posilovac.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
            #protiprokluzovy system
        self.frprotiprokluzovy_system=Frame(self.rozsireneR, borderwidth=2, relief=GROOVE)
        self.frprotiprokluzovy_system.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lprotiprokluzovy_system=Label(self.frprotiprokluzovy_system, anchor=W, text="Protiprokluzový systém: ", width=18)
        self.lprotiprokluzovy_system.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.protiprokluzovy_system = Checkbutton(self.frprotiprokluzovy_system, variable=self.var_protiprokluzovy_system)
        self.protiprokluzovy_system.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
            #stresni okno
        self.frstresni_okno=Frame(self.rozsireneR, borderwidth=2, relief=GROOVE)
        self.frstresni_okno.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lstresni_okno=Label(self.frstresni_okno, anchor=W, text="Strešní okno: ", width=18)
        self.lstresni_okno.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.stresni_okno = Checkbutton(self.frstresni_okno, variable=self.var_stresni_okno)
        self.stresni_okno.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
    
            #tazne zarizeni
        self.frtazne_zarizeni=Frame(self.rozsireneR, borderwidth=2, relief=GROOVE)
        self.frtazne_zarizeni.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.ltazne_zarizeni=Label(self.frtazne_zarizeni, anchor=W, text="Tažné zařízení: ", width=18)
        self.ltazne_zarizeni.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.tazne_zarizeni = Checkbutton(self.frtazne_zarizeni, variable=self.var_tazne_zarizeni)
        self.tazne_zarizeni.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
            #xenony
        self.frxenony=Frame(self.rozsireneR, borderwidth=2, relief=GROOVE)
        self.frxenony.pack(fill=BOTH, expand=0, anchor=W, side=TOP)
        self.lxenony=Label(self.frxenony, anchor=W, text="Xenony: ", width=18)
        self.lxenony.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        self.xenony = Checkbutton(self.frxenony, variable=self.var_xenony)
        self.xenony.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=1)
        
        self.tlacitka_rozsirene = Frame(self.rozsirene, borderwidth=2, relief=GROOVE)
        self.tlacitka_rozsirene.pack(side=BOTTOM, padx=4, pady=1)
        self.rVymazat_pole = Button(self.tlacitka_rozsirene, text='Vymazat pole', width=16, height=1, command=self.vymaz_rozsirene)
        self.rVymazat_pole.pack(side=LEFT, padx=4, pady=1)
        self.rVymazat_zaznam = Button(self.tlacitka_rozsirene, text='Vymazat záznam', width=16, height=1, command=self.vymaz_zaznam)
        self.rVymazat_zaznam.pack(side=LEFT, padx=4, pady=1)
        self.rUpravit_zaznam = Button(self.tlacitka_rozsirene, text='Uložit změny', width=16, height=1, command=self.upravit_zaznam)
        self.rUpravit_zaznam.pack(side=LEFT, padx=4, pady=1)
        self.rUpravit_zaznam = Button(self.tlacitka_rozsirene, text='Uložit nový záznam', width=16, height=1, command=self.pridat_zaznam)
        self.rUpravit_zaznam.pack(side=LEFT, padx=4, pady=1)

        
#poznamky
        #pravy frame
        self.fpoznamky = Frame(self.zp, borderwidth=2, relief=GROOVE)
        self.fpoznamky.pack(fill=BOTH, expand=1, side=TOP, padx=1, pady=1)
        
		#Poznamka
        self.frpozn=Frame(self.fpoznamky, borderwidth=2, relief=GROOVE)
        self.frpozn.pack(anchor=W, expand=1, fill=BOTH, side=TOP)
        self.lpozn=Label(self.frpozn, text="Poznámky", font="Arial 12 bold")
        self.lpozn.pack(fill=Y, side=TOP, padx=8, pady=0)
        self.poznamky = Text(self.frpozn, width=25, height=4)
        self.poznamky.pack(fill=BOTH, expand=1, side=LEFT, padx=0, pady=5)
        
		#tlacitka upravit a smazat
        self.tlacitka_poznamky = Frame(self.fpoznamky, borderwidth=2, relief=GROOVE)
        self.tlacitka_poznamky.pack(anchor=N, side=TOP, padx=4, pady=1)
        self.pVymazat_poznamky = Button(self.tlacitka_poznamky, text='Vymazat pole', width=16, height=1, command=self.vymaz_poznamku)
        self.pVymazat_poznamky.pack(side=LEFT, padx=4, pady=1)
        self.pVymazat_zaznam = Button(self.tlacitka_poznamky, text='Vymazat záznam', width=16, height=1, command=self.vymaz_zaznam)
        self.pVymazat_zaznam.pack(side=LEFT, padx=4, pady=1)
        self.pUpravit_poznamky = Button(self.tlacitka_poznamky, text='Uložit změny', width=16, height=1, command=self.upravit_zaznam)
        self.pUpravit_poznamky.pack(side=LEFT, padx=4, pady=1)
        self.pUpravit_zaznam = Button(self.tlacitka_poznamky, text='Uložit nový záznam', width=16, height=1, command=self.pridat_zaznam)
        self.pUpravit_zaznam.pack(side=LEFT, padx=4, pady=1)

root = Tix.Tk()
app = myApp(root)
root.mainloop()
