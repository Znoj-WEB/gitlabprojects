#------------------------------------------------------------------------------#
# Quadratic equation                                                           #
#------------------------------------------------------------------------------#

from Tkinter import * 
from math import *

class myApp:
    
    def vymaz(self):
        self.enta.delete(0, END)
	self.entb.delete(0, END)
	self.entc.delete(0, END)
        print "clear"

    def vyresit(self):
        print "calc"
	try:
        	a = float(self.enta.get())
        	b = float(self.entb.get())
        	c = float(self.entc.get())
	except ValueError:
		self.butv.configure(text="Musi byt zadana cisla", foreground="red");
		return 0

        diskr = 4*a*c
        print "diskriminant: " + str(diskr)
	if diskr < 0:
		vysledek = "Zaporny diskriminant,\nrovnice nema reseni v R."
		self.butv.configure(text=vysledek, foreground="red");
		return 0
	elif diskr == 0:
		x = -b/2*a;
		vysledek = "x = %.4f"%(x)
	else:
        	x1 = (-b+sqrt(diskr))/2*a
        	x2 = (-b-sqrt(diskr))/2*a
	        vysledek = "x1 = %.4f\nx2 = %.4f"%(x1, x2)
	self.butv.configure(text=vysledek, foreground="#339900");
        

    def konec(self):
        print "konec"
        quit()

    def __init__(self, root):
    
        root.title('Quadratic equation')

        self.top = Frame(root)
        self.top.pack(fill=BOTH, expand=True)


        self.nadpis = Frame(self.top, borderwidth=2)
        self.nadpis.pack(fill=X, side=TOP, padx=2, pady=2)
        self.buta = Button(self.nadpis, text='Reseni rovnice axx + bx + c = 0', height=2)
        self.buta.pack(fill=X, expand=True, side=TOP, padx=4, pady=2)

        self.zbytek = Frame(self.top, relief=GROOVE, borderwidth=2)
        self.zbytek.pack(fill=BOTH, anchor=N, expand=True, side=TOP, padx=4, pady=2)

        
        self.zadanif = Frame(self.zbytek, relief=GROOVE, borderwidth=2)
        self.zadanif.pack(fill=Y, side=LEFT, padx=4, pady=2)

        self.la=Label(self.zadanif, text="a =")
        self.la.pack(side=TOP, padx=8, pady=1)
        self.enta = Entry(self.zadanif)
        self.enta.pack(side=TOP, padx=8, pady=1)

        self.lb=Label(self.zadanif, text="b =")
        self.lb.pack(side=TOP, padx=8, pady=1)
        self.entb = Entry(self.zadanif)
        self.entb.pack(side=TOP, padx=8, pady=1)

        self.lc=Label(self.zadanif, text="c =")
        self.lc.pack(side=TOP, padx=8, pady=1)
        self.entc = Entry(self.zadanif)
        self.entc.pack(side=TOP, padx=8, pady=1)

        self.but = Button(self.zadanif, text='Vymazat', width=8, command=self.vymaz)
        self.but.pack(side=TOP, padx=4, pady=2)


        self.vysledek = Frame(self.zbytek, borderwidth=2)
        self.vysledek.pack(fill=BOTH, expand=True, side=LEFT, padx=4, pady=1)


        self.vysledek_text = Frame(self.vysledek, borderwidth=2)
        self.vysledek_text.pack(fill=BOTH, expand=True, side=TOP, padx=4, pady=1)
        self.vla=Label(self.vysledek_text, text="Vysledek")
        self.vla.pack(fill=BOTH, side=TOP, padx=4, pady=1)
        self.butv = Button(self.vysledek_text, text='Zadny zatim neni', width=20, height=6)
        self.butv.pack(fill=BOTH, expand=True, side=BOTTOM, padx=4, pady=4)

        self.vysledek_tlacitka = Frame(self.vysledek, borderwidth=2)
        self.vysledek_tlacitka.pack(side=BOTTOM, padx=4, pady=1)
        self.butv2 = Button(self.vysledek_tlacitka, text='Vyresit', width=10, height=2, command=self.vyresit)
        self.butv2.pack(side=LEFT, padx=4, pady=1)
        self.butk = Button(self.vysledek_tlacitka, text='Konec', width=10, height=2, command=self.konec)
        self.butk.pack(side=LEFT, padx=4, pady=1)

        self.enta.insert(0, '+1.0')
        self.enta.focus_force()
        self.entb.insert(0, '-3.0')
        self.entb.focus_force()
        self.entc.insert(0, '+2.0')
        self.entc.focus_force()
        

root = Tk()
app = myApp(root)
root.mainloop()
