# -*- coding: utf-8 -*-

from Tkinter import *
import Tix
import MultiListbox as table


data = [
       ["Petr", "Bílý","045214/1512","17. Listopadu", 15, "Ostrava", 70800,"poznamka"],
       ["Jana", "Zelený","901121/7238","Vozovna", 54, "Poruba", 78511,""],
       ["Karel", "Modrý","800524/5417","Porubská", 7, "Praha", 11150,""],
       ["Martin", "Stříbrný","790407/3652","Sokolovská", 247, "Brno", 54788,"nic"]]

class App(object):
    def vlozeni(self, row):
        self.vynulovat_error()
        self.ejm.delete(0, END)
        self.eprjm.delete(0, END)
        self.erc.delete(0, END)
        self.eUlice.delete(0, END)
        self.eCp.delete(0, END)
        self.eMesto.delete(0, END)
        self.ePsc.delete(0, END)
        self.txtPozn.delete(1.0, END)
            
        try:            
            self.ejm.insert(END,data[row][0])
            self.eprjm.insert(END,data[row][1])
            self.erc.insert(END,data[row][2])
            self.eUlice.insert(END,data[row][3])
            self.eCp.insert(END,data[row][4])
            self.eMesto.insert(END,data[row][5])
            self.ePsc.insert(END,data[row][6])
            self.txtPozn.insert(END,data[row][7])
        except Exception:
            self.ejm.insert(END,self.mlb.get(self.mlb.curselection()[0])[0])
            self.eprjm.insert(END,self.mlb.get(self.mlb.curselection()[0])[1])
            self.erc.insert(END,self.mlb.get(self.mlb.curselection()[0])[2])
        
    def novy_zaznam(self):
        self.vynulovat_error()
        self.ejm.delete(0, END)
        self.eprjm.delete(0, END)
        self.erc.delete(0, END)
        self.eUlice.delete(0, END)
        self.eCp.delete(0, END)
        self.eMesto.delete(0, END)
        self.ePsc.delete(0, END)
        self.txtPozn.delete(1.0, END)

    def ulozit_zaznam(self):
        self.vynulovat_error()
        jmeno = self.ejm.get()
        prijmeni = self.eprjm.get()
        rc = self.erc.get()
        if jmeno == '':
            print "Jmeno musi byt zadano"
            self.err.config(state=NORMAL)
            self.err.insert(END, 'CHYBA')
            #self.err.config(state=DISABLED)
        else:
            self.mlb.insert(END, [self.ejm.get(), self.eprjm.get(), self.erc.get(), self.eUlice.get(), self.eCp.get(), self.eMesto.get(), self.ePsc.get(), self.txtPozn.get(1.0, END)])

    def smazat_zaznam(self):
        self.vynulovat_error()
        try:
            self.mlb.delete(self.mlb.curselection()[0],self.mlb.curselection()[0])
        except Exception:
            print "Neni vybran zaznam ke smazani"
            self.err.config(state=NORMAL)
            self.err.insert(END, 'CHYBA')
            #self.err.config(state=DISABLED)

    def konec(self):
        quit()

    def vynulovat_error(self):
        self.err.config(state=NORMAL)
        self.err.delete(0, END)
        self.err.config(state=DISABLED)
   
    def __init__(self, master):
        self.row = IntVar()
        self.row = None
        self.jmeno = StringVar()
        self.prijmeni = StringVar()
        self.rc = StringVar()
        self.ulice = StringVar()
        self.cp = StringVar()
        self.mesto = StringVar()
        self.psc = StringVar()
        

        self.mlb = table.MultiListbox(master, (('Jméno', 20), ('Příjmení', 20), ('Rodné číslo', 12)))
        for i in range(len(data)):
            self.mlb.insert(END, (data[i][0], data[i][1],data[i][2]))
        self.mlb.pack(expand=YES,fill=BOTH, padx=10, pady=10)
        self.mlb.subscribe( lambda row: self.vlozeni(row) )

        
            #jmeno
        self.frjm = Frame(master)
        self.frjm.pack(pady=5, padx=5)
        self.ljm = Label(self.frjm, text="Jméno:", width=12)
        self.ljm.pack(side=LEFT)
        self.ejm = Entry(self.frjm, width=25)
        self.ejm.pack(side=LEFT, fill=X)

            #prijmeni
        self.frprjm = Frame(master)
        self.frprjm.pack(pady=5, padx=5)
        self.lprjm = Label(self.frprjm, text="Příjmení:", width=12)
        self.lprjm.pack(side=LEFT)
        self.eprjm = Entry(self.frprjm, width=25)
        self.eprjm.pack(side=LEFT, fill=X)

            #rodne cislo
        self.frrc = Frame(master)
        self.frrc.pack(pady=5, padx=5)
        self.lrc = Label(self.frrc, text="Rodné číslo:", width=12)
        self.lrc.pack(side=LEFT)
        self.erc = Entry(self.frrc, width=12)
        self.erc.pack(side=LEFT, fill=X)

            #chyba
        self.err = Entry(self.frrc, borderwidth=0, width=12, foreground="#FF2222", state=DISABLED)
        self.err.pack(side=RIGHT)


        #Zalozky
        self.nb = Tix.NoteBook(master)
        self.nb.add("page1", label="Adresa")
        self.nb.add("page2", label="Poznámka")
        self.p1 = self.nb.subwidget_list["page1"]
        self.p2 = self.nb.subwidget_list["page2"]
        self.nb.pack(expand=1, fill=BOTH)
        
        #Z1 - Adresa 
        self.adrFrame = LabelFrame(self.p1, text="Adresa", padx=5, pady=5)
        self.adrFrame.pack(expand=1, fill=BOTH, pady=5, padx=5)

            #Ulice
        self.frUlice = Frame(self.adrFrame)
        self.frUlice.pack(expand=1, fill=BOTH, pady=5, padx=5)
        self.lUlice = Label(self.frUlice, text="Ulice:", width=6)
        self.lUlice.pack(side=LEFT)
        self.eUlice = Entry(self.frUlice, width=20)
        self.eUlice.pack(side=LEFT, fill=X)
        
        self.lCp = Label(self.frUlice, text="č.p.", width=6)
        self.lCp.pack(side=RIGHT)
        self.eCp = Entry(self.frUlice, width=10)
        self.eCp.pack(side=RIGHT, fill=X)

            #Mesto
        self.frMesto = Frame(self.adrFrame)
        self.frMesto.pack(expand=1, fill=BOTH, pady=5, padx=5)
        self.lMesto = Label(self.frMesto, text="Město:", width=6)
        self.lMesto.pack(side=LEFT)
        self.eMesto = Entry(self.frMesto, width=25)
        self.eMesto.pack(side=LEFT, fill=X)

            #Psc
        self.frPsc = Frame(self.adrFrame)
        self.frPsc.pack(expand=1, fill=BOTH, pady=5, padx=5)
        self.lPsc = Label(self.frPsc, text="PSČ:", width=6)
        self.lPsc.pack(side=LEFT)
        self.ePsc = Entry(self.frPsc, width=6)
        self.ePsc.pack(side=LEFT, fill=X)

        
        #Z2 - Poznamka
        self.pozn = LabelFrame(self.p2, text="Poznámka")
        self.pozn.pack(expand=1, fill=BOTH, pady=5, padx=5)
        self.txtPozn = Text(self.pozn, borderwidth=0, width=50, height=10 )
        self.scrol = Scrollbar(self.pozn)
        self.scrol.pack(side=RIGHT, fill=Y)
        self.txtPozn.pack(expand=1, fill=BOTH)
        self.scrol.config(command=self.txtPozn.yview)
        self.txtPozn.config(yscrollcommand=self.scrol.set)


        #Tlacitka
        self.frButtons = Frame(master)
        self.frButtons.pack(fill=BOTH, padx=10, pady=10)
        self.new = Button(self.frButtons, width=15, text="Nový záznam", command=self.novy_zaznam)
        self.new.pack(side=LEFT)
        self.save = Button(self.frButtons, width=15, text="Uložit záznam", command=self.ulozit_zaznam)
        self.save.pack(side=LEFT)
        self.delete = Button(self.frButtons, width=15, text="Smazat záznam", command=self.smazat_zaznam)
        self.delete.pack(side=LEFT)
        self.delete = Button(self.frButtons, width=10, text="Konec", command=self.konec)
        self.delete.pack(side=RIGHT, padx=5)

      
    def edit(self, row):
        self.row=row
        print row
             

root = Tix.Tk()
root.wm_title("Formulář")
app = App(root)
root.mainloop()

