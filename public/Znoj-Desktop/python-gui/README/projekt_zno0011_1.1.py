﻿#------------------------------------------------------------------------------#
#------Projekt Poznamky + Kontakty - zno0011                                   #
#------------------------------------------------------------------------------#
# -*- coding: utf-8 -*- 
from Tkinter import *
import datetime
import Tix
import MultiListbox as table

class myApp:

    def novy_okno(self):
        about = Toplevel()
        about.title('Poznamky + kontakty')
        self.fram = Frame(about)
        self.fram.pack()
        Message(self.fram, text=u"Poznámky + Kontakty", width=200).pack()
        Message(self.fram, text=u"Uživatelská rozhraní", width=200).pack()
        Message(self.fram, text=u"letní semestr", width=200).pack()
        Message(self.fram, text=u"2012/2013", width=200).pack()
        Message(self.fram, text=u"ZNO0011", width=200).pack()
        Button(self.fram, text="Konec", command=about.destroy).pack(side=RIGHT)
    
    def aktualizovat_cas(self):
        dnes = datetime.datetime.now()
        self.v_dnes = dnes.strftime("%Y-%m-%d %H:%M")
        self.datum.delete(0, END)
        self.datum.insert(0, self.v_dnes)
        
        
    def vypis_vsechny_poznamky(self):
        self.mlb1 = table.MultiListbox(self.levy_poznamky, (('Název poznámky', 25), ('', 0)))
        for i in range(len(self.pole_poznamky)):
            self.mlb1.insert(END, (self.pole_poznamky[i][0], None))
        self.mlb1.pack(expand=YES,fill=BOTH, padx=2, pady=2)
        self.mlb1.subscribe( lambda row: self.radio_vyber_poznamky(row) ) 
        
        #pocitadlo = 0
        #self.frpoznazvy.destroy()
        #self.frpoznazvy=Frame(self.levy_poznamky, borderwidth=2, relief=GROOVE)
        #self.frpoznazvy.pack(anchor=W, side=TOP)
        
        #for i in self.pole_poznamky:
        #    self.poznazvy=Radiobutton(self.frpoznazvy, variable=self.v_radiobutton_poznamky, indicatoron=0, width=20, value=pocitadlo, command=self.radio_vyber_poznamky)
        #    self.poznazvy.pack(side=TOP, padx=4, pady=2)
            #--testovaci data
        #    self.poznazvy.config(text=i[0])
        #    pocitadlo+=1
        
    def vypis_vsechny_kontakty(self):
        
        self.mlb = table.MultiListbox(self.levy_kontakty, (('Jméno', 11), ('Příjmení', 13)))
        for i in range(len(self.pole_kontakty)):
            self.mlb.insert(END, (self.pole_kontakty[i][0], self.pole_kontakty[i][1]))
        self.mlb.pack(expand=YES,fill=BOTH, padx=2, pady=2)
        self.mlb.subscribe( lambda row: self.radio_vyber_kontakty(row) ) 
        
        #pocitadlo=0
        #self.frkonnazvy.destroy()
        #self.frkonnazvy=Frame(self.levy_kontakty, borderwidth=2, relief=GROOVE)
        #self.frkonnazvy.pack(anchor=W, side=TOP)
        
        #for i in self.pole_kontakty:
        #    self.konnazvy=Radiobutton(self.frkonnazvy, variable=self.v_radiobutton_kontakty, indicatoron=0, width=30, value=pocitadlo, command=self.radio_vyber_kontakty)
        #    self.konnazvy.pack(side=TOP, padx=4, pady=2)
            #--testovaci data
        #    self.konnazvy.config(text=i[0] + " " + i[1])
        #    pocitadlo+=1

    def vymaz_np(self):
        self.nazev.delete(0, END)
        self.datum.delete(0, END)
        self.poznamka.delete(1.0, END)
        print "clear"
            
    def vymaz_vp_bez_pole(self):
        self.v_datumy.set('')
        self.poznamky.delete(1.0, END)
        
    def vymaz_vp(self):
        self.vymaz_vp_bez_pole()
        try:
            self.pole_poznamky.pop(int(self.mlb1.curselection()[0]))
            self.mlb1.delete(self.mlb1.curselection()[0],self.mlb1.curselection()[0])
        except Exception:
            print 'Neni oznaceno nic co by slo smazat'
        print "clear"
    
    def vymaz_nk(self):
        self.jmeno.delete(0, END)
        self.prijmeni.delete(0, END)
        self.ulice.delete(0, END)
        self.cp.delete(0, END)
        self.mesto.delete(0, END)
        self.psc.delete(0, END)
        self.v_radiobutton_kontakt_pohlavi.set('0')
        self.tel1.delete(0, END)
        self.tel2.delete(0, END)
        self.tel3.delete(0, END)
        self.email.delete(0, END)
        self.pozn_kontakt.delete(1.0, END)
        print "clear"
        
    def vymaz_vk_bez_pole(self):
        self.jmeno_kontakty.delete(0, END)
        self.prijmeni_kontakty.delete(0, END)
        self.ulice_kontakty.delete(0, END)
        self.cp_kontakty.delete(0, END)
        self.mesto_kontakty.delete(0, END)
        self.psc_kontakty.delete(0, END)
        self.v_radiobutton_kontakty_pohlavi.set('0')
        self.tel1_kontakty.delete(0, END)
        self.tel2_kontakty.delete(0, END)
        self.tel3_kontakty.delete(0, END)
        self.email_kontakty.delete(0, END)
        self.poznamky_kontakty.delete(1.0, END)
        
    def vymaz_vk(self):
        self.vymaz_vk_bez_pole()
        try:
            self.pole_kontakty.pop(int(self.mlb.curselection()[0]))
            self.mlb.delete(self.mlb.curselection()[0],self.mlb.curselection()[0])
        except Exception:
            print 'Neni oznaceno nic co by slo smazat'
        #self.vypis_vsechny_kontakty()
        print "clear"
        
    def konec(self):
        quit()
                
    def radio_vyber_poznamky(self, row):
        self.vymaz_vp_bez_pole()
        self.v_datumy.set(self.pole_poznamky[row][2])
        self.poznamky.insert(END, self.pole_poznamky[row][1])
        
    def radio_vyber_kontakty(self, row):
        self.vymaz_vk_bez_pole()
        self.jmeno_kontakty.insert(END, self.pole_kontakty[row][0])
        self.prijmeni_kontakty.insert(END, self.pole_kontakty[row][1])
        self.ulice_kontakty.insert(END, self.pole_kontakty[row][2])
        self.cp_kontakty.insert(END, self.pole_kontakty[row][3])
        self.mesto_kontakty.insert(END, self.pole_kontakty[row][4])
        self.psc_kontakty.insert(END, self.pole_kontakty[row][5])
        self.v_radiobutton_kontakty_pohlavi.set(self.pole_kontakty[row][6])
        self.tel1_kontakty.insert(END, self.pole_kontakty[row][7])
        self.tel2_kontakty.insert(END, self.pole_kontakty[row][8])
        self.tel3_kontakty.insert(END, self.pole_kontakty[row][9])
        self.email_kontakty.insert(END, self.pole_kontakty[row][10])
        self.poznamky_kontakty.insert(END, self.pole_kontakty[row][11])
    
    def pridat_kontakt(self):
        if(self.jmeno.get() == '' or self.prijmeni.get() == ''):
            print 'Chyba - je potřeba zadat jméno i příjmení kontaktu'
        else:
            #je potreba pridat kontakt do MultilistBoxu i do pole
            self.mlb.insert(END, [self.jmeno.get(),
                                       self.prijmeni.get(),
                                       self.ulice.get(),
                                       self.cp.get(),
                                       self.mesto.get(),
                                       self.psc.get(),
                                       self.v_radiobutton_kontakt_pohlavi.get(),
                                       self.tel1.get(),
                                       self.tel2.get(),
                                       self.tel3.get(),
                                       self.email.get(),
                                       self.pozn_kontakt.get(1.0, END)
                                       ])
            self.pole_kontakty.append([self.jmeno.get(),
                                       self.prijmeni.get(),
                                       self.ulice.get(),
                                       self.cp.get(),
                                       self.mesto.get(),
                                       self.psc.get(),
                                       self.v_radiobutton_kontakt_pohlavi.get(),
                                       self.tel1.get(),
                                       self.tel2.get(),
                                       self.tel3.get(),
                                       self.email.get(),
                                       self.pozn_kontakt.get(1.0, END)
                                       ])
            print self.v_radiobutton_kontakt_pohlavi.get()
            #vymazani vsech poli - viditelna reakce na ulozeni kontaktu
            self.vymaz_nk()
            #vypsani kontaktu vcetne prave pridaneho v zalozce vsechny kontakty
            #self.vypis_vsechny_kontakty()
            
        
    def pridat_poznamku(self):
            #self.poznamka.get(1.0, END)
            #self.nazev.get()
            #self.datum.get()
        #je potreba kontrolovat a ukladat poznamku bez posledniho znaku - odradkovani
        if(self.nazev.get() != '' and self.poznamka.get(1.0, END)[:-1] != ''):
            #je potreba pridat poznamku do MultilistBoxu i do pole
            self.mlb1.insert(END, [self.nazev.get(), ''])
            self.pole_poznamky.append([self.nazev.get(), self.poznamka.get(1.0, END)[:-1], self.datum.get()])
            #aby byla nejaka reakce na pridani prvku, tak mazu vsechna pole
            self.vymaz_np()
            #aby se zmeny projevily
            #self.vypis_vsechny_poznamky()
        else:
            print 'Chyba - je třeba zadat text poznámky i její název'
        
    def upravit_poznamku(self):
        self.pridat_poznamky()
        self.vymaz_vp()
    
    def pridat_poznamky(self):
        if(self.poznamky.get(1.0, END)[:-1] != ''):
            #je potreba pridat poznamku do MultilistBoxu i do pole
            self.mlb1.insert(END, [self.pole_poznamky[int(self.mlb1.curselection()[0])][0], ''])
            self.pole_poznamky.append([self.pole_poznamky[int(self.mlb1.curselection()[0])][0], self.poznamky.get(1.0, END)[:-1], self.pole_poznamky[int(self.mlb1.curselection()[0])][2]])

        else:
            print 'Chyba - je třeba zadat text poznámky i její název'
        
    def upravit_kontakt(self):
        self.pridat_kontakty()
        self.vymaz_vk()
        
    def pridat_kontakty(self):
        if(self.jmeno_kontakty.get() == '' or self.prijmeni_kontakty.get() == ''):
            print 'Chyba - je potřeba zadat jméno i příjmení kontaktu'
        else:
            #je potreba pridat kontakt do MultilistBoxu i do pole
            self.mlb.insert(END, [self.jmeno_kontakty.get(),
                                       self.prijmeni_kontakty.get(),
                                       self.ulice_kontakty.get(),
                                       self.cp_kontakty.get(),
                                       self.mesto_kontakty.get(),
                                       self.psc_kontakty.get(),
                                       self.v_radiobutton_kontakty_pohlavi.get(),
                                       self.tel1_kontakty.get(),
                                       self.tel2_kontakty.get(),
                                       self.tel3_kontakty.get(),
                                       self.email_kontakty.get(),
                                       self.poznamky_kontakty.get(1.0, END)
                                       ])
            self.pole_kontakty.append([self.jmeno_kontakty.get(),
                                       self.prijmeni_kontakty.get(),
                                       self.ulice_kontakty.get(),
                                       self.cp_kontakty.get(),
                                       self.mesto_kontakty.get(),
                                       self.psc_kontakty.get(),
                                       self.v_radiobutton_kontakty_pohlavi.get(),
                                       self.tel1_kontakty.get(),
                                       self.tel2_kontakty.get(),
                                       self.tel3_kontakty.get(),
                                       self.email_kontakty.get(),
                                       self.poznamky_kontakty.get(1.0, END)
                                       ])
            print self.v_radiobutton_kontakt_pohlavi.get()
        #posun na dalsi polozku - aby byla videt akce po uprave
        #'+1' je prechod na dalsi polozku; modulo proto, aby se po poslednim skocilo na prvni
        #self.v_radiobutton_kontakty.set(str((int(self.v_radiobutton_kontakty.get())+1)%len(self.pole_kontakty)))
        #zobrazení obsahu poznamky nasledujici za upravenou
        #self.radio_vyber_kontakty()
        
#init----------------------------
    def __init__(self, root):

        #promenne
        #udelat jako instance trid a ty ulozit do pole - nedelat jako vicerozmerne pole
        self.pole_poznamky = [
            ['Název poznámky', 'Text první poznámky\nzkouška\nfunkčnosti\ns \npolem', '2013-03-15 17:00'],
            ['Název poznámky 2', 'Text druhé poznámky', '2013-03-15 17:40'],
            ['3', 'Text poznámky 3', '2013-03-15 19:00'],
            ['4. poznámka', 'Text poznámky 4', '2013-02-02 08:30'],
            ['oběd 5', 'Text poznámky 5 \n třeba recept', '2013-03-16 17:00'],
            ['6. poznámka = večeře', 'Text poznámky 6\n\n\n\n\nco bude na večeři', '2013-03-02 17:00'],
            ['cokoli', 'Text poznámky 7\n\n\nať je to dlouhý\n\n\n\n\ntak ještě pár řádků....\n\n\n\n....\n\nchce to posuvník\n\n...', '2013-03-17 17:00']
        ]

        self.pole_kontakty = [
            ['Jméno', 'Příjmení', 'Ulice', '111', 'Ostrava', '77777', '1', '777777777', '765432101', '789456123', 'prijmeni@ulice.cz', 'poznamka'],
            ['Jméno2', 'Příjmení2', 'Ulice', '111', 'Ostrava', '77777', '1', '777777777', '765432101', '789456123', 'prijmeni@ulice.cz', 'poznamka'],
            ['Jméno3', 'Příjmení3', 'Ulice', '111', 'Ostrava-Poruba', '77777', '1', '777777777', '765432101', '789456123', 'prijmeni@ulice.cz', ''],
            ['Jméno4', 'Poznámka', 'Ulice', '111', 'Ostrava', '77777', '1', '777777777', '765432101', '789456123', 'prijmeni@ulice.cz', 'poznamka\npoznamka2ynpoz3\npoz444444444\n5\n6'],
            ['Jméno5', 'Příjmení5', 'Dr. Březiny', '111', 'Olomouc', '', '1', '777777777', '765432101', '789456123', 'prijmeni@ulice.cz', 'poznamka'],
            ['Muž', 'Příjmení6', 'Ulice', '111', 'Ostrava', '77777', '1', '777777777', '765432101', '789456123', 'prijmeni@ulice.cz', 'poznamka'],
            ['Žena', 'Příjmení7', 'Ulice', '111', 'Praha', '77777', '0', '777777777', '765432101', '789456123', '', 'poznamka'],
            ['Jméno8', 'Příjmení8', 'Ulice', '111', 'Brno', '77777', '1', '777777777', '', '', 'prijmeni@ulice.cz', 'poznamka'],
            ['Jméno9', 'Příjmení9', 'Ulice', '111', 'Šumperk', '77777', '1', '', '765432101', '789456123', 'prijmeni@ulice.cz', 'poznamka']
        ]
        self.v_radiobutton_poznamky = StringVar()
        self.v_radiobutton_kontakty = StringVar()
        self.v_radiobutton_kontakt_pohlavi = StringVar()
        self.v_radiobutton_kontakty_pohlavi = StringVar()
        self.v_dnes = StringVar()
        self.v_datumy = StringVar()
        
        #nastaveni promennych
        dnes = datetime.datetime.now()
        self.v_dnes.set(dnes.strftime("%Y-%m-%d %H:%M"))
        self.v_radiobutton_poznamky.set("0") #automaticky nastavena 1. poznamka
        self.v_radiobutton_kontakty.set("0") #automaticky nastaven 1. kontakt
        self.v_radiobutton_kontakt_pohlavi.set("0") 
        self.v_radiobutton_kontakty_pohlavi.set("0") #automaticky nastavena žena
        
        #nadpis
        root.title('Poznámky + Kontakty')
        
#menu
        self.menubar = Menu(root)
        
        self.filemenu = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="Volby", menu=self.filemenu)
        self.filemenu.add_command(label="Konec", command=self.konec)
        self.menubar.add_cascade(label="O aplikaci", command=self.novy_okno)
        root.config(menu=self.menubar)
        
#Zalozky
        self.zalozky = Tix.NoteBook(root, height=330, width=650)
        self.zalozky.add("1", label="Nová poznámka")
        self.np = self.zalozky.subwidget_list["1"]
        self.zalozky.add("2", label="Všechny poznámky")
        self.vp = self.zalozky.subwidget_list["2"]
        self.zalozky.add("3", label="Nový kontakt")
        self.nk = self.zalozky.subwidget_list["3"]
        self.zalozky.add("4", label="Všechny kontakty")
        self.vk = self.zalozky.subwidget_list["4"]
        self.zalozky.pack(fill=BOTH, expand=1)

#Nova poznamka
        #levy frame
        self.leva_poznamka = Frame(self.np, borderwidth=2, relief=GROOVE)
        self.leva_poznamka.pack(fill=BOTH, expand=1, side=LEFT, padx=4, pady=2)
        
            #Poznamka
        self.lpoznamka=Label(self.leva_poznamka, text="Poznámka: ")
        self.lpoznamka.pack(fill=Y, side=TOP, padx=8, pady=0)
        self.poznamka = Text(self.leva_poznamka, width=40, height=10)
        self.poznamka.pack(fill=BOTH, expand=1, side=TOP, padx=0, pady=5)
        
        #pravy frame
        self.prava_poznamka = Frame(self.np, borderwidth=2, relief=GROOVE)
        self.prava_poznamka.pack(fill=BOTH, expand=0, side=RIGHT, padx=2, pady=2)
            
            #pravy horni ram
        self.detaily_vpravo = Frame(self.prava_poznamka, relief=GROOVE, borderwidth=2, width=40)
        self.detaily_vpravo.pack(side=TOP, padx=2, pady=11)
        
                #Nazev
        self.frnazev=Frame(self.detaily_vpravo, borderwidth=2, relief=GROOVE)
        self.frnazev.pack(pady=10)
        self.lanazev=Label(self.frnazev, text="Název: ", width=10)
        self.lanazev.pack(side=LEFT, padx=4, pady=2)
        self.nazev = Entry(self.frnazev, width=33)
        self.nazev.pack(side=LEFT, padx=4, pady=2)

                #Datum
        self.frdatum=Frame(self.detaily_vpravo, borderwidth=2, relief=GROOVE)
        self.frdatum.pack()
        self.ladatum=Label(self.frdatum, text="Datum: ", width=8)
        self.ladatum.pack(side=LEFT, padx=8, pady=2)
        self.datum = Entry(self.frdatum, textvariable=self.v_dnes, width=15)
        self.datum.pack(side=LEFT, padx=4, pady=2)
        self.but_akt = Button(self.frdatum, text='Aktualizovat cas', command=self.aktualizovat_cas, width=12)
        self.but_akt.pack(side=LEFT, padx=4, pady=1)


            #pravy dolni ram
        self.tlacitka = Frame(self.prava_poznamka, borderwidth=2, relief=GROOVE)
        self.tlacitka.pack(side=BOTTOM, padx=4, pady=1)
        self.bVymazat = Button(self.tlacitka, text='Vymazat', width=10, height=1, command=self.vymaz_np)
        self.bVymazat.pack(side=LEFT, padx=4, pady=1)
        self.bPridat = Button(self.tlacitka, text='Pridat', width=10, height=1, command=self.pridat_poznamku)
        self.bPridat.pack(side=LEFT, padx=4, pady=1)
        #self.butk = Button(self.tlacitka, text='Konec', width=8, height=1, command=self.konec)
        #self.butk.pack(side=LEFT, padx=4, pady=1)


#Novy kontakt
        #levy frame
        self.levy_kontakt = Frame(self.nk, borderwidth=2, relief=GROOVE)
        self.levy_kontakt.pack(fill=BOTH, expand=1, side=LEFT, padx=2, pady=2)
        
            #jmeno
        self.frjmeno=Frame(self.levy_kontakt, borderwidth=2, relief=GROOVE)
        self.frjmeno.pack(anchor=W, side=TOP)
        self.lajmeno=Label(self.frjmeno, text="Jméno: ", width=10)
        self.lajmeno.pack(side=LEFT, padx=4, pady=2)
        self.jmeno = Entry(self.frjmeno, width=20)
        self.jmeno.pack(side=LEFT, padx=4, pady=2)
        
            #prijmeni
        self.frprijmeni=Frame(self.levy_kontakt, borderwidth=2, relief=GROOVE)
        self.frprijmeni.pack(anchor=W, side=TOP)
        self.laprijmeni=Label(self.frprijmeni, text="Příjmení: ", width=10)
        self.laprijmeni.pack(side=LEFT, padx=4, pady=2)
        self.prijmeni = Entry(self.frprijmeni, width=20)
        self.prijmeni.pack(side=LEFT, padx=4, pady=2)
            
            #ulice
        self.frulice=Frame(self.levy_kontakt, borderwidth=2, relief=GROOVE)
        self.frulice.pack(anchor=W, side=TOP)
        self.laulice=Label(self.frulice, text="Ulice: ", width=10)
        self.laulice.pack(side=LEFT, padx=4, pady=2)
        self.ulice = Entry(self.frulice, width=20)
        self.ulice.pack(side=LEFT, padx=4, pady=2)
        
            #cislo popisne
        self.frcp=Frame(self.levy_kontakt, borderwidth=2, relief=GROOVE)
        self.frcp.pack(anchor=W, side=TOP)
        self.lacp=Label(self.frcp, text="Číslo popisné: ", width=10)
        self.lacp.pack(side=LEFT, padx=4, pady=2)
        self.cp = Entry(self.frcp, width=20)
        self.cp.pack(side=LEFT, padx=4, pady=2)
        
            #mesto
        self.frmesto=Frame(self.levy_kontakt, borderwidth=2, relief=GROOVE)
        self.frmesto.pack(anchor=W, side=TOP)
        self.lamesto=Label(self.frmesto, text="Město: ", width=10)
        self.lamesto.pack(side=LEFT, padx=4, pady=2)
        self.mesto = Entry(self.frmesto, width=20)
        self.mesto.pack(side=LEFT, padx=4, pady=2)
        
            #PSC
        self.frpsc=Frame(self.levy_kontakt, borderwidth=2, relief=GROOVE)
        self.frpsc.pack(anchor=W, side=TOP)
        self.lapsc=Label(self.frpsc, text="PSČ: ", width=10)
        self.lapsc.pack(side=LEFT, padx=4, pady=2)
        self.psc = Entry(self.frpsc, width=20)
        self.psc.pack(side=LEFT, padx=4, pady=2)
            
            #pohlavi
        self.frpohlavi=Frame(self.levy_kontakt, borderwidth=2, relief=GROOVE)
        self.frpohlavi.pack(anchor=W, side=TOP, padx=52)
        self.zena=Radiobutton(self.frpohlavi, variable=self.v_radiobutton_kontakt_pohlavi, text="Žena", value=0)
        self.zena.pack(side=LEFT, padx=4, pady=2)
        self.muz=Radiobutton(self.frpohlavi, variable=self.v_radiobutton_kontakt_pohlavi, text="Muž", value=1)
        self.muz.pack(side=LEFT, padx=4, pady=2)
        
        
        #pravy frame
        self.pravy_kontakt = Frame(self.nk, borderwidth=2, relief=GROOVE)
        self.pravy_kontakt.pack(fill=BOTH, expand=1, side=RIGHT, padx=2, pady=2)
        
            #tel1
        self.frtel1=Frame(self.pravy_kontakt, borderwidth=2, relief=GROOVE)
        self.frtel1.pack(anchor=W, side=TOP)
        self.latel1=Label(self.frtel1, text="tel: ", width=10)
        self.latel1.pack(side=LEFT, padx=4, pady=2)
        self.tel1 = Entry(self.frtel1, width=20)
        self.tel1.pack(side=LEFT, padx=4, pady=2)
        
            #tel2
        self.frtel2=Frame(self.pravy_kontakt, borderwidth=2, relief=GROOVE)
        self.frtel2.pack(anchor=W, side=TOP)
        self.latel2=Label(self.frtel2, text="tel2: ", width=10)
        self.latel2.pack(side=LEFT, padx=4, pady=2)
        self.tel2 = Entry(self.frtel2, width=20)
        self.tel2.pack(side=LEFT, padx=4, pady=2)
        
            #tel3
        self.frtel3=Frame(self.pravy_kontakt, borderwidth=2, relief=GROOVE)
        self.frtel3.pack(anchor=W, side=TOP)
        self.latel3=Label(self.frtel3, text="tel3: ", width=10)
        self.latel3.pack(side=LEFT, padx=4, pady=2)
        self.tel3 = Entry(self.frtel3, width=20)
        self.tel3.pack(side=LEFT, padx=4, pady=2)
        
            #email
        self.fremail=Frame(self.pravy_kontakt, borderwidth=2, relief=GROOVE)
        self.fremail.pack(anchor=W, side=TOP)
        self.laemail=Label(self.fremail, text="email: ", width=10)
        self.laemail.pack(side=LEFT, padx=4, pady=2)
        self.email = Entry(self.fremail, width=20)
        self.email.pack(side=LEFT, padx=4, pady=2)
        
            #Poznamka ke kontaktu
        self.frpozn_kontakt=Frame(self.pravy_kontakt, borderwidth=2, relief=GROOVE)
        self.frpozn_kontakt.pack(anchor=W, side=TOP)
        self.lpozn_kontakt=Label(self.frpozn_kontakt, text="poznámka: ", width=10)
        self.lpozn_kontakt.pack(fill=Y, side=LEFT, padx=4, pady=0)
        self.pozn_kontakt = Text(self.frpozn_kontakt, width=40, height=7)
        self.pozn_kontakt.pack(fill=BOTH, expand=1, side=TOP, padx=4, pady=5)
        
            #tlacitka upravit a smazat
        self.tlacitka_kontakt = Frame(self.pravy_kontakt, borderwidth=2, relief=GROOVE)
        self.tlacitka_kontakt.pack(side=BOTTOM, padx=4, pady=1)
        self.bVymazat_kontakt = Button(self.tlacitka_kontakt, text='Vymazat', width=10, height=1, command=self.vymaz_nk)
        self.bVymazat_kontakt.pack(side=LEFT, padx=4, pady=1)
        self.bUpravit_kontakt = Button(self.tlacitka_kontakt, text='Uložit', width=10, height=1, command=self.pridat_kontakt)
        self.bUpravit_kontakt.pack(side=LEFT, padx=4, pady=1)

#Vsechny poznamky
        #pravy frame
        self.pravy_poznamky = Frame(self.vp, borderwidth=2, relief=GROOVE)
        self.pravy_poznamky.pack(fill=BOTH, expand=1, side=RIGHT, padx=2, pady=2)
            
            #Poznamka
        self.lpoznamky=Label(self.pravy_poznamky, text="Text poznámky: ")
        self.lpoznamky.pack(fill=Y, side=TOP, padx=8, pady=0)
        self.poznamky = Text(self.pravy_poznamky, width=40, height=10)
        self.poznamky.pack(fill=BOTH, expand=1, side=TOP, padx=0, pady=5)
        
            #Datum
        self.frdatumy=Frame(self.pravy_poznamky, borderwidth=2, relief=GROOVE)
        self.frdatumy.pack()
        self.ladatumy=Label(self.frdatumy, text="Datum vytvoření: ")
        self.ladatumy.pack(side=LEFT, padx=2, pady=2)
        self.datumy = Label(self.frdatumy, textvariable=self.v_datumy, width=15)
        self.datumy.pack(side=LEFT, padx=4, pady=2)
        
            #tlacitka upravit a smazat
        self.tlacitka_poznamky = Frame(self.pravy_poznamky, borderwidth=2, relief=GROOVE)
        self.tlacitka_poznamky.pack(side=BOTTOM, padx=4, pady=1)
        self.bVymazat_poznamky = Button(self.tlacitka_poznamky, text='Vymazat', width=10, height=1, command=self.vymaz_vp)
        self.bVymazat_poznamky.pack(side=LEFT, padx=4, pady=1)
        self.bUpravit_poznamky = Button(self.tlacitka_poznamky, text='Uložit', width=10, height=1, command=self.upravit_poznamku)
        self.bUpravit_poznamky.pack(side=LEFT, padx=4, pady=1)
        
        #levy frame
        self.levy_poznamky = Frame(self.vp, borderwidth=2, relief=GROOVE)
        self.levy_poznamky.pack(fill=BOTH, expand=0, side=LEFT, padx=4, pady=2)
        
        self.frpoznazvy=Frame(self.levy_poznamky, borderwidth=2, relief=GROOVE)
        self.frpoznazvy.pack(anchor=W, side=TOP)
        
        #--dodelat scroollbar
        #self.radio_vyber_poznamky()
        self.vypis_vsechny_poznamky()

#Vsechny kontakty
        #pravy frame
        self.pravy_kontakty = Frame(self.vk, borderwidth=2, relief=GROOVE)
        self.pravy_kontakty.pack(fill=BOTH, expand=1, side=RIGHT, padx=2, pady=2)
            
            #jmeno, prijmeni
        self.frjmeno_kontakty=Frame(self.pravy_kontakty, borderwidth=2, relief=GROOVE)
        self.frjmeno_kontakty.pack(anchor=W, fill=X, side=TOP)
        self.lajmeno_kontakty=Label(self.frjmeno_kontakty, text="Jméno: ", width=10)
        self.lajmeno_kontakty.pack(side=LEFT, padx=4, pady=2)
        self.jmeno_kontakty = Entry(self.frjmeno_kontakty, width=20)
        self.jmeno_kontakty.pack(side=LEFT, fill=X, expand=1, padx=4, pady=2)
        
        self.laprijmeni_kontakty=Label(self.frjmeno_kontakty, text="Příjmení: ", width=10)
        self.laprijmeni_kontakty.pack(side=LEFT, padx=4, pady=2)
        self.prijmeni_kontakty = Entry(self.frjmeno_kontakty, width=20)
        self.prijmeni_kontakty.pack(side=LEFT, fill=X, expand=1, padx=4, pady=2)
            
            #ulice, cislo popisne
        self.frulice_kontakty=Frame(self.pravy_kontakty, borderwidth=2, relief=GROOVE)
        self.frulice_kontakty.pack(anchor=W, fill=X, side=TOP)
        self.laulice_kontakty=Label(self.frulice_kontakty, text="Ulice: ", width=10)
        self.laulice_kontakty.pack(side=LEFT, padx=4, pady=2)
        self.ulice_kontakty = Entry(self.frulice_kontakty, width=20)
        self.ulice_kontakty.pack(side=LEFT, fill=X, expand=1,  padx=4, pady=2)
        
        self.lacp_kontakty=Label(self.frulice_kontakty, text="Číslo popisné: ", width=10)
        self.lacp_kontakty.pack(side=LEFT, padx=4, pady=2)
        self.cp_kontakty = Entry(self.frulice_kontakty, width=20)
        self.cp_kontakty.pack(side=LEFT, fill=X, expand=1,  padx=4, pady=2)
        
            #mesto, PSC
        self.frmesto_kontakty=Frame(self.pravy_kontakty, borderwidth=2, relief=GROOVE)
        self.frmesto_kontakty.pack(anchor=W, fill=X, side=TOP)
        self.lamesto_kontakty=Label(self.frmesto_kontakty, text="Město: ", width=10)
        self.lamesto_kontakty.pack(side=LEFT, padx=4, pady=2)
        self.mesto_kontakty = Entry(self.frmesto_kontakty, width=20)
        self.mesto_kontakty.pack(side=LEFT, fill=X, expand=1,  padx=4, pady=2)
        
        self.lapsc_kontakty=Label(self.frmesto_kontakty, text="PSČ: ", width=10)
        self.lapsc_kontakty.pack(side=LEFT, padx=4, pady=2)
        self.psc_kontakty = Entry(self.frmesto_kontakty, width=20)
        self.psc_kontakty.pack(side=LEFT, fill=X, expand=1,  padx=4, pady=2)
            
            #pohlavi
        self.frpohlavi_kontakty=Frame(self.pravy_kontakty, borderwidth=2, relief=GROOVE)
        self.frpohlavi_kontakty.pack(anchor=N, side=TOP)
        self.zena_kontakty=Radiobutton(self.frpohlavi_kontakty, variable=self.v_radiobutton_kontakty_pohlavi, text="Žena", value=0)
        self.zena_kontakty.pack(side=LEFT, padx=4, pady=2)
        self.muz_kontakty=Radiobutton(self.frpohlavi_kontakty, variable=self.v_radiobutton_kontakty_pohlavi, text="Muž", value=1)
        self.muz_kontakty.pack(side=LEFT,  padx=4, pady=2)
        
            #tel1, tel2
        self.frtel1_kontakty=Frame(self.pravy_kontakty, borderwidth=2, relief=GROOVE)
        self.frtel1_kontakty.pack(anchor=W, fill=X, side=TOP)
        self.latel1_kontakty=Label(self.frtel1_kontakty, text="tel: ", width=10)
        self.latel1_kontakty.pack(side=LEFT, padx=4, pady=2)
        self.tel1_kontakty = Entry(self.frtel1_kontakty, width=20)
        self.tel1_kontakty.pack(side=LEFT, fill=X, expand=1,  padx=4, pady=2)
        
        self.latel2_kontakty=Label(self.frtel1_kontakty, text="tel2: ", width=10)
        self.latel2_kontakty.pack(side=LEFT, padx=4, pady=2)
        self.tel2_kontakty = Entry(self.frtel1_kontakty, width=20)
        self.tel2_kontakty.pack(side=LEFT, fill=X, expand=1,  padx=4, pady=2)
        
            #tel3, email
        self.frtel3_kontakty=Frame(self.pravy_kontakty, borderwidth=2, relief=GROOVE)
        self.frtel3_kontakty.pack(anchor=W, fill=X, side=TOP)
        self.latel3_kontakty=Label(self.frtel3_kontakty, text="tel3: ", width=10)
        self.latel3_kontakty.pack(side=LEFT, padx=4, pady=2)
        self.tel3_kontakty = Entry(self.frtel3_kontakty, width=20)
        self.tel3_kontakty.pack(side=LEFT, fill=X, expand=1,  padx=4, pady=2)
        
        self.laemail_kontakty=Label(self.frtel3_kontakty, text="email: ", width=10)
        self.laemail_kontakty.pack(side=LEFT, padx=4, pady=2)
        self.email_kontakty = Entry(self.frtel3_kontakty, width=20)
        self.email_kontakty.pack(side=LEFT, fill=X, expand=1,  padx=4, pady=2)
        
            #Poznamka ke kontaktu
        self.frpozn_kontakty=Frame(self.pravy_kontakty, borderwidth=2, relief=GROOVE)
        self.frpozn_kontakty.pack(anchor=W, expand=1, fill=BOTH, side=TOP)
        self.lpozn_kontakty=Label(self.frpozn_kontakty, text="poznámka: ", width=10)
        self.lpozn_kontakty.pack(fill=Y, side=LEFT, padx=8, pady=0)
        self.poznamky_kontakty = Text(self.frpozn_kontakty, width=25, height=4)
        self.poznamky_kontakty.pack(fill=BOTH, expand=1, side=LEFT, padx=0, pady=5)
        
            #tlacitka upravit a smazat
        self.tlacitka_kontakty = Frame(self.pravy_kontakty, borderwidth=2, relief=GROOVE)
        self.tlacitka_kontakty.pack(anchor=N, side=TOP, padx=4, pady=1)
        self.bVymazat_kontakty = Button(self.tlacitka_kontakty, text='Vymazat', width=10, height=1, command=self.vymaz_vk)
        self.bVymazat_kontakty.pack(side=LEFT, padx=4, pady=1)
        self.bUpravit_kontakty = Button(self.tlacitka_kontakty, text='Uložit', width=10, height=1, command=self.upravit_kontakt)
        self.bUpravit_kontakty.pack(side=LEFT, padx=4, pady=1)
        
        #levy frame
        self.levy_kontakty = Frame(self.vk, borderwidth=2, relief=GROOVE)
        self.levy_kontakty.pack(fill=BOTH, expand=0, side=LEFT, padx=4, pady=2)
        
        self.frkonnazvy=Frame(self.levy_kontakty, borderwidth=2, relief=GROOVE)
        self.frkonnazvy.pack(anchor=W, side=TOP)
        
        #--dodelat scroollbar
        #self.radio_vyber_kontakty()
        self.vypis_vsechny_kontakty()



root = Tix.Tk()
app = myApp(root)
root.mainloop()
