#------------------------------------------------------------------------------#
# Kalkulacka                                                                   #
#------------------------------------------------------------------------------#

from Tkinter import * 
from math import *
import tkFont

def callback(f, *a):
    """
    Vraci funkci f s nastavenym parametrem a.
    Tato funkce umi osetrit i vice nez jeden parametr.

    Pouziti:
        callback(funkce, arg1, arg2, ..., argn )
    """
    return lambda: f(*a)

class MyApp:

    def big(self):
        self.font.config(size=12, weight="bold")
        print "big"

    def normal(self):
        self.font.config(size=12, weight="normal")
        print "normal" 

    def vysledek(self):
        print "vysledek"
        try:
            vysledek = eval(self.text)
            self.clc()
            self.la.config(text=vysledek)
        except Exception:
            print "Chyba!!!",  sys.exc_info()[0]
            self.la.config(text='Error')
            self.clc()
            

    def insKey(self, znak):
        print "insKey: \""+ znak + "\""
	if znak == ',':
		znak = '.'
		self.carka.config(state=DISABLED);
        if znak == '=':
            self.vysledek()
        elif znak == '+' or znak == '-' or znak == '*' or  znak == '/':
            #pokud nebyl jeden z techto znaku zadan jako posledi, pak jej lze zadat
            self.carka.config(state=NORMAL)
	    if self.operator != True:
                 self.text = self.text + znak
                 print self.text
                 self.la.config(text=self.text)
            self.operator = True		
        else:
            #pokud je carka za operatorem nebo jako prvni znak, pak ji predchazi nula
            if znak == '.' and self.text == '' or znak == '.' and self.operator:
                self.text = self.text + '0' + '.'
                self.la.config(text=self.text);
            else:
                self.text = self.text + znak
                self.la.config(text=self.text)
            self.operator = False
            print self.text
    def clc(self):
        print "vymazani obsahu okna a promenne s obsahem okna"
        self.fo = "0"
        self.text = ""
	self.carka.config(state=NORMAL)
        self.la.config(text=self.fo)
        
        
    def __init__(self, root):
        self.text = ""
        self.operator = False
        self.fo = StringVar()
        self.fo = "0"
        root.title("Calculator")
        self.font = tkFont.Font(size=10, weight="normal")

        self.la = Label(root, text=self.fo, background="#ffffff", anchor=E, relief=SUNKEN, height=2, font=self.font)
        self.la.pack(fill=X, side=TOP, padx=8, pady=5)
    
        self.opts = Frame(root, relief=GROOVE)
        self.opts.pack()
        
        self.nor = Radiobutton(self.opts, text="Normal", variable=self.fo, value="normal", command=self.normal, font=self.font)
        self.nor.pack(side=LEFT)

        self.nol = Radiobutton(self.opts, text="Big", variable=self.fo, value="big", command=self.big, font=self.font)
        self.nol.pack(side=RIGHT)
        
        self.numbts = Frame(root)
        self.numbts.pack(fill=BOTH, expand=1, padx=4, pady=4)
        #roztahovani
        self.numbts.rowconfigure(0, weight=1)
        self.numbts.columnconfigure(0, weight = 1)
        self.numbts.rowconfigure(1, weight=1)
        self.numbts.columnconfigure(1, weight = 1)
        self.numbts.rowconfigure(2, weight=1)
        self.numbts.columnconfigure(2, weight = 1)
        self.numbts.rowconfigure(3, weight=1)
        self.numbts.columnconfigure(3, weight = 1)
        self.numbts.rowconfigure(4, weight=1)
        self.numbts.columnconfigure(4, weight = 1)
        

        self.cls = Button(self.numbts, text="Cls", width=5, height=2, font=self.font, command=self.clc)
        self.cls.grid(row=0, column=0, sticky=W+E+N+S, padx=2, pady=2)

        self.lomeno = Button(self.numbts, text="/", width=5, height=2, font=self.font, command=callback(self.insKey, "/"))
        self.lomeno.grid(row=0, column=1, sticky=W+E+N+S, padx=2, pady=2)

        self.krat = Button(self.numbts, text="*", width=5, height=2, font=self.font, command=callback(self.insKey, "*"))
        self.krat.grid(row=0, column=2, sticky=W+E+N+S, padx=2, pady=2)

        self.minus = Button(self.numbts, text="-", width=5, height=2, font=self.font, command=callback(self.insKey, "-"))
        self.minus.grid(row=0, column=3, sticky=W+E+N+S, padx=2, pady=2)



        self.cislo7 = Button(self.numbts, text="7",width=5, height=2, font=self.font, command=callback(self.insKey, "7"))
        self.cislo7.grid(row=1, column=0,sticky=W+E+N+S, padx=2, pady=2)

        self.cislo8 = Button(self.numbts, text="8",width=5, height=2, font=self.font, command=callback(self.insKey, "8"))
        self.cislo8.grid(row=1, column=1,sticky=W+E+N+S, padx=2, pady=2)

        self.cislo9 = Button(self.numbts, text="9",width=5, height=2, font=self.font, command=callback(self.insKey, "9"))
        self.cislo9.grid(row=1, column=2,sticky=W+E+N+S, padx=2, pady=2)
        


        self.cislo4 = Button(self.numbts, text="4",width=5, height=2, font=self.font, command=callback(self.insKey, "4"))
        self.cislo4.grid(row=2, column=0,sticky=W+E+N+S, padx=2, pady=2)

        self.cislo5 = Button(self.numbts, text="5",width=5, height=2, font=self.font, command=callback(self.insKey, "5"))
        self.cislo5.grid(row=2, column=1,sticky=W+E+N+S, padx=2, pady=2)

        self.cislo6 = Button(self.numbts, text="6",width=5, height=2, font=self.font, command=callback(self.insKey, "6"))
        self.cislo6.grid(row=2, column=2,sticky=W+E+N+S, padx=2, pady=2)



        self.cislo1 = Button(self.numbts, text="1",width=5, height=2, font=self.font, command=callback(self.insKey, "1"))
        self.cislo1.grid(row=3, column=0,sticky=W+E+N+S, padx=2, pady=2)

        self.cislo2 = Button(self.numbts, text="2",width=5, height=2, font=self.font, command=callback(self.insKey, "2"))
        self.cislo2.grid(row=3, column=1,sticky=W+E+N+S, padx=2, pady=2)

        self.cislo3 = Button(self.numbts, text="3",width=5, height=2, font=self.font, command=callback(self.insKey, "3"))
        self.cislo3.grid(row=3, column=2,sticky=W+E+N+S, padx=2, pady=2)



        self.cislo0 = Button(self.numbts, text="0",width=5, height=2, font=self.font, command=callback(self.insKey, "0"))
        self.cislo0.grid(row=4, column=0, columnspan=2,sticky=W+E+N+S, padx=2, pady=2)

        self.carka = Button(self.numbts, text=",",width=5, height=2, font=self.font, command=callback(self.insKey, ","))
        self.carka.grid(row=4, column=2,sticky=W+E+N+S, padx=2, pady=2)


                
        self.plus = Button(self.numbts, text="+",width=5, height=2, font=self.font, command=callback(self.insKey, "+"))
        self.plus.grid(row=1, column=3, rowspan=2,sticky=W+E+N+S, padx=2, pady=2)

        self.plus = Button(self.numbts, text="=",width=5, height=2, font=self.font, command=callback(self.insKey, "="))
        self.plus.grid(row=3, column=3, rowspan=2,sticky=W+E+N+S, padx=2, pady=2)
        
        self.nor.select()

root = Tk()
app = MyApp(root)
root.mainloop()
root.destroy()
