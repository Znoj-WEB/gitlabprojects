### **Description**

Python mini programs for setting GUI in python

---

### **Technology**

Python

---

### **Year**

2013

---

### **Screenshots**

## <div align="center">

  <img src="./README/kalkulacka.py" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">Calculator</img>
</div>

<div align="center">
  <img src="./README/kalkulacka.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
## <div align="center">

  <img src="./README/rovnice_final.py" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">Quadratic equation</img>
</div>

<div align="center">
  <img src="./README/rovnice.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/rovnice_error.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
## <div align="center">

  <img src="./README/zno0011_form.py" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">Form</img>
</div>

<div align="center">
  <img src="./README/formNote.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/formAddress.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
## <div align="center">

  <img src="./README/projekt_zno0011_1.1.py" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">Notes and Contacts</img>
</div>

<div align="center">
  <img src="./README/project_newNote.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/project_notes.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/project_newContact.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/project_allContacts.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
## <div align="center">

  <img src="./README/vehicleDatabase.py" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">Vehicles database</img>
</div>

<div align="center">
  <img src="./README/vehicleDatabaseBasics.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/vehicleDatabaseExtended.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/vehicleDatabaseNotes.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  