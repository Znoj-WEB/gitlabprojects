- [**Description**](#description)
- [**Technology**](#technology)
- [**Year**](#year)
- [**Specifikace zadání**](#specifikace-zadani)
  - [**Diagram případů užití**](#diagram-pripadu-uziti)
- [**Konceptuální model**](#konceptualni-model)
  - [**Lineární zápis seznamu typů entit a jejich atributů**](#linearni-zapis-seznamu-typu-entit-a-jejich-atributu)
  - [**ER diagram**](#er-diagram)
- [**Datový model**](#datovy-model)
- [**Funkční analýza**](#funkcni-analyza)
  - [Seznam funkcí](#seznam-funkci)
  - [Detailní popis funkcí](#detailni-popis-funkci)
- [**Docs**](#docs)
- [**SQL scripts**](#sql-scripts)

### **Description**

Implementace části IS navrženého v [MHD - návrh IS a SQL skripty](https://gitlab.com/Znoj-Desktop/MHD)

---

### **Technology**

C#, .NET

---

### **Year**

2013

---

### **Specifikace zadání**

**PROČ**  
Je potřeba vytvořit komplexní informační systém městské hromadné dopravy, který bude sloužit jak pro vnitřní, tak pro vnější účely.  
**K ČEMU**  
Tento informační systém bude sloužit k vyhledávání spojů městské hromadné dopravy podle různých kritérií, jakými jsou například typ vozidla, jestli vozidlo umožňuje bezbariérový přístup, vyhledávání jen v zastávkách umožňujících bezbariérový přístup do vozidla a zároveň bude sloužit k evidenci řidičů a k práci s jejich směnami.
**KDO**  
Se systémem bude moci pracovat každý, kdo si chce vyhledat nějaký spoj městské hromadné dopravy, řidiči vozidel městské hromadné dopravy, správce systému a také zaměstnavatel řidičů.  
**VSTUPY**  
U řidičů evidujeme rodné číslo, jméno, příjmení, bydliště, telefonní číslo, pohlaví, heslo a plat.
U směn evidujeme typ – tedy název směny, v kolik začíná, v kolik končí a kolik je to dohromady hodin.
U vozidel evidujeme typ – jestli se jedná o tramvaj, autobus, metro, trolejbus nebo vlak, dále pak jestli je vozidlo s bariérovým přístupem, značku, max. počet cestujících a řidiče, který se o vozidlo stará.
Dále evidujeme, jací řidiči jezdí s jakými vozidly a od kolika, do kolika hodin jezdí vozidlo určitou linku.
U zastávek evidujeme identifikační číslo zastávky, její název a možnost bezbariérového přístup.
Dále pak v kolik hodin zastavuje na konkrétní zastávce která linka.
U zón evidujeme číslo zóny a pak jestli se jedná o městskou, nebo mimoměstskou oblast.
U linek evidujeme číslo linky a typ linky (vozidla jakého typu po ní jezdí – tramvaj/autobus/metro/trolejbus/vlak)  
**VÝSTUPY**
Výstupem pro správce nebo zaměstnavatele budou všichni řidiči, všechna vozidla, jaký řidič se stará o jaké vozidlo, jací řidiči řídí jaká vozidla, všechny zastávky, linky, zóny.
Pro nepřihlášeného uživatele bude výstupem spojení dle zadaných kritérií včetně všech zastávek, které leží mezi zadanou vstupní a výstupní zastávkou, všechny odjezdy ze zadané zastávky, všechny spoje z dané zastávky.
Pro řidiče se zobrazí rozpis směn, všechny detaily o vozidle, o které se stará, všechny směny včetně jejich detailů.  
**OKOLÍ**  
Kdokoli, kdo se k aplikaci dostane, může v aplikaci vyhledávat spoje.
Pracovníci městské hromadné dopravy pak používají aplikaci pro vnitřní účely.  

#### **Diagram případů užití**

<div align="center">
  <img src="./README/usecase2.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Konceptuální model**

#### **Lineární zápis seznamu typů entit a jejich atributů**

**Entity**  
Ridic (rc, jmeno, prijmenu, bydliste, tel, pohlavi, heslo, plat)  
Vozidlo (idVozidla, tyo, znacka, maxPocet, barierove, rc)  
Linka (cisloLinky, typLinky)  
Zastavka (idZastavky, nazev, barierova, cisloZony)  
Zona (cisloZony, mesto)  
Smena (typSmeny, začátek, konec, hodin)  
VozidloLinka (od, do, idVozidla, cisloLinky)  
ZastavkaLinkaCas (cas, cisloLinky, idZastavky)  
RozpisSmen (datum, jmenoSmeny, rc, typSmeny)  

**Vztahy**  
stara_se_o (Ridic, Vozdilo) 1:1  
jezdi (Vozdilo, VozidloLinka) 1:N  
ma (Linka, VozidloLinka) 1:N  
se sklada (Linka, ZastavkaLinkaCas) 1:N  
zastavuje (Zastavka, zastavkaLinkaCas) 1:N  
obsahuje (Zona, Zastavka) 1:N  
chodi (Ridic, RozpisSmen) 1:N  
je v (Smena, RozpisSmen) 1:N  

#### **ER diagram**

<div align="center">
  <img src="./README/er2.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

### **Datový model**

<div align="center">
  <img src="./README/datovy_slovnik.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

...  
NN = not null, N = null, PK = primární klíč, FK = cizí klíč

---

### **Funkční analýza**

#### Seznam funkcí

1. Evidence směn  
Tabulka: Smena, RozpisSmen, Ridic
1.1. Přidělení směn řidičům  
Zodpovědnost: Systém automaticky  
1.2. Změnit směny  
Zodpovědnost: Zaměstnavatel
1.3. Zobrazit směny  
Zodpovědnost: Zaměstnavatel, Řidič  

2. Evidence vozidel  
Tabulka: Vozidlo, Ridic, VozidloLinka  
Zodpovědnost: Správce, Zaměstnavatel  
2.1. Přidat dopravní prostředek  
2.2. Odebrat dopravní prostředek  
2.3. Seznam dopravních prostředků  
2.4. Zobrazit dopravní prostředek  
Zodpovědnost: Správce, Zaměstnavatel, Řidič  

3. Evidence řidičů  
Tabulka: Ridic  
Zodpovědnost: Správce, Zaměstnavatel  
3.1. Přidat řidiče  
3.2. Změnit detaily o řidiči  
Tabulka: Ridic, Vozidlo  
Zodpovědnost: Správce, Zaměstnavatel, Řidič (řidič mění jen svoje detaily)  
3.3. Odebrat řidiče  
Tabulka: Ridic, Vozidlo, RozpisSmen  
3.4. Seznam řidičů  
3.5. Zobrazit řidiče  

4. Evidence zastávek  
Tabulka: Zastavka, ZastavkaLinkaCas, Linka, VozidloLinka  
Zodpovědnost: Správce, Zaměstnavatel  
4.1. Přidat zastávku  
4.2. Zrušit zastávku  
4.3. Zobrazit detail vybrané zastávky  
Zodpovědnost: Správce, Zaměstnavatel, Uživatel  
4.4. Vyhledat zastávku  
Zodpovědnost: Správce, Zaměstnavatel, Uživatel  

5. Evidence linek  
Tabulka: Linka, VozidloLinka, ZastavkaLinkaCas  
Zodpovědnost: Správce, Zaměstnavatel  
5.1. Přidat linku  
5.2. Zrušit linku  
5.3. Seznam linek  
Zodpovědnost: Správce, Zaměstnavatel, Uživatel  
5.4. Seznam zastávek ležících na dané lince  
Zodpovědnost: Správce, Zaměstnavatel, Uživatel  
5.5. Zobrazit detail o lince  

6. Evidence zón  
Tabulka: Zóna
Zodpovědnost: Správce, Zaměstnavatel  
6.1. Přidat zónu  
6.2. Změnit zónu  
6.3. Seznam zón  
Zodpovědnost: Správce, Zaměstnavatel, Uživatel  

7. Vyhledat spoj  
Tabulka: Zastavka, ZastavkaLinkaCas, Linka, VozidloLinka  
Zodpovědnost: Uživatel  

8. Vyhledat zpáteční spoj  
Tabulka: Zastavka, ZastavkaLinkaCas, Linka, VozidloLinka  
Zodpovědnost: Uživatel  

9. Výpočet jízdného podle času  
Tabulka: Zona, Zastavka, ZastavkaLinkaCas, Linka, VozidloLinka  
Zodpovědnost: Uživatel  

- vyhledá se spoj, podle délky spoje se určí cena  

#### Detailní popis funkcí

1. Evidence směn  
1.1. Přidělení směn řidičům  
  neimplementováno
1.2.    Změnit směny  
  neimplementováno  
2. Evidence vozidel  
2.1. Přidat dopravní prostředek  
  Proměnná `@typLinky` obsahuje typ linky, kterou chceme přidat  
  Proměnná `@cisloL` obsahuje číslo linky, kterou chceme přidat.  
  Proměnná `@idVozidla` udává id vozidla, které chci přidat.  
  Proměnná `@typVozidla` udává typ vozidla, které chci přidat.  
  Proměnná `@znacka` udává značku přidávaného vozidla.  
  Proměnná `@maxPocet` udává kapacitu vozidla.  
  Proměnná `@barierove` udává příznak, jestli se jedná o bezbariérové vozidlo.  
  Proměnná `@rc` obsahuje rodné číslo řidiče, který je vybrán jako řidič, který se o dané vozidlo stará.  
  Proměnná `@od` udává čas, od kdy jede dané vozidlo danou linku  
  Proměnná `@do` udává čas, do kdy jede dané vozidlo danou linku  

  ```
  INSERT INTO Vozidlo VALUES (@idVozidla, @typVozidla, @znacka, @maxPocet, @barierove, @rc);  
  INSERT INTO VozidloLinka VALUES(@od, @do, @idVozidla,@cisloL);
  ```

  Metoda bude transakce.  

2.2. Odebrat dopravní prostředek  
  Proměnná `@idVozidla` obsahuje id vozidla, které chceme odebrat.  
  Je potřeba odebrat veškeré vazby vozidla na jiné tabulky, pak teprve dané vozidlo.  

  ```
  DELETE FROM VozidloLinka WHERE idVozidla=@idVozidla; 
  DELETE FROM Vozidlo WHERE idVozidla=@idVozidla;
  ```

  Metoda bude transakce.  

3. Evidence řidičů  
3.3. Odebrat Řidiče  
  neimplementováno  
4. Evidence zastávek  
4.1. Přidat zastávku  
  Proměnná `@cisloZony`, udává číslo zóny, ve které daná zastávka leží.  
  Proměnná `@cisloLinky`, udává linku, která bude na zastávce zastavovat.  
  Proměnná `@idZastavky` udává idZastávky, kterou přidáváme.  
  Proměnná `@cas` udává čas, ve který bude na dané zastávce stavět vybraná linka.  
  Proměnná `@nazev` udává název přidávané zastávky.  
  Proměnná `@barierova` nese příznak, jestli je daná zastávka bariérová či nikoli.  

  ```
  INSERT INTO Zastavka VALUES(@idZastavky, @nazev, @barierova, @cisloZony);  
  INSERT INTO ZastavkaLinkaCas VALUES(@cas, @cisloLinky, @idZastavky);
  ```

  Metoda bude transakce.  

4.2. Zrušit zastávku  
  Proměnná `@idZastavky` udává id zastávky, kterou chceme odebrat.  
  Nejdříve je třeba zrušit veškeré vazby do tabulky ZastavkaLinkaCas.  

  ```
  DELETE FROM ZastavkaLinkaCas  WHERE idZastavky=@idZastavky; 
  DELETE FROM Zastavka WHERE idZastavky=@idZastavky;
  ```

  Metoda bude transakce.  

5. Evidence linek  
5.1. Přidat linku  
5.2. Změnit informace o lince  
  neimplementováno  
5.3. Zrušit linku  
 Proměnná `@cisloLinky` udává číslo linky, kterou chceme zrušit.  
 Nejdříve je třeba odebrat veškeré výskyty v tabulce VozidloLinka a ZastavkaLinkaCas, které linka má.  

  ```
  DELETE FROM VozidloLinka WHERE cisloLinky=@cisloLinky;
  DELETE FROM ZastavkaLinkaCas WHERE cisloLinky=@cisloLinky; 
  DELETE FROM Linka WHERE cisloLinky=@cisloLinky;
  ```

  Metoda bude transakce.  

5.5. Seznam zastávek ležících na dané lince  
 Proměnná `@cisloLink`y udává číslo linky, která je vybraná  

  ```
  SELECT z.nazev FROM Zastavka z 
  join ZastavkaLinkaCas zlc on zlc.idZastavky = z.idZastavky 
  where zlc.cisloLinky=@cisloLinky GROUP BY z.nazev
  ```

6. Vyhledat spoj  
  Proměnná `@nazev_odjezdu` udává název zastávky, ve které bude spoj začínat.  
  Proměnná `@nazev_prijezdu` udává název zastávky, ve které bude spoj končit.  
  Proměnná `@cas_zadan` udává čas, od kterého chceme spojení vyhledat.  
  Proměnná `@cas_odjezdu` udává čas, kdy vyjíždí daný spoj ze zastávky `@nazevev_odjezdu`.
  Proměnná `@cas_prijezdu` udává čas, kdy dorazí daný spoj do zastávky `@nazev_prijezdu`.  
  Proměnná `@zastavka_prijezdu` obsahuje idZastavky pro zastávku odjezdu.  
  Proměnná `@zastavka_odjezdu` obsahuje idZastavky pro zastávku příjezdu.  
  Proměnná `@linka_odjezdu` udává linku, na které leží zastávka @nazev_odjezdu.  
  Proměnná `@linka_prijezdu` udává linku, na které leží zastávka @nazev_prijezdu.
  Pokud zastávka `@nazev_prijezdu` i zastávka `@nazev_odjezdu` leží na téže lince, pak se vyhledá nejbližší zastávka, na které lze nastoupit a čas kdy vystoupit.  

  ```
  select @cas_odjezdu=cas, @linka_odjezdu=cisloLinky from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_odjezdu and cas>=@cas_zadan and zlc.cisloLinky in (select zlc.cisloLinky from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_prijezdu) order by cas desc;
  
  select @cas_prijezdu=cas, @linka_prijezdu=cisloLinky from Zastavka z join  zlc.cisloLinky=@linka_odjezdu and cas>=@cas_odjezdu order by cas desc;
  ```

7. Vyhledat zpáteční spoj  
  Zavolá se metoda 6. Vyhledat spoj s přehozenou zastávkou příjezdu a odjezdu.  

8. Výpočet jízdného podle času  
  Zavolá se metoda 6. Vyhledat spoj, odečte se čas u zastávky příjezdu od času u zastávky odjezdu a porovná se s tabulkou pro výpočet jízdného.  
  ceník: do 15 minut = 15 Kč, do 30 minut 17 Kč, do 60 minut 20 Kč, nad 60 minut 30 Kč.  

### **Screenshot**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Docs**  

<div align="center">
  <img src="./README/MHD-zno0011.docx" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">MHD-zno0011 docx</img>
</div>
  
<div align="center">
  <img src="./README/MHD-zno0011.pdf" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">MHD-zno0011 pdf</img>
</div>
  
<div align="center">
  <img src="./README/mhd_dotazy.pdf" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">MHD SQL dotazy pdf</img>
</div>
  
---

### **SQL scripts**  

<div align="center">
  <img src="./README/MHD_create.sql" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">SQL script schema - create tables</img>
</div>
  
<div align="center">
  <img src="./README/MHD_init.sql" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">SQL script data - initialize</img>
</div>
  
<div align="center">
  <img src="./README/k_mhd.sql" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">SQL script procedures</img>
</div>
  