/*
Created: 15. 4. 2013
Modified: 16. 5. 2013
Model: MHD_ZNO0011
Database: MS SQL Server 2012
*/

-- Create tables section -------------------------------------------------

-- Table Ridic

CREATE TABLE [Ridic]
(
 [rc] Bigint NOT NULL PRIMARY KEY,
 [jmeno] Varchar(30) NOT NULL,
 [prijmeni] Varchar(30) NOT NULL,
 [bydliste] Varchar(30) NOT NULL,
 [tel] Bigint NOT NULL,
 [pohlavi] Varchar(4) NULL,
 [heslo] Varchar(20) NOT NULL,
 [plat] Int NOT NULL
)
go

-- Table Vozidlo

CREATE TABLE [Vozidlo]
(
 [idVozidla] Int NOT NULL PRIMARY KEY IDENTITY,
 [typVozidla] Varchar(30) NOT NULL,
 [znacka] Varchar(30) NOT NULL,
 [maxPocet] Int NOT NULL,
 [barierove] Bit NULL,
 [rc] Bigint NOT NULL REFERENCES [Ridic]
)
go

-- Table Linka

CREATE TABLE [Linka]
(
 [cisloLinky] Int NOT NULL PRIMARY KEY IDENTITY,
 [typLinky] Varchar(30) NOT NULL
)
go

-- Table VozidloLinka

CREATE TABLE [VozidloLinka]
(
 [od] Time NOT NULL,
 [do] Time NOT NULL,
 [idVozidla] Int NOT NULL REFERENCES [Vozidlo],
 [cisloLinky] Int NOT NULL REFERENCES [Linka]
)
go

-- Table Zona

CREATE TABLE [Zona]
(
 [cisloZony] Int NOT NULL PRIMARY KEY,
 [mesto] Bit NOT NULL
)
go

-- Table Zastavka

CREATE TABLE [Zastavka]
(
 [idZastavky] Int NOT NULL PRIMARY KEY IDENTITY,
 [nazev] Varchar(30) NOT NULL,
 [barierova] Bit NULL,
 [cisloZony] Int NOT NULL REFERENCES [Zona]
)
go

-- Table Smena

CREATE TABLE [Smena]
(
 [typSmeny] Varchar(30) NOT NULL PRIMARY KEY,
 [zacatek] Time NOT NULL,
 [konec] Time NOT NULL,
 [hodin] Int NULL
)
go

-- Table RozpisSmen

CREATE TABLE [RozpisSmen]
(
 [datum] Date NOT NULL,
 [jmenoSmeny] Char(256) NOT NULL,
 [rc] Bigint NOT NULL REFERENCES [Ridic],
 [typSmeny] Varchar(30) NOT NULL REFERENCES [Smena]
)
go

-- Table ZastavkaLinkaCas

CREATE TABLE [ZastavkaLinkaCas]
(
 [cas] Time NOT NULL,
 [cisloLinky] Int NOT NULL REFERENCES [Linka],
 [idZastavky] Int NOT NULL REFERENCES [Zastavka],
)
go

-- Create relationships section ------------------------------------------------- 

--ALTER TABLE [VozidloLinka] ADD CONSTRAINT [jezdi] FOREIGN KEY ([idVozidla]) REFERENCES [Vozidlo] ([idVozidla])
--go

--ALTER TABLE [VozidloLinka] ADD CONSTRAINT [ma] FOREIGN KEY ([cisloLinky]) REFERENCES [Linka] ([cisloLinky])
--go

--ALTER TABLE [Zastavka] ADD CONSTRAINT [obsahuje] FOREIGN KEY ([cisloZony]) REFERENCES [Zona] ([cisloZony])
--go

--ALTER TABLE [Vozidlo] ADD CONSTRAINT [stara_se_o] FOREIGN KEY ([rc]) REFERENCES [Ridic] ([rc])
--go

--ALTER TABLE [RozpisSmen] ADD CONSTRAINT [chodi] FOREIGN KEY ([rc]) REFERENCES [Ridic] ([rc])
--go

--ALTER TABLE [RozpisSmen] ADD CONSTRAINT [je_v] FOREIGN KEY ([typSmeny]) REFERENCES [Smena] ([typSmeny])
--go

--ALTER TABLE [ZastavkaLinkaCas] ADD CONSTRAINT [se_sklada] FOREIGN KEY ([cisloLinky]) REFERENCES [Linka] ([cisloLinky])
--go

--ALTER TABLE [ZastavkaLinkaCas] ADD CONSTRAINT [zastavuje] FOREIGN KEY ([idZastavky]) REFERENCES [Zastavka] ([idZastavky])
--go

