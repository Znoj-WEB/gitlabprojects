### **Description**

Scripts for checking item existence in e-shop.  
Periodically check e-shop if the item from `.env` exists.

- when exists - email is sent, a notification will pop up, the webpage is open in Firefox, the item is put into a shopping cart, and shopping cart is opened.
- when not found - keep looking, number of requests is shown.

Install:

- NodeJS
- download GeckoDriver [](https://github.com/mozilla/geckodriver/releases/) or take it from folder assets
- put path to GeckoDriver into PATH [](https://en.wikipedia.org/wiki/PATH_%28variable%29)
- `npm i`

Setup:
create and fill `.env`:

```
EMAIL=@gmail.com
PASSWORD=
SENDTO=@gmail.com
WHATTOSEARCH=
```

Run:

- `npm start`

---

### **Technology**

TypeScript, NodeJS, WebDriver

---

### **Year**

2021

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/1.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
