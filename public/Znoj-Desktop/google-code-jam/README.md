# Google Code Jam

### **Description**

Google’s longest running global coding competition

---

### **Technology**

JavaScript

---

### **Year**

2020

---

# Structure

I use template.js which I created

- comment input and uncomment process.stdin for production

1. option to run script (with debugger)

- with extension <https://marketplace.visualstudio.com/items?itemName=miramac.vscode-exec-node&ssr=false#overview>
- run it with F8 (focus in file), stop it with F9
- for debbugger set in VS Code in Settings > `node debug` > Auto Attach: On

2. option with hot deploy:

- `npm i -g nodemon`
- `nodemon --inspect 2020_q1.js`

# Practise

## Qualification Round 2019

<https://codingcompetitions.withgoogle.com/codejam/round/0000000000051705>

| Name + description                          | result                                 |
| ------------------------------------------- | -------------------------------------- |
| Foregone Solution (6pts, 10pts, 1pts)       | got 16                                 |
| You Can Go Your Own Way (5pts, 9pts, 10pts) | got 24                                 |
| Cryptopangrams (10pts, 15pts)               | got 0 (visible tests passed)           |
| Dat Bae (14pts, 20pts)                      | got 0 (Pass known input, RuntimeError) |
| **Sum**                                     | **40b**                                |

## A1 Round 2019

<https://codingcompetitions.withgoogle.com/codejam/round/0000000000051635>

| Name + description          | result                                                                                                                                 |
| --------------------------- | -------------------------------------------------------------------------------------------------------------------------------------- |
| Pylons (8pts, 23pts)        | got 0 (POSSIBLE / IMPOSSIBLE and number of results the same as script provided by Google, all possible cases passed, checked manually) |
| Golf Gophers (11pts, 21pts) | got 11                                                                                                                                 |
| **Sum**                     | **11b**                                                                                                                                |

---

# Copetition

## Qualification Round 2020

<https://codingcompetitions.withgoogle.com/codejam/round/000000000019fd27>

| Name + description                         | result                                                              |
| ------------------------------------------ | ------------------------------------------------------------------- |
| Vestigium (7pts)                           | got 7                                                               |
| Nesting Depth (5pts, 11pts)                | got 16                                                              |
| Parenting Partnering Returns (7pts, 12pts) | got 19                                                              |
| ESAb ATAd (1pts, 9pts, 16pts)              | got 1                                                               |
| Indicium (7pts, 25pts)                     | got 0 (for first level all possible cases passed, checked manually) |
| **Sum**                                    | **43b**                                                             |
| **Rank**                                   | **7528 / 40698**                                                    |

## 1A Round 2020

<https://codingcompetitions.withgoogle.com/codejam/round/000000000019fd74>

| Name + description                   | result                    |
| ------------------------------------ | ------------------------- |
| Pattern Matching (5pts, 5pts, 18pts) | got 10                    |
| Pascal Walk (3pts, 11pts, 21pts)     | got 14                    |
| Square Dance (9pts, 28pts)           | got 0 (didn't even start) |
| **Sum**                              | **24b**                   |
| **Rank**                             | **5305 / 8987**           |

## 1B Round 2020

<https://codingcompetitions.withgoogle.com/codejam/round/000000000019fef2>

| Name + description                        | result                        |
| ----------------------------------------- | ----------------------------- |
| Expogo (5pts, 8pts, 16pts)                | got 13                        |
| Blindfolded Bullseye (3pts, 12pts, 19pts) | got 0 (didn't start)          |
| Join the Ranks (14pts, 23pts)             | got 0 (passed visible, got 0) |
| **Sum**                                   | **13b**                       |
| **Rank**                                  | **3982 / 6539**               |

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
