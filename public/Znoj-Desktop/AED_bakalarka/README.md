### **Description**

program for Bc. Jan Gronych's bachelor thesis to track AED's in Olomouc region

---

### **Technology**

C#

---

### **Year**

2015

---

### **Screenshot**

<div align="center">
  <img src="./README/screen.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
