### **Description**

script for getting non-malware software from publicly accessible sources like Studna, SourceForge or FileHippo

---

### **Technology**

C#

---

### **Year**

2017

---

### **Screenshot**

<div align="center">
  <img src="./README/goodwareDownloadScript.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
