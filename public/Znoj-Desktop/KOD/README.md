### **Description**

My text compression algorithm  
  
- reading input file and creating matrix
- saving matrix into file 'matrix'
  
---

- restoring matrix from the file
- creating output for printing coded message
- creating file 'fullyCoded' for printing coded message in chars for smaller size  
  
---

- opening file 'fullyCoded' for getting coded message in chars
- creating file 'input2' for printing decoded message - for control  
  
---

### **Technology**

C++

---

### **Year**

2015

---

### **Screenshot**

#### Compressed books  

<div align="center">
  <img src="./README/compare_size.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">compare_size</img>
</div>

<div align="center">
  <img src="./README/compare_size_hp7.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">compare_size_hp7</img>
</div>

<div align="center">
  <img src="./README/compare_size_pycha_a_predsudek.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">compare_size_pycha_a_predsudek</img>
</div>

#### Worst case  

<div align="center">
  <img src="./README/compare_size_worst_case.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">compare_size_worst_case</img>
</div>

#### Creating matrix  

<div align="center">
  <img src="./README/l_bin.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">l_bin</img>
</div>

<div align="center">
  <img src="./README/l_input.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">l_input</img>
</div>

<div align="center">
  <img src="./README/l_input_proporcion.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">l_input_proporcion</img>
</div>

<div align="center">
  <img src="./README/l_matrix.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">l_matrix</img>
</div>

#### Real output  

##### Matrix  

<div align="center">
  <img src="./README/matrix.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">matrix</img>
</div>
  
##### Output  

<div align="center">
  <img src="./README/output.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">output</img>
</div>
  
#### Output log  

<div align="center">
  <img src="./README/program_run.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">program_run</img>
</div>
