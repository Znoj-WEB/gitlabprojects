### **Description**
14 programs from school subject "Programming seminar"  
Tasks are from https://uva.onlinejudge.org  
  
110 - Meta-Loopless Sorts - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=46  
177 - Paper Folding - https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=113  
283 - Compress - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=219  
397 - Equation Elation - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=333  
594 - One Little, Two Little, Three Little Endians - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=535  
705 - Slash Maze - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=646  
706 - LC-Display - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=647  
10131 - Is Bigger Smarter? - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1072  
10139 - Factovisors - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1080  
10196 - Check The Check - https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=1137  
10249 - The Grand Dinner - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1190  
10261 - Ferry Loading - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1202  
10315 - Poker Hands - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1256  
11258 - String Partition - https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=2225  

---
### **Technology**
C/C++

---
### **Year**
2013
