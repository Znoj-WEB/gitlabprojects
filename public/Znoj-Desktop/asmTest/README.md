### **Description**

basics of ASM for a class "Computer viruses and security of computer systems"

---

### **Technology**

ASM

---

### **Year**

2017

---

### **Screenshot**

<div align="center">
  <img src="./README/screen.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
