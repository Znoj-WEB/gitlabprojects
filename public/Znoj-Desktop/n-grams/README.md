# n-grams - Bakalářská práce

- [n-grams - Bakalářská práce](#n-grams-bakalarska-prace)
  - [**Assignment**](#assignment)
  - [**Abstract**](#abstract)
  - [**Technology**](#technology)
  - [**Year**](#year)
  - [**Implementation**](#implementation)
    - [**B+ Tree**](#b-tree)
    - [**R-Tree**](#r-tree)
  - [**Presentations**](#presentations)
  - [**Text**](#text)

### **Assignment**

Implementace efektivních datových struktur - Naimplementujte vhodné datové struktury použitelné pro efektivní vyhledávání v datech. Tato data mohou být například n-gramy extrahované z textů, nebo sekvence DNA. Následně tyto datové struktury otestujte na vhodné datové kolekci a srovnejte výsledky. Pro implementaci použijte programovací jazyk C, nebo C++.

---

### **Abstract**

Práce se zabývá stromovými datovými strukturami. Především B+ stromy a R–stromy,
které jsou vhodné pro ukládání n–gramů a zejména pak k jejich vyhledávání. N–gramy jsou části textu používané například pro detekci plagiátorství, porovnání DNA, detekci spamu či překladu z jednoho jazyka do druhého. Práce se také zabývá implementací těchto dvou stromových struktur a jejich otestování na vhodných testovacích datech.

---

### **Technology**

C++

---

### **Year**

2014

---

### **Implementation**

#### **B+ Tree**

Ukázka štěpení B+ stromu při vložení n-gramu s klíčem 14  
<div align="center">
  <img src="./README/B+Tree_Example.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Ukázka výstupu metody UkazStrom()  
<div align="center">
  <img src="./README/B+Tree_UkazStrom.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Ukázka výstupu metody Vypis()  
<div align="center">
  <img src="./README/B+Tree_Vystup.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
#### **R-Tree**

Ukázka stromové struktury R–stromu  
<div align="center">
  <img src="./README/R-tree_T.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Jedna z možných planárních reprezentací dat R–stromu  
<div align="center">
  <img src="./README/R-tree_S.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Ukázka výstupu metody UkazStrom()  
<div align="center">
  <img src="./README/R_Tree_UkazStrom.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Ukázka výstupu metody VypisPlus()  
<div align="center">
  <img src="./README/r_Tree_VystupPlus.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
---

### **Presentations**  

<div align="center">
  <img src="./README/Implementace%20efektivn%C3%ADch%20datov%C3%BDch%20struktur.pptx" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Implementace efektivních datových struktur pptx</img>
</div>
  
<div align="center">
  <img src="./README/Implementace%20efektivn%C3%ADch%20datov%C3%BDch%20struktur.pdf" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Implementace efektivních datových struktur pdf</img>
</div>

### **Text**  

<div align="center">
  <img src="./README/bakalarka.pdf" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">bakalarka pdf</img>
</div>
