### **Description**

library for reading libraries from PE files based on "PE Maker" from Ashkbiz Danehkar  

From me:

- 64-bit files support
- upgraded to use MSVC2017 64b
- updated API
- bug fixes
- stability fixes, exceptions handling
- fixed memory leaks

---

### **Technology**

C++

---

### **Year**

2017

---

### **Screenshot**

<div align="center">
  <img src="./README/library.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
