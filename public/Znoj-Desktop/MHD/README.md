- [**Description**](#description)
- [**Technology**](#technology)
- [**Year**](#year)
- [**Diagram případů užití**](#diagram-pripadu-uziti)
- [**Kontextový diagram**](#kontextovy-diagram)
- [**Datová analýza**](#datova-analyza)
  - [**Lineární zápis seznamu typů entit a jejich atributů**](#linearni-zapis-seznamu-typu-entit-a-jejich-atributu)
    - [Nahrazení vztahů M:N](#nahrazeni-vztahu-m-n)
  - [**Grafický tvar konceptuálního modelu (ERD, UML)**](#graficky-tvar-konceptualniho-modelu-erd-uml)
  - [**Grafický tvar logického modelu**](#graficky-tvar-logickeho-modelu)
  - [**Grafický tvar relačního datového modelu**](#graficky-tvar-relacniho-datoveho-modelu)
  - [**Úplné tabulky atributů (tj. datový slovník) a integritní omezení**](#uplne-tabulky-atributu-tj-datovy-slovnik-a-integritni-omezeni)
- [**Funkční závislosti a normální formy**](#funkcni-zavislosti-a-normalni-formy)
  - [Sestavení množiny funkčních závislostí](#sestaveni-mnoziny-funkcnich-zavislosti)
  - [**Sestavení relačního datového modelu v BCNF**](#sestaveni-relacniho-datoveho-modelu-v-bcnf)
- [**Porovnání původního relačního modelu získaného z konceptuálního modelu a modelu v BCNF**](#porovnani-puvodniho-relacniho-modelu-ziskaneho-z-konceptualniho-modelu-a-modelu-v-bcnf)
- [**Docs**](#docs)
- [**Presentations**](#presentations)
- [**SQL scripts**](#sql-scripts)

### **Description**

<div align="center">
  <img src="./README/zadani.pdf" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">zadání pdf</img>
</div>
  
**PROČ**  
Je potřeba vytvořit komplexní informační systém městské hromadné dopravy, který bude sloužit jak pro vnitřní, tak pro vnější účely.  
**K ČEMU**  
Tento informační systém bude sloužit k vyhledávání spojů městské hromadné dopravy podle různých kritérií, jakými jsou například typ vozidla, jestli vozidlo umožňuje bezbariérový přístup, vyhledávání jen v zastávkách umožňujících bezbariérový přístup do vozidla a zároveň bude sloužit k evidenci řidičů a k práci s jejich směnami.
**KDO**  
Se systémem bude moci pracovat každý, kdo si chce vyhledat nějaký spoj městské hromadné dopravy, řidiči vozidel městské hromadné dopravy, správce systému a také zaměstnavatel řidičů.  
**VSTUPY**  
U řidičů evidujeme rodné číslo, jméno, příjmení, bydliště, telefonní číslo, pohlaví, heslo a plat.
U směn evidujeme typ – tedy název směny, v kolik začíná, v kolik končí a kolik je to dohromady hodin.
U vozidel evidujeme typ – jestli se jedná o tramvaj, autobus, metro, trolejbus nebo vlak, dále pak jestli je vozidlo s bariérovým přístupem, značku, max. počet cestujících a řidiče, který se o vozidlo stará.
Dále evidujeme, jací řidiči jezdí s jakými vozidly a od kolika, do kolika hodin jezdí vozidlo určitou linku.
U zastávek evidujeme identifikační číslo zastávky, její název a možnost bezbariérového přístup.
Dále pak v kolik hodin zastavuje na konkrétní zastávce která linka.
U zón evidujeme číslo zóny a pak jestli se jedná o městskou, nebo mimoměstskou oblast.
U linek evidujeme číslo linky a typ linky (vozidla jakého typu po ní jezdí – tramvaj/autobus/metro/trolejbus/vlak)  
**VÝSTUPY**
Výstupem pro správce nebo zaměstnavatele budou všichni řidiči, všechna vozidla, jaký řidič se stará o jaké vozidlo, jací řidiči řídí jaká vozidla, všechny zastávky, linky, zóny.
Pro nepřihlášeného uživatele bude výstupem spojení dle zadaných kritérií včetně všech zastávek, které leží mezi zadanou vstupní a výstupní zastávkou, všechny odjezdy ze zadané zastávky, všechny spoje z dané zastávky.
Pro řidiče se zobrazí rozpis směn, všechny detaily o vozidle, o které se stará, všechny směny včetně jejich detailů.  
**FUNKCE**  
Pokud přidáme nové vozidlo, musíme mu také přidělit řidiče, který se o toto vozidlo bude starat.
Po přidání zastávky se musí zadat, ve které zóně leží a do které linky bude zastávka patřit.
Každý řidič patří do jednoho rozpisu směn, kde se směny „točí“.
Pro každé vyhledávání vyhledá k zastávce odjezdu trasu (všechny navštívené zastávky) k zastávce příjezdu.  
**OKOLÍ**  
Kdokoli, kdo se k aplikaci dostane, může v aplikaci vyhledávat spoje.
Pracovníci městské hromadné dopravy pak používají aplikaci pro vnitřní účely.  

---

### **Technology**

SQL

---

### **Year**

2012

---

### **Diagram případů užití**

<div align="center">
  <img src="./README/usecase2.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Kontextový diagram**

<div align="center">
  <img src="./README/kontextDiagram.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Datová analýza**

#### **Lineární zápis seznamu typů entit a jejich atributů**

**Entity**  
Ridic (rc, jmeno, prijmenu, bydliste, tel, pohlavi, heslo, plat)  
Vozidlo (idVozidla, tyo, znacka, maxPocet, barierove, rc, rc_ridi)  
Linka (cisloLinky, typLinky)  
Zastavka (idZastavky, nazev, barierova, cisloZony)  
Zona (cisloZony, mesto)  
Smena (typSmeny, začátek, konec, hodin)  

**Vztahy**  
stara_se_o (Ridic, Vozdilo) 1:1  
ridi(Ridic, Vozidlo) N:M  
jezdi (Vozdilo, Linka) N:M  
se sklada (Linka, Zastavka) N:M  
obsahuje (Zona, Zastavka) 1:N  
chodi (Ridic, RozpisSmen) N:M  

##### Nahrazení vztahů M:N

**Entity**  
Ridic (rc, jmeno, prijmenu, bydliste, tel, pohlavi, heslo, plat)  
Vozidlo (idVozidla, tyo, znacka, maxPocet, barierove, rc)  
Linka (cisloLinky, typLinky)  
Zastavka (idZastavky, nazev, barierova, cisloZony)  
Zona (cisloZony, mesto)  
Smena (typSmeny, začátek, konec, hodin)  
VozidloLinka (od, do, idVozidla, cisloLinky)  
ZastavkaLinkaCas (cas, cisloLinky, idZastavky)  
RozpisSmen (datum, jmenoSmeny, rc, typSmeny)  
RidicVozidlo (rc, idVozidla)  

**Vztahy**  
stara_se_o (Ridic, Vozdilo) 1:1  
ridi_Ridic (Ridic, RidicVozidlo) 1:N  
ridi_Vozidlo (Vozidlo, RidicVozidlo) 1:N
jezdi (Vozdilo, VozidloLinka) 1:N  
ma (Linka, VozidloLinka) 1:N  
se sklada (Linka, ZastavkaLinkaCas) 1:N  
zastavuje (Zastavka, zastavkaLinkaCas) 1:N  
obsahuje (Zona, Zastavka) 1:N  
chodi (Ridic, RozpisSmen) 1:N  
je v (Smena, RozpisSmen) 1:N  

#### **Grafický tvar konceptuálního modelu (ERD, UML)**

<div align="center">
  <img src="./README/er_koncept.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

#### **Grafický tvar logického modelu**

<div align="center">
  <img src="./README/Logical%20model.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

#### **Grafický tvar relačního datového modelu**

<div align="center">
  <img src="./README/er_mhd.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

#### **Úplné tabulky atributů (tj. datový slovník) a integritní omezení**

<div align="center">
  <img src="./README/datovy_slovnik.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

...  
NN = not null, N = null, PK = primární klíč, FK = cizí klíč, PFK = cizí klíč je součást primárního klíče

---

### **Funkční závislosti a normální formy**

#### Sestavení množiny funkčních závislostí

**Universální schéma**  
U (rc, jméno, prijmeni, bydliste, tel, pohlavi, heslo, plat, idVozidla, typVozidla, znacka, maxPocet, barierove, od, do, cisloLinky, typLinky, cas, IdZastavky, nazev, barierova, cisloZony, mesto, datum, jmenoSmeny, typSmeny, zacatek, konec, hodin)

**přejmenování pro rychlejší a přehlednější zpracování:**  
U (rc, jm, pr, bydl, tel, pohl, hes, plat, idV, typV, zn, maxP, barV, od, do, cisL, typL, cas, idZ, naz, barZ, cisZ, met, dat, jmS, typS, zac, kon, hod)

**Množina všech funkčních závislostí**  
F = {  
    rc -> jm pr bydl tel pohl hes plat  
    idV -> typV zn maxP barV rc  
    idV cisL -> od do  
    cisL -> typL  
    cisL idZ -> cas  
    idZ -> naz barZ cisZ  
    cisZ -> met  
    rc typS -> dat, jmS  
    typS -> zac, kon, hod  
}  

**Klíč univerzálního schématu**  
rc             += { rc jm pr bydl tel pohl hes plat }  
idV        += { idV typV zn maxP barV rc jm pr bydl tel pohl hes plat }  
idV cisL += { idV typV zn maxP barV rc jm pr bydl tel pohl hes plat cisL typL od do } cisL        += { cisL typL }  
cisL idZ    += { cisL typL idZ naz barZ cisZ met cas }  
idZ        += { idZ naz barZ cisZ met }  
cisZ       += { cisZ met }  
rc typS     += { rc jm pr bydl tel pohl hes plat typS zac kon hod dat jmS }  
typS       += { typS zac kon hod }  

Klíč universálního schématu **K = {idV cisL idZ typS}**

Ve funkčních závislostech se žádné redundantní závislosti ani atributy nevyskytují, tudíž F = Fmin.

#### **Sestavení relačního datového modelu v BCNF**

**Rozklad pomocí dekompozice**  
<div align="center">
  <img src="./README/Rozklad_dekompozice.jpg" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

**Rozklad pomocí syntézy**  
<div align="center">
  <img src="./README/Rozklad_synteza.jpg" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Porovnání původního relačního modelu získaného z konceptuálního modelu a modelu v BCNF**

Oba rozklady se od konceptuálního modelu výrazně neliší. Liší se jen tím, že tabulka RidicVozidlo, která reprezentuje m:n vztah mezi řidiči a vozidly, je identifikována pomocí klíče univerzálního schématu – tedy atributy idVozidla, cisloLinky, idZastavky, TypSmeny.

---

### **Docs**  

<div align="center">
  <img src="./README/MHD.docx" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">MHD docx</img>
</div>
  
<div align="center">
  <img src="./README/MHD.pdf" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">MHD pdf</img>
</div>
  
<div align="center">
  <img src="./README/MHD%20-%20dotazy.pdf" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">MHD SQL dotazy pdf</img>
</div>
  
<div align="center">
  <img src="./README/MHD%20-%20dotazy.docx" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">MHD SQL dotazy docx</img>
</div>
  
---

### **Presentations**  

<div align="center">
  <img src="./README/prezentace1.pdf" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">prezentace1 pdf</img>
</div>
  
<div align="center">
  <img src="./README/prezentace1.ppt" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">prezentace1 ppt</img>
</div>
  
<div align="center">
  <img src="./README/prezentace1.pptx" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">prezentace1 pptx</img>
</div>
  
<div align="center">
  <img src="./README/prezentace2.pdf" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">prezentace2 pdf</img>
</div>
  
<div align="center">
  <img src="./README/prezentace2.ppt" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">prezentace2 ppt</img>
</div>
  
<div align="center">
  <img src="./README/prezentace2.pptx" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">prezentace2 pptx</img>
</div>
  
---

### **SQL scripts**  

<div align="center">
  <img src="./README/Skript_schema.sql" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">SQL script schema</img>
</div>
  
<div align="center">
  <img src="./README/Skript_data.sql" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">SQL script data</img>
</div>
  
<div align="center">
  <img src="./README/Skript_query.sql" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">SQL script query</img>
</div>
  