insert into Ridic values (8202101235, 'Karel', 'R��i�ka', 'Brno', 777888999, 'muz', 'ruz123456', 12000);
insert into Ridic values (7902117561, 'Ond�ej', 'Kozel', 'Ostrava', 777987789, 'muz', 'dsf687g', 12200);
insert into Ridic values (9203201235, 'Ji��', 'Znoj', 'Brno', 723654789, 'muz', 'dfg87hdfgh', 13000);
insert into Ridic values (8202127380, 'Jan', 'Pa�ez', 'Opava', 732145698, 'muz', 'hg7fhgfh', 12000);
insert into Ridic values (8205201235, 'V�clav', 'Koblih', 'Praha', 795236871, 'muz', 'tert7re', 11500);
insert into Ridic values (8502151235, 'Karel', 'Sv�t�k', 'Ostrava', 789654231, 'muz', 'gd6fgds', 13200);
insert into Ridic values (8504271235, 'Adam', 'Smet�k', 'Brno', 741253986, 'muz', 'bcvb7xcv', 12400);
insert into Ridic values (8202181545, 'Jan', 'R��i�ka', 'Ostrava', 712305687, 'muz', 'hgfh8dfgh', 12000);
insert into Ridic values (8407231200, 'Dominik', 'palice', 'Ostrava', 701236548, 'muz', 'te8r', 12500);
insert into Ridic values (6704108220, 'Jakub', 'Barto�', 'Praha', 732014569, 'muz', 'te8rwt', 12000);
insert into Ridic values (7210261235, 'Simona', 'Koutn�', 'Ostrava', 702145368, 'zena', 'yd7fgd', 13000);
insert into Ridic values (7808182235, 'Andrea', 'Pln�', 'Brno', 702547896, 'zena', 'ycvb6cv', 11500);
insert into Ridic values (5512281115, 'Jana', 'Vtipn�', 'Ostrava', 702145236, 'zena', 'dfg7dfg', 12000);
insert into Ridic values (4501171235, 'Kate�ina', 'R��i�kov�', 'Brno', 702147896, 'zena', 'as5dfas', 11000);
insert into Ridic values (7803011211, 'Jitka', 'B�l�', 'Praha', 775632014, 'zena', 'fasd6fs', 11800);
insert into Ridic values (7904107220, 'Anna', 'Barto�ov�', 'Ostrava', 775889633, 'zena', 'fs4dfdsf', 14000);
insert into Ridic values (5911108237, 'Jana', 'Chytr�', 'Praha', 775102365, 'zena', 'fasdfa5sdfr', 15000);
insert into Ridic values (6804309270, 'Eva', 'Zdrav�', 'Ostrava', 777881299, 'zena', 'hz123456', 12000);
insert into Ridic values (3908122525, 'Karla', '�enat�', 'Ostrava', 777878999, 'zena', 'ruhfg456', 12200);
insert into Ridic values (4905113024, 'Olga', 'Siln�', 'Ostrava', 777888899, 'zena', 'rfgh123456', 12400);
insert into Ridic values (6009113245, 'Jindra', 'Siln�', 'Praha', 776878999, 'zena', 'ruh6', 12500);
insert into Ridic values (8910123422, 'Kv�toslava', 'Proch�zkov�', 'Praha', 777880089, 'zena', 'gz1f56', 12400);
insert into Ridic values (9011113222, 'Hana', '�t�rbov�', 'P�erov', 776878529, 'zena', 'dfghh56', 12200);

insert into Vozidlo values (1, 'tramvaj', '�koda', 200, 1, 8202101235);
insert into Vozidlo values (2, 'tramvaj', '�koda', 220, 0, 8202127380);
insert into Vozidlo values (3, 'tramvaj', '�koda', 200, 1, 7902117561);
insert into Vozidlo values (4, 'tramvaj', '�koda', 300, 0, 8407231200);
insert into Vozidlo values (5, 'tramvaj', '�koda', 200, 1, 9203201235);
insert into Vozidlo values (6, 'tramvaj', '�koda', 100, 1, 5911108237);
insert into Vozidlo values (7, 'tramvaj', '�koda', 300, 0, 3908122525);
insert into Vozidlo values (8, 'trolejbus', 'Opel', 70, 1, 6009113245);
insert into Vozidlo values (9, 'trolejbus', 'Opel', 70, 1, 8910123422);
insert into Vozidlo values (10, 'vlak', 'Rail', 500, 1, 8407231200);
insert into Vozidlo values (11, 'vlak', 'Rail', 400, 1, 7808182235);
insert into Vozidlo values (12, 'vlak', 'Rail', 300, 0, 7803011211);
insert into Vozidlo values (13, 'vlak', 'Rail', 450, 1, 5512281115);
insert into Vozidlo values (14, 'vlak', 'Rail', 350, 1, 4501171235);
insert into Vozidlo values (15, 'autobus', 'Karosa', 60, 1, 6704108220);
insert into Vozidlo values (16, 'autobus', 'Karosa', 60, 0, 8205201235);
insert into Vozidlo values (17, 'autobus', '�koda', 90, 1, 7904107220);

insert into Linka values(1, 'tramvaj');
insert into Linka values(2, 'tramvaj');
insert into Linka values(3, 'tramvaj');
insert into Linka values(4, 'tramvaj');
insert into Linka values(5, 'tramvaj');
insert into Linka values(6, 'tramvaj');
insert into Linka values(7, 'tramvaj');
insert into Linka values(8, 'tramvaj');
insert into Linka values(9, 'tramvaj');
insert into Linka values(10, 'trolejbus');
insert into Linka values(11, 'trolejbus');
insert into Linka values(12, 'trolejbus');
insert into Linka values(13, 'autobus');
insert into Linka values(14, 'autobus');
insert into Linka values(15, 'autobus');
insert into Linka values(16, 'autobus');
insert into Linka values(17, 'vlak');
insert into Linka values(18, 'vlak');
insert into Linka values(19, 'vlak');
insert into Linka values(20, 'vlak');

insert into Zona values(1,1);
insert into Zona values(2,1);
insert into Zona values(3,1);
insert into Zona values(4,1);
insert into Zona values(5,1);
insert into Zona values(6,1);
insert into Zona values(7,1);
insert into Zona values(8,0);
insert into Zona values(9,0);
insert into Zona values(10,0);

insert into Zastavka values(1, 'Studentsk�',0,1);
insert into Zastavka values(2, 'Kr�tk�',1,1);
insert into Zastavka values(3, 'Dlouh�',1,2);
insert into Zastavka values(4, 'Studen�',1,3);
insert into Zastavka values(5, 'Elektra',0,4);
insert into Zastavka values(6, 'Stodoln�',1,5);
insert into Zastavka values(7, 'Hlavn� n�dra��',1,6);
insert into Zastavka values(8, 'Pustkoveck�',1,7);
insert into Zastavka values(9, 'Moravsk�',0,8);
insert into Zastavka values(10, 'Ji�n�',1,9);
insert into Zastavka values(11, 'Severn�',0,10);
insert into Zastavka values(12, 'Z�padn�',1,1);
insert into Zastavka values(13, 'V�chodn�',0,1);
insert into Zastavka values(14, 'Rychl�',1,2);
insert into Zastavka values(15, 'Pomal�',1,2);

insert into Smena values('rann�', '04:00', '12:00', 8);
insert into Smena values('odpoledn�', '12:00', '20:00', 8);
insert into Smena values('no�n�', '20:00', '04:00', 8);
insert into Smena values('denn�', '07:00', '15:00', 8);

--uvazuju pripad, ze kdyz se ridic stara o vozidlo, tak ho taky ridi
INSERT INTO RidicVozidlo(rc, idVozidla) SELECT r.rc, v.idVozidla FROM Ridic r JOIN Vozidlo v ON v.rc = r.rc GROUP BY r.rc, v.idVozidla
--ostatni ridice pridam do prvnich n vozidel a pridam dalsi vozidla se kterymi mohou n�kteri jezdit
insert into RidicVozidlo values('4905113024', 1);
insert into RidicVozidlo values('6804309270', 2);
insert into RidicVozidlo values('7210261235', 3);
insert into RidicVozidlo values('8202181545', 4);
insert into RidicVozidlo values('8502151235', 5);
insert into RidicVozidlo values('8504271235', 6);
insert into RidicVozidlo values('9011113222', 7);
insert into RidicVozidlo values('4905113024', 2);
insert into RidicVozidlo values('6804309270', 3);
insert into RidicVozidlo values('7210261235', 4);
insert into RidicVozidlo values('8502151235', 5);
insert into RidicVozidlo values('8504271235', 5);
insert into RidicVozidlo values('9011113222', 5);

insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'A', 8202101235, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'A', 7902117561, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'A', 9203201235, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'A', 8202127380, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'A', 8205201235, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'A', 8502151235, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 8504271235, 'odpoledn�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 8202181545, 'odpoledn�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 8407231200, 'odpoledn�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 6704108220, 'odpoledn�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 7210261235, 'odpoledn�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 9011113222, 'odpoledn�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'C', 7808182235, 'no�n�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'C', 5512281115, 'no�n�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'C', 4501171235, 'no�n�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'C', 7803011211, 'no�n�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'C', 7904107220, 'no�n�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'D', 5911108237, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'D', 6804309270, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'D', 3908122525, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'D', 4905113024, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'D', 6009113245, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'D', 8910123422, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 8504271235, 'denn�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 8202181545, 'denn�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 8407231200, 'denn�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 6704108220, 'denn�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 7210261235, 'denn�');
insert into RozpisSmen values(Convert(DateTime,'20121220',112), 'B', 9011113222, 'denn�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'A', 8202101235, 'no�n�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'A', 7902117561, 'no�n�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'A', 9203201235, 'no�n�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'A', 8202127380, 'no�n�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'A', 8205201235, 'no�n�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'A', 8502151235, 'no�n�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'C', 7808182235, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'C', 5512281115, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'C', 4501171235, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'C', 7803011211, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'C', 7904107220, 'rann�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'D', 5911108237, 'odpoledn�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'D', 6804309270, 'odpoledn�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'D', 3908122525, 'odpoledn�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'D', 4905113024, 'odpoledn�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'D', 6009113245, 'odpoledn�');
insert into RozpisSmen values(Convert(DateTime,'20121221',112), 'D', 8910123422, 'odpoledn�');

insert into VozidloLinka values(Convert(DateTime,'05:00:00', 108), Convert(DateTime,'16:00:00', 108), 1, 1)
insert into VozidloLinka values(Convert(DateTime,'05:00:00', 108), Convert(DateTime,'16:00:00', 108), 2, 2)
insert into VozidloLinka values(Convert(DateTime,'05:00:00', 108), Convert(DateTime,'16:00:00', 108), 3, 3)
insert into VozidloLinka values(Convert(DateTime,'05:00:00', 108), Convert(DateTime,'16:00:00', 108), 4, 4)
insert into VozidloLinka values(Convert(DateTime,'05:00:00', 108), Convert(DateTime,'16:00:00', 108), 5, 5)
insert into VozidloLinka values(Convert(DateTime,'06:00:00', 108), Convert(DateTime,'18:00:00', 108), 6, 6)
insert into VozidloLinka values(Convert(DateTime,'06:00:00', 108), Convert(DateTime,'18:00:00', 108), 7, 7)
insert into VozidloLinka values(Convert(DateTime,'06:00:00', 108), Convert(DateTime,'18:00:00', 108), 1, 8)
insert into VozidloLinka values(Convert(DateTime,'06:00:00', 108), Convert(DateTime,'18:00:00', 108), 6, 9)
insert into VozidloLinka values(Convert(DateTime,'06:00:00', 108), Convert(DateTime,'18:00:00', 108), 8, 10)
insert into VozidloLinka values(Convert(DateTime,'14:00:00', 108), Convert(DateTime,'20:00:00', 108), 9, 11)
insert into VozidloLinka values(Convert(DateTime,'14:00:00', 108), Convert(DateTime,'20:00:00', 108), 10, 17)
insert into VozidloLinka values(Convert(DateTime,'14:00:00', 108), Convert(DateTime,'20:00:00', 108), 11, 18)
insert into VozidloLinka values(Convert(DateTime,'14:00:00', 108), Convert(DateTime,'20:00:00', 108), 12, 19)
insert into VozidloLinka values(Convert(DateTime,'14:00:00', 108), Convert(DateTime,'20:00:00', 108), 13, 20)
insert into VozidloLinka values(Convert(DateTime,'14:00:00', 108), Convert(DateTime,'20:00:00', 108), 14, 20)
insert into VozidloLinka values(Convert(DateTime,'19:00:00', 108), Convert(DateTime,'05:00:00', 108), 8, 12)
insert into VozidloLinka values(Convert(DateTime,'19:00:00', 108), Convert(DateTime,'05:00:00', 108), 15, 13)
insert into VozidloLinka values(Convert(DateTime,'19:00:00', 108), Convert(DateTime,'05:00:00', 108), 16, 14)
insert into VozidloLinka values(Convert(DateTime,'19:00:00', 108), Convert(DateTime,'05:00:00', 108), 17, 15)
insert into VozidloLinka values(Convert(DateTime,'19:00:00', 108), Convert(DateTime,'05:00:00', 108), 16, 16)

insert into ZastavkaLinkaCas values(Convert(DateTime,'6:00:00', 108), 1, 1)
insert into ZastavkaLinkaCas values(Convert(DateTime,'6:20:00', 108), 2, 2)
insert into ZastavkaLinkaCas values(Convert(DateTime,'6:20:00', 108), 3, 3)
insert into ZastavkaLinkaCas values(Convert(DateTime,'6:30:00', 108), 4, 4)
insert into ZastavkaLinkaCas values(Convert(DateTime,'6:30:00', 108), 5, 5)
insert into ZastavkaLinkaCas values(Convert(DateTime,'6:40:00', 108), 6, 6)
insert into ZastavkaLinkaCas values(Convert(DateTime,'6:50:00', 108), 7, 7)
insert into ZastavkaLinkaCas values(Convert(DateTime,'6:50:00', 108), 8, 8)
insert into ZastavkaLinkaCas values(Convert(DateTime,'7:00:00', 108), 9, 9)
insert into ZastavkaLinkaCas values(Convert(DateTime,'7:10:00', 108), 10, 10)
insert into ZastavkaLinkaCas values(Convert(DateTime,'7:20:00', 108), 11, 11)
insert into ZastavkaLinkaCas values(Convert(DateTime,'7:30:00', 108), 12, 12)
insert into ZastavkaLinkaCas values(Convert(DateTime,'7:40:00', 108), 13, 13)
insert into ZastavkaLinkaCas values(Convert(DateTime,'8:00:00', 108), 14, 14)
insert into ZastavkaLinkaCas values(Convert(DateTime,'8:20:00', 108), 15, 15)
insert into ZastavkaLinkaCas values(Convert(DateTime,'8:30:00', 108), 16, 11)
insert into ZastavkaLinkaCas values(Convert(DateTime,'9:40:00', 108), 17, 12)
insert into ZastavkaLinkaCas values(Convert(DateTime,'9:50:00', 108), 18, 13)
insert into ZastavkaLinkaCas values(Convert(DateTime,'10:00:00', 108), 19, 14)
insert into ZastavkaLinkaCas values(Convert(DateTime,'10:20:00', 108), 20, 15)
insert into ZastavkaLinkaCas values(Convert(DateTime,'11:00:00', 108), 1, 2)
insert into ZastavkaLinkaCas values(Convert(DateTime,'11:20:00', 108), 2, 3)
insert into ZastavkaLinkaCas values(Convert(DateTime,'11:20:00', 108), 3, 4)
insert into ZastavkaLinkaCas values(Convert(DateTime,'11:30:00', 108), 4, 5)
insert into ZastavkaLinkaCas values(Convert(DateTime,'11:30:00', 108), 5, 6)
insert into ZastavkaLinkaCas values(Convert(DateTime,'11:40:00', 108), 6, 7)
insert into ZastavkaLinkaCas values(Convert(DateTime,'11:50:00', 108), 7, 1)
insert into ZastavkaLinkaCas values(Convert(DateTime,'11:50:00', 108), 8, 9)
insert into ZastavkaLinkaCas values(Convert(DateTime,'12:00:00', 108), 9, 10)
insert into ZastavkaLinkaCas values(Convert(DateTime,'12:10:00', 108), 10, 11)
insert into ZastavkaLinkaCas values(Convert(DateTime,'12:20:00', 108), 11, 12)
insert into ZastavkaLinkaCas values(Convert(DateTime,'12:30:00', 108), 12, 13)
insert into ZastavkaLinkaCas values(Convert(DateTime,'12:40:00', 108), 13, 14)
insert into ZastavkaLinkaCas values(Convert(DateTime,'13:00:00', 108), 14, 15)
insert into ZastavkaLinkaCas values(Convert(DateTime,'13:20:00', 108), 15, 8)
insert into ZastavkaLinkaCas values(Convert(DateTime,'13:30:00', 108), 16, 10)
insert into ZastavkaLinkaCas values(Convert(DateTime,'13:40:00', 108), 17, 14)
insert into ZastavkaLinkaCas values(Convert(DateTime,'13:50:00', 108), 18, 12)
insert into ZastavkaLinkaCas values(Convert(DateTime,'14:00:00', 108), 19, 13)
insert into ZastavkaLinkaCas values(Convert(DateTime,'14:00:00', 108), 20, 1)
insert into ZastavkaLinkaCas values(Convert(DateTime,'14:20:00', 108), 1, 3)
insert into ZastavkaLinkaCas values(Convert(DateTime,'14:30:00', 108), 2, 1)
insert into ZastavkaLinkaCas values(Convert(DateTime,'14:40:00', 108), 3, 2)
insert into ZastavkaLinkaCas values(Convert(DateTime,'15:00:00', 108), 4, 6)
insert into ZastavkaLinkaCas values(Convert(DateTime,'15:30:00', 108), 5, 4)
insert into ZastavkaLinkaCas values(Convert(DateTime,'15:40:00', 108), 6, 5)
insert into ZastavkaLinkaCas values(Convert(DateTime,'16:00:00', 108), 1, 4)
insert into ZastavkaLinkaCas values(Convert(DateTime,'16:20:00', 108), 2, 5)
insert into ZastavkaLinkaCas values(Convert(DateTime,'16:20:00', 108), 3, 6)
insert into ZastavkaLinkaCas values(Convert(DateTime,'16:30:00', 108), 4, 1)
insert into ZastavkaLinkaCas values(Convert(DateTime,'16:30:00', 108), 5, 2)
insert into ZastavkaLinkaCas values(Convert(DateTime,'16:40:00', 108), 6, 3)
insert into ZastavkaLinkaCas values(Convert(DateTime,'16:50:00', 108), 7, 10)
insert into ZastavkaLinkaCas values(Convert(DateTime,'16:50:00', 108), 8, 11)
insert into ZastavkaLinkaCas values(Convert(DateTime,'17:00:00', 108), 9, 12)
insert into ZastavkaLinkaCas values(Convert(DateTime,'17:10:00', 108), 10, 7)
insert into ZastavkaLinkaCas values(Convert(DateTime,'17:20:00', 108), 11, 8)
insert into ZastavkaLinkaCas values(Convert(DateTime,'17:30:00', 108), 12, 9)
insert into ZastavkaLinkaCas values(Convert(DateTime,'17:40:00', 108), 13, 1)
insert into ZastavkaLinkaCas values(Convert(DateTime,'18:00:00', 108), 14, 2)
insert into ZastavkaLinkaCas values(Convert(DateTime,'18:20:00', 108), 15, 3)
insert into ZastavkaLinkaCas values(Convert(DateTime,'18:30:00', 108), 16, 4)
insert into ZastavkaLinkaCas values(Convert(DateTime,'19:40:00', 108), 17, 5)
insert into ZastavkaLinkaCas values(Convert(DateTime,'19:50:00', 108), 18, 6)
insert into ZastavkaLinkaCas values(Convert(DateTime,'20:00:00', 108), 19, 7)
insert into ZastavkaLinkaCas values(Convert(DateTime,'20:20:00', 108), 20, 8)