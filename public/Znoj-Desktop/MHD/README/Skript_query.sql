--1;1;23
Select * from Ridic
--1;2;17
Select * from Vozidlo
--1;3;15
Select * from Zastavka

--2;1;15
Select * from Zastavka
order by Zastavka.nazev
--2;2;23
Select * from Ridic
order by Ridic.prijmeni
--2;3;17
Select * from Vozidlo
order by Vozidlo.maxPocet desc

--3;1;13
Select * from Ridic
where Ridic.pohlavi='zena'
--3;2;4
Select * from Linka
where Linka.typLinky = 'autobus'
--3;3;29
Select * from RozpisSmen
where RozpisSmen.datum = Convert(DateTime,'20121220',112)

--4;1;46
Select * from RozpisSmen
where RozpisSmen.datum like '2012%'
--4;2;11
Select * from Ridic
where Ridic.bydliste like 'O%'
--4;3;8
Select * from Zastavka
where Zastavka.nazev like '%�'

--5;1;23
Select LEFT(Ridic.rc, 2) from Ridic
--5;2;23
Select LEFT(Ridic.tel, 3) from Ridic
--5;3;4
Select REVERSE(Smena.typSmeny) from Smena

--6;1;1
Select COUNT(*) from Ridic
where ridic.pohlavi = 'muz'
--6;2;1
Select MAX(Ridic.rc) from Ridic
--6;3;1
Select SUM(Ridic.plat) from Ridic

--7;1;8
Select r.prijmeni from Ridic r
join RidicVozidlo rv on r.rc = rv.rc
join Vozidlo v on v.idVozidla = rv.idVozidla
group by r.prijmeni
Having Count(*) > 1
--7;2;13
Select v.idVozidla from Vozidlo v
join VozidloLinka vl on vl.idVozidla = v.idVozidla
join Linka l on l.cisloLinky = vl.cisloLinky
group by v.idVozidla
Having Count(*) = 1
--7;3;4
Select v.idVozidla, v.typVozidla from Vozidlo v
join VozidloLinka vl on vl.idVozidla = v.idVozidla
join Linka l on l.cisloLinky = vl.cisloLinky
group by v.idVozidla, v.typVozidla
Having Count(*) > 1

--8;1;24
select r.jmeno, r.prijmeni, v.idVozidla from Ridic r 
LEFT JOIN Vozidlo v on v.rc = r.rc 
group by r.jmeno, r.prijmeni, v.idVozidla
--8;2;1
Select Zastavka.nazev from ZastavkaLinkaCas
JOIN Zastavka on ZastavkaLinkaCas.idZastavky = Zastavka.idZastavky
group by ZastavkaLinkaCas.idZastavky, Zastavka.nazev
having COUNT(ZastavkaLinkaCas.idZastavky) > 5
--8;3;15
Select z.cisloZony, za.idZastavky from Zona z
LEFT JOIN Zastavka za on z.cisloZony = za.cisloZony

--9;1;7
Select r.prijmeni from Ridic r
where r.rc not in (Select v.rc from Vozidlo v
					where v.rc = r.rc)
--9;2;11
Select * from Zastavka z
where z.idZastavky not in(Select z2.idZastavky from Zastavka z2
							where z2.cisloZony = 1)
--9;3;6
Select distinct r.prijmeni from Ridic r
join RidicVozidlo rv on rv.rc = r.rc
where r.rc in (Select r2.rc from Ridic r2
				join RidicVozidlo rv2 on rv2.rc = r2.rc
				where r.rc = r2.rc and rv.idVozidla <> rv2.idVozidla)

--10;1;2
Select distinct r.prijmeni from Ridic r
join RidicVozidlo rv on rv.rc = r.rc
where r.rc in (Select r2.rc from Ridic r2
				join RidicVozidlo rv2 on rv2.rc = r2.rc
				where r.rc = r2.rc and rv.idVozidla <> rv2.idVozidla)
	and r.prijmeni like 'S%'
--10;2;13
Select Zastavka.nazev from ZastavkaLinkaCas
JOIN Zastavka on ZastavkaLinkaCas.idZastavky = Zastavka.idZastavky
group by ZastavkaLinkaCas.idZastavky, Zastavka.nazev
having COUNT(ZastavkaLinkaCas.idZastavky) > 3
--10;3;1
Select r.prijmeni from Ridic r
where r.rc not in (Select v.rc from Vozidlo v
					where v.rc = r.rc)
	and r.pohlavi='muz' and r.prijmeni like '%a%'
