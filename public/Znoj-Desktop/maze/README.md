# Maze - Hledání cyklů v bludišti

- [Maze - Hledání cyklů v bludišti](#maze-hledani-cyklu-v-bludisti)
  - [**Description**](#description)
    - [Vstup:](#vstup)
    - [Výstup:](#vystup)
  - [**Technology**](#technology)
  - [**Year**](#year)
  - [**Implementation**](#implementation)
    - [**Rekurzivní řešení (no_paralel.cpp)**](#rekurzivni-reseni-no-paralel-cpp)
    - [**Nerekurzivní řešení (no_recursive.cpp)**](#nerekurzivni-reseni-no-recursive-cpp)
    - [**Obarvení různými barvami (no_rec_diff_colors.cpp)**](#obarveni-ruznymi-barvami-no-rec-diff-colors-cpp)
    - [**Příprava na paralelizaci (like_paralel.cpp)**](#priprava-na-paralelizaci-like-paralel-cpp)
    - [**Paralelní řešení (paralel.cpp)**](#paralelni-reseni-paralel-cpp)
  - [**Compare**](#compare)
  - [**Presentations**](#presentations)

### **Description**

<http://uva.onlinejudge.org/external/7/705.pdf>  
<div align="center">
  <img src="./README/maze_description.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

#### Vstup  

<div align="center">
  <img src="./README/maze_in.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

#### Výstup  

<div align="center">
  <img src="./README/maze_out.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

---

### **Technology**

C++

---

### **Year**

2015

---

### **Implementation**

#### **Rekurzivní řešení (no_paralel.cpp)**

- uložení vstupu do pole (znak \ a / jsou bool hodnoty v poli 3*3)
- pro každý prvek x bool pole zavolej funkci **seminkove_vyplnovani**
  - x je mimo hranice pole → vrať -1
  - x je obarvený nebo false → vrať 0
  - obarvi x a zavolej seminkove_vyplnovani pro všechny (4) okolní prvky
    - pokud jsou všechny návratové hodnoty >= 0, pak je sečti, přičti 1 a tuto hodnotu vrať
    - jinak vrať -1 (nejedná se o uzavřený cyklus)
- Návratovou hodnotu vyděl 3 a
- Pokud je hodnota kladná
  - porovnej s dosavadní maximální a maximální ulož
  - Inkrementuj počet kružnic

#### **Nerekurzivní řešení (no_recursive.cpp)**

- použití zásobníku v nerekurzivní funkci **seminkove_vyplnovani**
- dokud zásobník není prázdný, tak x = pop():
  - pokud je x obarvený – udělej další iteraci
  - obarvi x a inkrementuj proměnnou pocet
  - pokud je sousední mimo hranice, zaznač že se nejedná o cyklus a přiděl směru -1, jinak
  - pokud je sousední (4 směry) neobarvený a nejedná se o okraj kružnice, pak udělej push(sousední)
  - Pokud v žádném směru nebylo zaznamenáno -1, pak vrať inkrementovanou proměnnou pocet
  - Jinak vrať -1

#### **Obarvení různými barvami (no_rec_diff_colors.cpp)**

- Použití různých barev pro obarvení jednotlivých cyklů

<div align="center">
  <img src="./README/maze_console.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

#### **Příprava na paralelizaci (like_paralel.cpp)**

- rozdělení bludiště na několik částí (hodnota proměnné pocet_vlaken)
- v algoritmu se navíc detekuje okraj zpracovávané části
- různé barvy pro různé části

- po skončení algoritmu proběhne spojení jednotlivých částí cyklů:
  - obarví se na stejnou barvu,
  - sečte se velikost částí
  - upraví se počet cyklů
- v případě necyklů:
  - obarvení barvou značící necykly
  - úprava počtu cyklů, pokud je třeba

#### **Paralelní řešení (paralel.cpp)**

- každé vlákno má vlastní zásobník
- každé vlákno má privátní proměnné značící :
  - velikost cyklu,
  - souřadnice bodu,
  - barvu
- paralelizace je provedena za pomoci technologie OpenMP

---

### **Compare**

<div align="center">
  <img src="./README/maze_compare.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/maze_graph.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/namerena_data.xlsx" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">naměřená data xlsx</img>
</div>

---

### **Presentations**  

<div align="center">
  <img src="./README/Maze%20-%20prezentace%20-%20zno0011.pptx" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">Maze - prezentace pptx</img>
</div>
  
<div align="center">
  <img src="./README/Maze%20-%20prezentace%20-%20zno0011.pdf" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;">Maze - prezentace pdf</img>
</div>
