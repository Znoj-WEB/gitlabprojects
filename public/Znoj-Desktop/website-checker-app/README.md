### **Description**

App for checking item existence in e-shop.  
Periodically check e-shop if the item from `WHAT_TO_SEARCH` exists.

- when exists - based on settings - email is sent over sendgrid or courier, a sound is played, the webpage is opened in Firefox, the item is put into a shopping cart, and shopping cart is opened.
- when not found - keep looking, number of requests is shown.
- when click on url, url is opened in the app
- when click on `copy link` link is copied to the clipboard
- when choosing Fake IP or Change IP in the config, TOR is used for requests

Install:

- [Website Checker App.exe](releases/Website%20Checker%20App%20Setup%201.2.6.exe)

Development:

- install `NodeJS`
- `npm i`
- in 1st console `npm run dev:react`
- in 2nd console `npm run dev:electron`
- create `.env` with:

    ```
    SENDGRID_API_KEY=
    SENGRID_FROM=
    COURIER_API_KEY=
    ```

Packed inside:

- [geckodriver 0.30.0 (2021-09-16)](https://github.com/mozilla/geckodriver/releases)
- [tor-win32-0.4.6.9](https://www.torproject.org/download/tor/)

Relese:

- `npm run build`
- `npm run release`

---

### **Technology**

TypeScript, Electron, React, Fluent UI, WebDriver

---

### **Year**

2021

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/0.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/1.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/2.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/3.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/4.png" align="center" style="max-width: 956px;max-height: 600px;margin: 5px 0 10px 0;"></img>
</div>
