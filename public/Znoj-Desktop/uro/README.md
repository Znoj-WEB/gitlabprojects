### **Description**

GUI in QT - Cars and Employees, Options, Vehicles Database  

---

### **Technology**

C++ (QT)

---

### **Year**

2013

---

### **Screenshots**

#### Cars and employees

<div align="center">
  <img src="./README/URO_CarsAndEmployees.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/CarsAndEmployees1.PNG" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/CarsAndEmployees2.PNG" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/CarsAndEmployees3.PNG" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/CarsAndEmployees4.PNG" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/CarsAndEmployees5.PNG" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/CarsAndEmployees6.PNG" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/CarsAndEmployees7.PNG" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
#### Vehicles Database

Without screenshot  

#### Options

Without screenshot  
