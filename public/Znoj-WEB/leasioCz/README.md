### **Description**

Web created from photoshop designer.

---

### **Link**

[https://leasio.znoj.cz/](https://leasio.znoj.cz/)

---

### **Technology**

CSS, HTML

---

### **Year**

2015

---

### **Screenshots**

Main page:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Top bar:  
<div align="center">
  <img src="./README/width.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">width</img>
</div>
  
Phone view:  
<div align="center">
  <img src="./README/phone.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phone</img>
</div>
  