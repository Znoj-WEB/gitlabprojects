### **Description**

My first personal HTML page.

---

### **Link**

[https://zs.znoj.cz/](https://zs.znoj.cz/)

---

### **Technology**

HTML

---

### **Year**

2005

---

### **Screenshot**

Welcome page:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Cars section with dropdown:  
<div align="center">
  <img src="./README/carsMain.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">carsMain</img>
</div>
  
Particular car brand:  
<div align="center">
  <img src="./README/cars.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">cars</img>
</div>
  