### **Description**

Wall for notes.

---

### **Link**

[http://zed.znoj.cz/](http://zed.znoj.cz/)

---

### **Technology**

CSS, HTML, MySQL, PHP

---

### **Year**

2012

---

### **Screenshot**

Login page:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Wall with notes with possible to put + or - to each post but mine:  
<div align="center">
  <img src="./README/wall.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">wall</img>
</div>
  
About logged user:  
<div align="center">
  <img src="./README/aboutMe.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">aboutMe</img>
</div>
  
Change picture:  
<div align="center">
  <img src="./README/picture.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">picture</img>
</div>
  
Change password:  
<div align="center">
  <img src="./README/changePass.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">changePass</img>
</div>
  
Registration form:  
<div align="center">
  <img src="./README/register.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">register</img>
</div>
  