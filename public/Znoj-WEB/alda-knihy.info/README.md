### **Description**

Simple WordPress e-shop.  

---

### **Link**

Main page: [http://alda-knihy.cz/](http://alda-knihy.cz/)  
Main page of the e-shop: [http://alda-knihy.cz/nakladatelstvi/](http://alda-knihy.cz/nakladatelstvi/)  

---

### **Technology**

WordPress

---

### **Year**

2015

---

### **Screenshots**

Main page:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Main page of the e-shop:  
<div align="center">
  <img src="./README/main2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">main2</img>
</div>
  
List of books:  
<div align="center">
  <img src="./README/onlineOrders.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">onlineOrders</img>
</div>
  
Book detail:  
<div align="center">
  <img src="./README/bookDetail.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">bookDetail</img>
</div>
  
Page with basket content:  
<div align="center">
  <img src="./README/basket.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">basket</img>
</div>
  
Contact page:  
<div align="center">
  <img src="./README/contact.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">contact</img>
</div>
  