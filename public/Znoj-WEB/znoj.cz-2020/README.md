### **Description**

Jiri Znoj - personal website

---

### **Link**

served with Firebase:  
[https://znoj.cz](https://znoj.cz/)  
[https://znoj-4b8bc.web.app](https://znoj-4b8bc.web.app/)  
[https://znoj-4b8bc.firebaseapp.com](https://znoj-4b8bc.firebaseapp.com/)

---

### **Technology**

TypeScript, Ant Design, React, Hooks, Redux, Firebase, GitLab CI/CD

---

### **Year**

2020

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/m1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
