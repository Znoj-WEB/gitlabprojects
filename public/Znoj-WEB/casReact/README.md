### **Description**

React web app for countdown until chosen date and time in several units.  
Local storage for remembering data for browser is used.  
material-ui, material-ui-pickers and moments libraries are used in this app.

---

### **Link**

[https://cas.znoj.cz/](https://cas.znoj.cz/)

---

### **Technology**

JavaScript, Material-UI, React, GitLab CI/CD

---

### **Year**

2018

---

### **Screenshots**

Main page:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Calendar widget:  
<div align="center">
  <img src="./README/calendar.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">calendar</img>
</div>
  
Phone layout:  
<div align="center">
  <img src="./README/phone.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phone</img>
</div>
  