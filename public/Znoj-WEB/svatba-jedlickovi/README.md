### **Description**

Wedding Website

---

### **Link**

served with Netlify:  
[https://storied-moxie-342fdc.netlify.app](https://storied-moxie-342fdc.netlify.app/)  
[http://jedlickovi-svatba.cz](http://jedlickovi-svatba.cz/)

---

### **Technology**

TypeScript, Material-UI, React

---

### **Year**

2022

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/4.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
