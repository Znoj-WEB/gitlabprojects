### **Description**
School project (WebApps) - ASP.NET app - Informative system of the library.  
Task and conclusion in Czech language:  
  
### I. db server  
jméno: "sa"  
heslo: "123456789"  
  
ORM db v knihovně "DatovaVrstva"  
insert: DetailsView2  
update: GridView2  
delete: GridView3  
select: select ze 3 tabulek: GridView4  
  
### II. XML  
v knihovně "Knihovna" a třídě Knihy se nachází statické metody:  
public static List<Kniha> Select();  
public static void Insert(Kniha k);  
public static void Update(Kniha k);  
public static void Delete(string ISBN);  
pro práci se souborem XMLFile.xml za použití XmlDocument  
  
### III. ovládací prvky  
GridView - výpis dat z XML souboru XMLFile.xml pomocí metody Knihovna.Knihy.Select();  
	 - výpisy z databáze  
DetailView - insert do db - např. DetailsView2  
ObjectDataSource - výpisy z databáze  
Repeater - Výpis z XML souboru za použití XMLDataSource  
  
### IV. Composite Control  
Vlastnosti - TextBox poznamky, Label vypis, Button pridat, Button smazat, RequiredFieldValidator validator, property PoradiPoznamky  
Vlastní speciální události - poznamkyEvent "smazano" a "ulozeno" v knihovně CompositeControlx.CC;  
			   - pocetEvent s vlastními argumenty "pocetPoznamek"  
			   - metoda OnPoznamky(MyArgs e); k ovládání tlačítek komponenty CompositControl  
Samostatná knihovna použitá v aplikaci - soubor Default.aspx, TagPrefix="cc1"  
ViewState - ViewState["poznamky"] a property PoradiPoznamky  
HtmlTextWriter - v metodě Render(HtmlTextWriter writer);  
HttpBrowserCapabilities + Page.Request.Browser - v metodě Render(HtmlTextWriter writer);  
 	- browserCapabilities.IsMobileDevice - renderování výstupu dle prohlížeče (mobilní / desktop)  
 	- Page.Request.Browser.Browser pro výpis aktuálního prohlížeče a dle něj změny barvy pozadí tabulky  
  
### V. validace vstupů  
použity: RequiredFieldValidator, RegularExpressionValidator a RangeValidator  
  
### VI. lokalizace  
výchozí - angličtina a čeština - jazyk je závislý na jazyku prohlížeče  
použil jsem jak lokální (ve složce App_LocalResources), tak i globální (složka App_GlobalResources) lokalizaci  
  
### VII. logování chyb  
Chyby jsou logovány do souboru App_Data/log.txt; při neošetřené výjimce je zobrazena chyba v souboru Error.asax.  
Chyba 404 je zobrazena vlastní html stránkou.  
  
### VIII. přihlášení uživatelů  
Jelikož ve VS 2013 je třeba k přihlašování využít Windows Azure, tak jsem využil vlastní db a v ní tabulku "Ctenar".  
Tabulku s jmény a hesly smí zobrazit pouze administrátor:  
jmeno: admin  
heslo: admin  
všem přihlášeným uživatelům se objeví tlačítko pro odhlášení a skryje se přihlašování. Takovým uživatelem je např:  
jmeno: zno0011  
heslo: 123456  
  
### IX. Handler pro získání speciálních dat  
po zadání výšky a vybrání jednoho z vyobrazených obrázků se jeho velikost změní v závislosti na zadaných hodnotách.  
Handler získá požadovaný obrázek ze složky App_Data, vytvoří z něj nový dle zadané výšky se zachovaným poměrem stran a ten vrátí  
  
### X. WCF služba  
dle zadání - adaptér k Select, Insert, Update; k tabulce Kniha  
  
---
### **Technology**
ASP.NET

---
### **Year**
2015

---