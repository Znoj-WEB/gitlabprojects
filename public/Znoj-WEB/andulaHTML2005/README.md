### **Description**

Simple HTML page for my sister created in elementary school.

---

### **Technology**

HTML

---

### **Year**

2005

---

### **Screenshot**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
