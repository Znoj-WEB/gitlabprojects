### **Description**

My movies library with links to trailers.

---

### **Link**

[https://movies.znoj.cz/](https:/movies.znoj.cz/)

---

### **Technology**

CSS, HTML

---

### **Year**

2015

---

### **Screenshot**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/movies.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">movies</img>
</div>
