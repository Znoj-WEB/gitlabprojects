### **Description**

This project is template for working with Firebase Realtime db, login and use React UI framework Material-UI

---

### **Link**

served with Firebase:  
[https://fe.znoj.cz/](https://fe.znoj.cz/)

---

### **Technology**

JavaScript, Firebase, Material-UI, React, GitLab CI/CD

---

### **Year**

2019

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_1</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2</img>
</div>

<div align="center">
  <img src="./README/Screenshot_3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_3</img>
</div>

<div align="center">
  <img src="./README/Screenshot_4.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_4</img>
</div>

<div align="center">
  <img src="./README/Screenshot_5.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_5</img>
</div>

<div align="center">
  <img src="./README/Screenshot_6.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_6</img>
</div>

<div align="center">
  <img src="./README/Screenshot_7.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_7</img>
</div>

<div align="center">
  <img src="./README/Screenshot_8.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_8</img>
</div>

<div align="center">
  <img src="./README/Screenshot_9.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_9</img>
</div>
