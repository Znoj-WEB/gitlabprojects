### **Description**

Web app that allows registered users to sign up for and create events. The app contains several pages: Sign in, Event List, Create Event, 404 Page.

---

### **Link**

served with Firebase:  
[https://eventio.znoj.cz/](https://eventio.znoj.cz/)  
[https://strv-b782b.web.app/](https://strv-b782b.web.app/)  
[https://strv-b782b.firebaseapp.com/](https://strv-b782b.firebaseapp.com/)

serverd with Netlify:
[https://vigorous-joliot-f43805.netlify.app/](https://vigorous-joliot-f43805.netlify.app/)

served with Heroku:
[https://eventio-jz.herokuapp.com/](https://eventio-jz.herokuapp.com/)

---

### **Technologies**

TypeScript, Ant Design, React, Hooks, Redux, Firebase, GitLab CI/CD

---

### **Year**

2020

---

### **Build & Deploy**

file .env has to contain REACT_APP_API_KEY (API KEY for REST API communication) and few others values for firebase deploy:

```
REACT_APP_API_KEY = "[put your value in here]"
REACT_APP_FIREBASE_API_KEY = "[put your value in here]"
REACT_APP_FIREBASE_AUTH_DOMAIN = "[put your value in here]"
REACT_APP_FIREBASE_DATABASE_URL = "[put your value in here]"
REACT_APP_FIREBASE_PROJECT_ID = "[put your value in here]"
REACT_APP_FIREBASE_STORAGE_BUCKET = "[put your value in here]"
REACT_APP_FIREBASE_MESSAGING_SENDER_ID = "[put your value in here]"
REACT_APP_FIREBASE_APP_ID = "[put your value in here]"
```

#### **Requirements for build**

- [Node.js](https://nodejs.org/en/) 10 or newer
- (optional) [yarn](https://yarnpkg.com/), alternative: `npm`

#### **Requirements for local deploy to firebase**

- [firebase npm package](https://www.npmjs.com/package/firebase)

#### **Requirements for deploy from CI/CD in GitLab**

- variables FIREBASE_TOKEN (firebase token), FIREBASE_EVENTIO_ID (firebase project id) and all variables which are in the `.env` file

#### **Requirements for deploy from CI/CD in GitHub**

- put variables from the `.env` file into Secrets + variable FIREBASE_SERVICE_ACCOUNT

#### **Requirements for deploy with Netlify**

- in the Site settings / Build & Deploy / Environment variables  
    put REACT_APP_API_KEY variable from the `.env` file

#### **Requirements for deploy with Heroku**

- in the Settings / Config Vars  
    put REACT_APP_API_KEY variable from the `.env` file

---

### **Structure**

Project is based on `Create React App` TypeScript template.

#### **Conventions**

- 3rd party code except npm package or BLOBS should be in `assets` folder
- project specific component used in more than one page should be in folder `components`. When project will grow, more granularity will be needed - for example something could be divided by features or for example by region (group of pages)
- `HOC` should contain more logical High order components OR BETTER be replaced by custom hooks
- every page should be in its own folder inside `pages` folder. Every logical component should be separated and if component appeares in other page too, it should be moved to `components`
- everything conected directly with the application state should be inside `reducers` folder
- everything conected directly with the saga should be inside `sagas` folder
- we don't use UI Framework and build our own. Everything like that should be inside `uiComponents`. In case of using UI Framework - we should have only folder `components` with components based on components from UI Framework. So `uiComponents` is our pseudo UI Framework library, but not as general as it should be, because it is still not an UI Framework
- if you have an enum, interface, type - put it into nearest folder where these 'types' are used into file `types.ts`
- if you have a constant - put it into nearest folder into file `constants.ts`
- if you have a general help function which could be put outside component, you should put it into nearest folder into file `utils.ts`

#### **Folders / Files**

| folders      | description                                                                       |
| ------------ | --------------------------------------------------------------------------------- |
| .firebase    | folder with firebase cache                                                        |
| .github      | folder with GitHub CI/CD config files                                             |
| build        | folder with builded app                                                           |
| node_modules | folder with 3rd party packages downloaded with npm                                |
| public       | fodler with single page app config + icons                                        |
| README       | folder with files for README.md file (images, files or everything what is a BLOB) |
| src          | source code - eveything instaresting happens in here                              |
| test         | folder where jest unit tests should appear - this is huge TECHNICAL DEBT          |

| files            | description                                                                    |
| ---------------- | ------------------------------------------------------------------------------ |
| .env             | untracked file with secret variables - you have to create one                  |
|                  | as mentioned in section 'Build & Deploy'                                       |
| .firebaserc      | firebase config for deploy (`firebase.json` config file is used for the rest)  |
| .gitignore       | classic gitignore file - define what should and should not be tracked with GIT |
| .gitlab-ci.yml   | GitLab CI/CD config file                                                       |
| .prettierrc.json | prettier rules for the project                                                 |
| firebase.json    | firebase config file (`.firebaserc` config is used for deploy)                 |
| package.json     | project settings, npm packages dependencies definition, scripts definitions    |
| README.md        | this README file                                                               |
| tsconfig.json    | TypeScript config file                                                         |
| yarn.lock        | detail package definition used by yarn                                         |

#### **src**

| folders      | description                                                                      |
| ------------ | -------------------------------------------------------------------------------- |
| assets       | folder with assets (3rd party resources we use in our application)               |
| components   | reusable project specific components (other components in `uiComponents` folder) |
| HOC          | High Order Components                                                            |
| pages        | every app page has its own folder inside this folder                             |
| reducers     | reducer(s) definition(s) - keep the app state in here                            |
| sagas        | sagas definitions - keep the API handling stuff in here                          |
| uiComponents | reusable general components (other components in `components` folder)            |

| files              | description                                                                    |
| ------------------ | ------------------------------------------------------------------------------ |
| actions.ts         | all actions are defined in here. If it will grow,                              |
|                    | file could be divided into multiple files and put into folder `actions`        |
| constants.ts       | global constants used accross the aplications -                                |
|                    | - also all 'MAGIC CONSTANTS' should be in here                                 |
| index.css          | all styles are in here. This should be for sure put into                       |
|                    | folder `styles` and separated - REFACTOR IS NEEDED in here for sure            |
| index.tsx          | starting point of this application                                             |
| MainRouter.tsx     | router for the application, if application will grow, file could be divided    |
|                    | into multiple files and put into folder `routers`                              |
| react-app-env.d.ts | place where type definitions should appear - TECHNICAL DEBT                    |
| serviceWorker.ts   | service worker stuff                                                           |
| strings.ts         | all strings should appear in here for localization purposes                    |
| types.ts           | global types (interfaces, types, enums) definitions; if application will grow, |
|                    | file could be divided into multiple files and put into folder `types`          |
| utils.ts           | help functions used globally in the whole application                          |

#### **src/components**

| folders         | description                                                                  |
| --------------- | ---------------------------------------------------------------------------- |
| BasicLayout     | standard layout with Header - right component in the Header is configurable. |
|                 | If not set, Profile component is used                                        |
| LeftPanelLayout | layout with left panel - now used in case of error anonymous user            |

| files          | description                                                                           |
| -------------- | ------------------------------------------------------------------------------------- |
| LeftPanel.tsx  | Left panel (component) for LeftPanelLayout                                            |
| Logo.tsx       | Logo - redirect on click with configurable path, it has optional parameter 'mini' for |
|                | mini (mobile) version                                                                 |
| Profile.tsx    | Profile dropDown component                                                            |
| SignUpLink.tsx | Sign up link component                                                                |

#### **src/HOC**

| files                  | description                                                                           |
| ---------------------- | ------------------------------------------------------------------------------------- |
| withAuthentication.tsx | High Order Component which should be used everywhere where user has to be logged in.  |
|                        | it handles Redirect in case of log in, redirect in case of log out,                   |
|                        | redirect in case of first visit, redirect in case of repeatable visit without log out |
| withState.tsx          | High Order Component for all components, where user has to be logged in and store +   |
|                        | + store actions should be available - this coud and should be REFACTORed              |
|                        | - way how to access store from component should be IMPROVED                           |

#### **src/pages**

- this would deserve confluence page with the architecture

| folders     | description                                                                                                  |
| ----------- | ------------------------------------------------------------------------------------------------------------ |
| CreateEvent | Create event page definition, form is separated from the rest                                                |
| EventList   | event list page                                                                                              |
|             | - buttons - AttendButton component which is contains common code for buttons used for Edit/Join/Leave        |
|             | - - EditButton, JoinButton, LeaveButton components are now specific for Event List page, but probably        |
|             | - - will be used elsewhere when application grows and then will be moved to folder `src/components`          |
|             | - DashboardPanel - there is panel definition with separated components Filter and Switcher.                  |
|             | - - these components will be maybe used elsewhere in the future, than should be moved to `src/components`    |
|             | - SingleEvent - definition for single event in EventList page - it depends on eventSwitcherString parameter, |
|             | - - because component is little bit different for list view / grid view and different styles are used        |
| Page404     | error page definition                                                                                        |
| SignIn      | Sign In page definition, form is separated from the rest                                                     |

#### **src/sagas**

- critical point in here are missing unit/integration tests

| files          | description                                                                                    |
| -------------- | ---------------------------------------------------------------------------------------------- |
| index.ts       | every REST API endpoint has its own generator and use data from const.ts and utils.ts          |
| utils.ts       | help functions for the REST API communication - centralized error handling,                    |
|                | refresh Login handling in case of token timeout, response handling, compose proper header, ... |
| Profile.tsx    | Profile dropDown component                                                                     |
| SignUpLink.tsx | Sign up link component                                                                         |

#### **src/uiComponents**

- unit tests are needed in here
- these are components which I would use from UI Framework, so consider it as a really tiny UI framework for this project

| folders | description                                                                                             |
| ------- | ------------------------------------------------------------------------------------------------------- |
| Button  | - index.tsx - configurable component to have consistent buttons accros entire application.              |
|         | - - Except of the text you can define one of 9 variants and of course standard button type combinated   |
|         | - - with loading / disabled / selected capabilities and possibility to redirect after onClick.          |
|         | - - OnClick action is propagated, so you can use this component and handle onClick on your own as well. |
|         | - CloseButton.tsx - configurated button to be close button                                              |
|         | - - I suppose there will be more than one cases if application will grow.                               |
|         | - PlusButton.tsx - configurated button to be plus button                                                |
|         | - - I suppose there will be more than one cases if application will grow.                               |
|         | - Tab.tsx - configurated button to be tab button                                                        |
|         | - - it is used in TabPanel ui component                                                                 |

| files        | description                                                                                           |
| ------------ | ----------------------------------------------------------------------------------------------------- |
| DropDown.tsx | Drop down component with defined items and possibilities to have title, label                         |
|              | and one of 2 defined variants. OnSelect is propagated, so you can handle it on your own as well       |
|              | this is NOT WORKING PROPERLY nad has to be REWRITED because of                                        |
|              | limited style capabilities on select/option HTML element                                              |
| Error.tsx    | Basic Error component with nothing interesting in it. But it could interesting in the future          |
| Input.tsx    | Rich Input component. You except of name, value nad type (password / text), you can define label,     |
|              | passwordEye or errorStyle existence, disability, you can define custom error message                  |
|              | or validation object on that input - now it is supported email, notEmpty, number, futureDate,         |
|              | date, time validation. You can define onChange, onBlur or validHandler function AND/OR you can        |
|              | define 'ToState' derivates. It means that if you put update state hook function, the particular state |
|              | variable will be updated from this Input component.                                                   |
| Loading.tsx  | Full screen loading component                                                                         |
| TabPanel.tsx | Component which put multiple Button components together and create tab panel from it.                 |
|              | tabs has to be defined - these are strings to be put on buttons defined with optional                 |
|              | parameter variant (same as button variants). You can also handle onSelect on your own - parameter     |
|              | of the function would be key which is index of the button in the TabPanel.                            |

---

### **Known bugs & TODO list**

- DropDown component has to be rewrited and styled properly because of select / option HTML elements style limitations
- CSS need to refactored to prevent duplicities and to have better readability
- unit tests should be created (strating with `sagas` folder, then all `utils.ts` files, then the rest of the app)
- interfaces should be properly extracted from `pages` and `components` + some of them should be renamed
- confluence pages should be created from this README file to have proper documentation
- 'general error page' could be easily created with just changing title in Page404 - so Page404 schould be moved to `components`, interface should be extended for title property and `Page404` + new page `PageGeneralError` should have only that new component with different title param. We need to figure out when exactly this page should be shown.
- 'event detail page' - for this page there needs to be slighty extended `BasicLayout` component. In the `BasicLayout` component is `BasicLayoutHeader` component and that `BasicLayoutHeader` component should be extended to have ability to have a middle header component (now only 'rightComponent' is possible). That middle header component should be created and put into the folder `components`. That new component should be used in the same way as the 'rightComponent' in the `BasicLayoutHeader` component and should have similar functionality as the `CloseButton`.
- 'event detail page' - for this page there is a need for a new div for detail event title, there is also new component - card for Attendees. `Single Event` component should be put into `components` and extended to support both pages - `EventList` and new 'event detail page'. only change of the width is needed in here.
- 'sign in' - new page should use `LeftPanelLayout` in the same way as the `SignIn` page does, but Form will be little bit different. For `Input`` UI component there needs to be defined new validation ability - to check if the input has the same value as given. New saga generator should be defined - so new action too and reducer should be extended to track loading/data/error for the new API call.
- 'edit event' - `CreateEventForm` needs to be moved to `components` and style should be extended to support wider create form. Also there needs to be added new API call, so saga generator, actions for loading/data/error and reducer should be extended to hold these informations. Except of that new `DeleteButton` is needed and saga (actions, reducer) has to support that operation too. `CheckButton` should be new Button UI component and similar to `PlusButton`.
- 'profile' - new page with `BasicLayoutHeader`. Whole Event list is used in here with the new filter applied. The page is almost the same as the `EventList`, so `buttons` (except `JoinButton`),`SingleEvent` components should be moved to `components/Events`, `pages/EventList/utils.ts` functions should be moved to `pages/utils.ts` and new component `ProfileHeader` component should be created. New page `Profile` will be composed of `BasicLayoutHeader` new `ProfileHeader`, `buttons`, new `DashboardPanel` - similar to `pages/EventList/DashboardPanel`, but with title instead of filter and has to be filter only for users events. `SingleEvent` component will be of course also used in the same way as in `EventList`.

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### **Sign in - mobile**

<div align="center">
  <img src="./README/signIn_m.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/signIn_m_2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/signIn_m_3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/signIn_m_4.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/signIn_m_5.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/signIn_m_6.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### **Sign in - desktop**

<div align="center">
  <img src="./README/signIn_d.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/signIn_d_2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/signIn_d_3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/signIn_d_4.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/signIn_d_5.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/signIn_d_6.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### **Event list - mobile**

<div align="center">
  <img src="./README/eventList_m.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/eventList_m_2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/eventList_m_3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/eventList_m_4.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### **Event list - desktop**

<div align="center">
  <img src="./README/eventList_d.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/eventList_d_2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### **Create event - mobile**

<div align="center">
  <img src="./README/createEvent_m.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/createEvent_m_2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/createEvent_m_3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### **Create event - desktop**

<div align="center">
  <img src="./README/createEvent_d.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/createEvent_d_2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/createEvent_d_3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/createEvent_d_4.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### **Loading - mobile**

<div align="center">
  <img src="./README/error_m.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### **Loading - desktop**

<div align="center">
  <img src="./README/error_d.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### **Error - mobile**

<div align="center">
  <img src="./README/loading_m.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### **Error - desktop**

<div align="center">
  <img src="./README/loading_d.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
