### **Description**

Official web page of 2 veteran riders.

---

### **Link**

[https://kolstejn.znoj.cz/](https://kolstejn.znoj.cz/)

---

### **Technology**

HTML

---

### **Year**

2011

---

### **Screenshots**

Main:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Riders:  
<div align="center">
  <img src="./README/photos.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">photos</img>
</div>
  