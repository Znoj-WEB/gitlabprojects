### **Description**

Workout app for tracking daily exercise (now supported - PullUps, PushUps, Squats, SitUps, Burpees, Lunges, Dips, HipThrust, Kickback, Dumbbells, Walk, Run, Bike, Swim, RollerSkates, Plank, Gym, Fitness, Yoga).  
Login with Google account needed.

file .env has to contain:

REACT_APP_FIREBASE_API_KEY = ""  
REACT_APP_FIREBASE_AUTH_DOMAIN = ""  
REACT_APP_FIREBASE_DATABASE_URL = ""  
REACT_APP_FIREBASE_PROJECT_ID = ""  
REACT_APP_FIREBASE_STORAGE_BUCKET = ""  
REACT_APP_FIREBASE_MESSAGING_SENDER_ID = ""  
REACT_APP_FIREBASE_APP_ID = ""

db access rules are managed by database.rules.json file

---

### **Link**

served with Firebase:  
[https://workout.znoj.cz](https://workout.znoj.cz/)  
[https://workout-85b5b.web.app](https://workout-85b5b.web.app/)  
[https://workout-85b5b.firebaseapp.com](https://workout-85b5b.firebaseapp.com/)

---

### **Technology**

TypeScript, Ant Design, React, Hooks, Redux, Firebase, GitLab CI/CD

---

### **Year**

2020

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Home

<div align="center">
  <img src="./README/m.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Add new Exercise

- you can set category to filter Exercise (no filter in first access)
- you can choose Exercise (last used is set at beginning)
- you can choose Date (today is set at beginning)
- you can jump over days or jump to today by shortcut buttons
- you can choose time of your workout
- you can choose between setting points or pairs (pair is most recent or most common combination of Exercise and count)
- you can write points or use sliders (handy on phones)
- you can Add new Exercise with visible values or delete last one
- in the table at the bottom you can see values for chosen day

<div align="center">
  <img src="./README/m1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Add new Exercise / Delete last

- you can delete last Exercise

<div align="center">
  <img src="./README/m9.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Add new Exercise / Recent pairs

- You can add more pairs
- you can remove added pairs
- after clicking on pair, pair is added with chosen date and time

<div align="center">
  <img src="./README/d1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Review

- you can choose day / week / month / year / all
- for the chosen range you can see table of results, graph under it and at the very bottom there are listed all Exercises

<div align="center">
  <img src="./README/m2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Review / Graph

<div align="center">
  <img src="./README/m11.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Compare

- you can choose day / week / month / year / all
- for the chosen range you can compare your results with your friend (he has to give you access to see his results)

<div align="center">
  <img src="./README/m3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Statistics

- you can choose day / week / month / year / all
- In Default tab you can see results for one continues exercise
  - ... count columns - number which is everything in chosen range - so all points in Finished row, all PullUps in the 4th row, ...
  - ... Avg columns - average per day for chosen range
  - ... Min / Max for example in the following picture there is best result 30 PushUps. That means, that I did 30 PushUps tops at once in chosen period of time

<div align="center">
  <img src="./README/m4.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Pairs

- you can choose day / week / month / year / all
- for the chosen range you can see your most common or most recent exercise
- the first exercise in the table Most common is the one you did in the chosen period of time most times. It has to be same exercise and same amount of doing it
- the first exercise in the table Most recent is the one you did as the last one in the chosen period of time

<div align="center">
  <img src="./README/m5.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Users

- you can give or take away access to your Exercise data; full star means that you give access, empty one means, that access is not given
- in the column 'Have access' you can see if you have or have not access to someone data
- at the following picture you can see, that I give data to nick1 and to Daňka, but I have access only to Daňkas data.

<div align="center">
  <img src="./README/m6.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Exercises help

- there is table with all exercises in the app
- you can see how many points (column Points) per unit (column Unit) will you get for one unit of the exercise
- if you do 10 PushUps, you will get 10 points
- if you wil walk 10 kms, you will get for it 50 points

<div align="center">
  <img src="./README/m7.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Exercise help / icon

<div align="center">
  <img src="./README/d4.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Settings

- you can choose Date/Time format
- you can setup your daily goal
- you can change your Nick

<div align="center">
  <img src="./README/m8.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Side menu for mobile devices

<div align="center">
  <img src="./README/m10.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Desktop screenshots

<div align="center">
  <img src="./README/d0.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/d.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/d2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/d3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

### older version

<div align="center">
  <img src="./README/v1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
