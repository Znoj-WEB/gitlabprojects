### **Description**

Wedding Website

---

### **Link**

served with Netlify:  
[wonderful-squirrel-d4563f.netlify.app](wonderful-squirrel-d4563f.netlify.app/)  
[https://dostalovi-svatba.cz](https://dostalovi-svatba.cz/)

---

### **Technology**

TypeScript, Material-UI, React

---

### **Year**

2023

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/4.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
