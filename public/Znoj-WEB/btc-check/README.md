### **Description**

Balance from CoinMate count full in CZK.  
Data are save in JSON file and this db can be downloaded.  
Saving and getting are done through requests to php files (included in project).  
There is a table with last transactions with separate button for getting these data from CoinMate API.

file .env has to contain:  
REACT_APP_PUBLIC_KEY_CM = ""  
REACT_APP_PRIVATE_KEY_CM = ""  
REACT_APP_CLIENT_ID = ""  
REACT_APP_BITFINEX_BALANCE = ""  
REACT_APP_MARGIN = ""  
REACT_APP_ORIGIN_CZK = ""  
REACT_APP_ORIGIN_BTC = ""  
REACT_APP_HOMEPAGE = "https://\*.\*/\*"
REACT_APP_ALLOW_USERS = "\*@\*\.\*"

REACT_APP_FIREBASE_API_KEY = ""  
REACT_APP_FIREBASE_AUTH_DOMAIN = ""  
REACT_APP_FIREBASE_DATABASE_URL = ""  
REACT_APP_FIREBASE_PROJECT_ID = ""  
REACT_APP_FIREBASE_STORAGE_BUCKET = ""  
REACT_APP_FIREBASE_MESSAGING_SENDER_ID = ""  
REACT_APP_FIREBASE_APP_ID = ""  
REACT_APP_FIREBASE_MEASUREMENT_ID = ""

There is dollars input (and in env file REACT_APP_BITFINEX_BALANCE) for setting data for data outside CoinMate.  
BTC mount is increment by 0.1 due to my personal reason.  
MARGIN in env file is for proper counting, if you want to track new money outside statistic.  
ORIGIN_CZK and ORIGIN_BTC are set for statistic, because I am comparing HODL and TRADING.  
Homepage has to be set in package.json as well.  
ALLOW_USERS contains email adresses users which are allowed to see the page.

---

### **Link**

served with Firebase:  
`not public yet`

---

### **Technology**

JavaScript, PHP, Ant Design, React, Firebase, GitLab CI/CD

---

### **Year**

2019

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/mobile.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/login.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/loginFailed.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
