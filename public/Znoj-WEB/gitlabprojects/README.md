### **Description**

Web page for presenting public parts of my personal GitLab.  
Data are prepared by my other GitLab project [https://gp.znoj.cz/Znoj-WEB/gitlabprojects](https://gp.znoj.cz/Znoj-WEB/gitlabprojects) (link on GitLab: [https://gitlab.com/Znoj-Desktop/pythongitlabscript](https://gitlab.com/Znoj-Desktop/pythongitlabscript)).

CLI: build and deploy to Firebase.

---

### **Link**

served with Firebase:  
[https://gp.znoj.cz](https://gp.znoj.cz)  
[https://gp3.znoj.cz](https://gp3.znoj.cz)
[https://gitlabprojects-705b8.firebaseapp.com/](https://gitlabprojects-705b8.firebaseapp.com/)

served with Netfly:  
[https://flamboyant-albattani-bc57c7.netlify.com](https://flamboyant-albattani-bc57c7.netlify.com)  
[https://gp2.znoj.cz](https://gp2.znoj.cz)

---

### **Technology**

JavaScript, Ant Design, React, Firebase, GitLab CI/CD, CSS

---

### **Year**

2019

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/4.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/5.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/6.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/7.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/8.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/9.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
