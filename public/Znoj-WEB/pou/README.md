### **Description**

Web for school subject Computer defence and attack

---

### **Link**

[https://pou.znoj.cz/](https://pou.znoj.cz/)  

---

### **Technology**

HTML, PHP, Materialize

---

### **Year**

2017

---

### **Screenshots**

Part of znoj.cz:  
<div align="center">
  <img src="./README/master.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">master</img>
</div>
  
Main:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Files:  
<div align="center">
  <img src="./README/files.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">files</img>
</div>
  
Projects:  
<div align="center">
  <img src="./README/results.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">results</img>
</div>
  
Submission form:  
<div align="center">
  <img src="./README/send.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">send</img>
</div>
  
Failed submission:  
<div align="center">
  <img src="./README/error.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">error</img>
</div>
  
Success submission:  
<div align="center">
  <img src="./README/success.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">success</img>
</div>
  
Phone menu:  
<div align="center">
  <img src="./README/phone.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phone</img>
</div>
  
Projects table on phone:  
<div align="center">
  <img src="./README/phoneProtocols.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phoneProtocols</img>
</div>
  
Phone protocol submission:  
<div align="center">
  <img src="./README/phoneForm.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phoneForm</img>
</div>
  