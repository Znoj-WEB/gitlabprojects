### **Description**

Bookmarks for my favourite shows.

---

### **Link**

[https://bookmarks.znoj.cz/](https://bookmarks.znoj.cz/)

---

### **Technology**

HTML, PHP, Material Design Lite, GitLab CI/CD

---

### **Year**

2017

---

### **Screenshots**

Main:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Dropdown:  
<div align="center">
  <img src="./README/dropdown.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">dropdown</img>
</div>
  