### **Description**

Current scout's web page - main informative medium.

---

### **Link**

[http://junak.bludov.cz/](http://junak.bludov.cz/)

---

### **Technology**

Drupal

---

### **Year**

2013

---

### **Screenshots**

Main page with actualities:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Page with actions:  
<div align="center">
  <img src="./README/action.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">action</img>
</div>
  
Picture detail:  
<div align="center">
  <img src="./README/images.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">images</img>
</div>
  
Google calendar integration:  
<div align="center">
  <img src="./README/calendar.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">calendar</img>
</div>
  
Scout's magazine with information:  
<div align="center">
  <img src="./README/bludovit.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">bludovit</img>
</div>
  
Sections for parents:  
<div align="center">
  <img src="./README/forParents.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">forParents</img>
</div>
  
People:  
<div align="center">
  <img src="./README/leadership.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">leadership</img>
</div>
  
Videos:  
<div align="center">
  <img src="./README/videos.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">videos</img>
</div>
  
Maps:  
<div align="center">
  <img src="./README/map.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">map</img>
</div>
  
Contacts:  
<div align="center">
  <img src="./README/contacts.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">contacts</img>
</div>
  
Summaries:  
<div align="center">
  <img src="./README/summaryLetters.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">summaryLetters</img>
</div>
  