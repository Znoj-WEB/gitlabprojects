### **Description**

Fulfilled questions for ALG II

---

### **Technology**

HTML

---

### **Year**

2012

---

### **Screenshot**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
