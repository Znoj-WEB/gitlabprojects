### **Description**

Web page for my Dentist

---

### **Link**

served with Firebase:  
[https://zubar-a3f05.firebaseapp.com/](https://zubar-a3f05.firebaseapp.com/)  
[https://stomatologie-klimes.cz/](https://stomatologie-klimes.cz/)

---

### **Technology**

JavaScript, Ant Design, React, Firebase, GitLab CI/CD

---

### **Year**

2019

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/main2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/procedures.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/phone1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/phone2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/phone3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
