### **Description**

Layout of web page for Postřelmov's parish

---

### **Technology**

CSS, HTML

---

### **Year**

2014

---

### **Screenshots**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
