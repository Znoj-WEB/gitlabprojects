### **Description**

Dashboard for different versions of tested framework. Should be used on tablet and phones where framework will be tested.

---

### **Link**

[https://prfiq.znoj.cz/](https://prfiq.znoj.cz/)  
[https://prfiq.znoj.cz/62.php](https://prfiq.znoj.cz/62.php)  

---

### **Technology**

HTML, PHP, Materialize

---

### **Year**

2016

---

### **Screenshots**

Main:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Example of particular version:  
<div align="center">
  <img src="./README/dashboardExample.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">dashboardExample</img>
</div>
  