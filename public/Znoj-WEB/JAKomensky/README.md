### **Description**

Web page for my friend's school project - outdoor game about Jan Ámos Komenský.

---

### **Link**

[https://jak.znoj.cz/](https://jak.znoj.cz/)
[http://iri.8u.cz/jak/](http://iri.8u.cz/jak/)

---

### **Technology**

HTML

---

### **Year**

2014

---

### **Screenshots**

Introduction:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Map with links:  
<div align="center">
  <img src="./README/map.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">map</img>
</div>
  
Detail of one place on the map:  
<div align="center">
  <img src="./README/onePlace.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">onePlace</img>
</div>
  