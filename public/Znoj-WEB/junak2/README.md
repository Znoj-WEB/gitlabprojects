### **Description**

Scout's web page used for 4 years.

---

### **Link**

[https://junakold.znoj.cz/](https://junakold.znoj.cz/)

---

### **Technology**

HTML

---

### **Year**

2009

---

### **Screenshots**

Main page with paper infromations:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

Attendance:  
<div align="center">
  <img src="./README/attendance.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">attendance</img>
</div>

Photogallery:
<div align="center">
  <img src="./README/fotogalerie.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/fotogalerie2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
<div align="center">
  <img src="./README/fotogalerie3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
