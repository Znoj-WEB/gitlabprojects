### **Description**

Web page for creating or viewing list of wishes - for Birthdays, Christmases, Weddings... Worked on it with Dominik Ther. Design in 2015

---

### **Link**

[https://255old.znoj.cz/](https://255old.znoj.cz/)

---

### **Technology**

CSS, HTML, PHP, Materialize

---

### **Year**

2012

---

### **Screenshots**

Main page:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Main page with description:  
<div align="center">
  <img src="./README/mainDesc.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">mainDesc</img>
</div>
  
Create list of wishes 1/2:  
<div align="center">
  <img src="./README/create1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">create1</img>
</div>
  
Create list of wishes 2/2:  
<div align="center">
  <img src="./README/create2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">create2</img>
</div>
  
List of wishes 1/2:  
<div align="center">
  <img src="./README/created1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">created1</img>
</div>
  
List of wishes 2/2:
<div align="center">
  <img src="./README/created2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">created2</img>
</div>

Confirmation page:  
<div align="center">
  <img src="./README/created3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">created3</img>
</div>
  
Page for putting id and password for showing list of wishes:  
<div align="center">
  <img src="./README/read1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">read1</img>
</div>
  
List of wishes with possibility to book fulfill of particular wish or provide comment:  
<div align="center">
  <img src="./README/read2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">read2</img>
</div>
  
Phone menu:  
<div align="center">
  <img src="./README/phone.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phone</img>
</div>
  
List of wishes on phone:  
<div align="center">
  <img src="./README/phone2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phone2</img>
</div>
