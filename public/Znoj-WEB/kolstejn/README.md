### **Description**

Official web page of action Kolštejnský okruh created with Dominik Ther.

---

### **Technology**

HTML, WordPress

---

### **Year**

2013

---

### **Screenshot**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
