### **Description**

My personal web page mainly for school purposes.

---

### **Link**

[https://znoj16.znoj.cz/](https://znoj16.znoj.cz/)

---

### **Technology**

HTML, PHP, Materialize

---

### **Year**

2016

---

### **Screenshot**

Example of school subject:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
School section:  
<div align="center">
  <img src="./README/school.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">school</img>
</div>
  
My files picked up automatically from one folder:  
<div align="center">
  <img src="./README/myFiles.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">myFiles</img>
</div>
  
My web pages collection:  
<div align="center">
  <img src="./README/myWebPages.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">myWebPages</img>
</div>
  
Phone menu:  
<div align="center">
  <img src="./README/phone.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phone</img>
</div>
  