### **Description**

Web for school subject Computer viruses and security of computer systems

---

### **Link**

[https://pvbps.znoj.cz/](https://pvbps.znoj.cz/)

---

### **Technology**

HTML, PHP, Materialize

---

### **Year**

2017

---

### **Screenshots**

Part of znoj.cz:  
<div align="center">
  <img src="./README/master.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">master</img>
</div>
  
Main:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Files:  
<div align="center">
  <img src="./README/files.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">files</img>
</div>
  
Projects and tasks:  
<div align="center">
  <img src="./README/protocols.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">protocols</img>
</div>
  
Form for submission:  
<div align="center">
  <img src="./README/uploadForm.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">uploadForm</img>
</div>
  
Submission failed:  
<div align="center">
  <img src="./README/error.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">error</img>
</div>
  
Submission successes:  
<div align="center">
  <img src="./README/success.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">success</img>
</div>
  
Phone menu:  
<div align="center">
  <img src="./README/phone.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phone</img>
</div>
  
Files:  
<div align="center">
  <img src="./README/phoneFiles.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phoneFiles</img>
</div>
  
Phone protocols:  
<div align="center">
  <img src="./README/phoneProtocols.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phoneProtocols</img>
</div>
  
Phone form for submission:  
<div align="center">
  <img src="./README/phoneForm.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phoneForm</img>
</div>
  
Phone submission:  
<div align="center">
  <img src="./README/phoneProtocolError.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">phoneProtocolError</img>
</div>
  