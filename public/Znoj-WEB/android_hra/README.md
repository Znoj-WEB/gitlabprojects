### **Description**

Page for my android game SOKOBAN.

---

### **Link**

[http://game.znoj.cz/](http://game.znoj.cz/)
[http://iri.8u.cz/](http://iri.8u.cz/)

---

### **Technology**

HTML

---

### **Year**

2013

---

### **Screenshots**

Main page:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Screenshots of the game:  
<div align="center">
  <img src="./README/pictures1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">pictures1</img>
</div>
  
Screenshot detail:  
<div align="center">
  <img src="./README/pictures2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">pictures2</img>
</div>
  
Video from the game:  
<div align="center">
  <img src="./README/video.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">video</img>
</div>
  
Description of the game:  
<div align="center">
  <img src="./README/desc.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">desc</img>
</div>
  
Versions and download the game:  
<div align="center">
  <img src="./README/download.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">download</img>
</div>
  
Contact form:  
<div align="center">
  <img src="./README/contact.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">contact</img>
</div>
  