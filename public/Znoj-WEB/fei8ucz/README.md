### **Description**

Files and pages created during my school days for me or for other students created with Dominik Ther

---

### **Link**

[https://fei.znoj.cz/](https://fei.znoj.cz/)  
[http://fei.8u.cz/](http://fei.8u.cz/)  

---

### **Technology**

CSS, PHP

---

### **Year**

2012

---

### **Screenshots**

Main page:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
Content inside folder:  
<div align="center">
  <img src="./README/files.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">files</img>
</div>
  
Example of page placed in fei8u.cz:  
<div align="center">
  <img src="./README/page.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">page</img>
</div>
  