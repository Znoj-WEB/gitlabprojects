### **Description**

Web page for horse riders.

---

### **Link**

[https://kone.znoj.cz/](https://kone.znoj.cz/)

---

### **Technology**

HTML

---

### **Year**

2010

---

### **Screenshot**

<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
