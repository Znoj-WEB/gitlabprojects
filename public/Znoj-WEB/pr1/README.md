### **Description**

Web for school subject Programming I.

---

### **Link**

[https://pr1.znoj.cz/](https://pr1.znoj.cz/)  
[https://pr1.znoj.cz/sem.php](https://pr1.znoj.cz/sem.php)  

---

### **Technology**

HTML, PHP, Materialize

---

### **Year**

2016

---

### **Screenshots**

Part of znoj.cz:  
<div align="center">
  <img src="./README/master.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">master</img>
</div>
  
Description of using tests data:  
<div align="center">
  <img src="./README/main.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
School tasks:  
<div align="center">
  <img src="./README/schoolExercises.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">schoolExercises</img>
</div>
  
My tests data for school tasks:  
<div align="center">
  <img src="./README/results.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">results</img>
</div>
  
Homeworks:
<div align="center">
  <img src="./README/homeExercises.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">homeExercises</img>
</div>
  
Useful functions:  
<div align="center">
  <img src="./README/helpers.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">helpers</img>
</div>
  
Semestral project description:  
<div align="center">
  <img src="./README/semestralWork.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">semestralWork</img>
</div>
  