### **Description**

Android game sokoban created as a school project

---

### **Technology**

Android

---

### **Year**

2013

---

### **Screenshots**

<div align="center">
  <img src="./README/03.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">03</img>
</div>

<div align="center">
  <img src="./README/01.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">01</img>
</div>

<div align="center">
  <img src="./README/02.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">02</img>
</div>

<div align="center">
  <img src="./README/kod4.jpg" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">kod4</img>
</div>

<div align="center">
  <img src="./README/004.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">004</img>
</div>

<div align="center">
  <img src="./README/014.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">014</img>
</div>

<div align="center">
  <img src="./README/015.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">015</img>
</div>

<div align="center">
  <img src="./README/016.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">016</img>
</div>
