### **Description**

android app for counting difference between 2 dates - one is current, one is in the future.  
Output is in various time units.

---

### **Technology**

Android

---

### **Year**

2015

---

### **Screenshot**

<div align="center">
  <img src="./README/Screenshot_2018-08-12-20-11-04-093_com.example.iri.as.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-12-20-11-11-665_com.example.iri.as.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-12-20-11-04-093_com.example.iri.as.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
