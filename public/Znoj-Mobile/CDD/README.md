### **Description**

concept of android app for fictive company DJ in the Box.
More about whole project could be found in folder *docs*.

---

### **Technology**

Android

---

### **Year**

2015

---

### **Screenshot**

Black theme:  
<div align="center">
  <img src="./README/screen0_postcard.jpg" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

White theme:  
<div align="center">
  <img src="./README/screen_white_postcard.jpg" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

tablet:  
<div align="center">
  <img src="./README/screen4.jpg" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
