### **Description**

Android app running on background, periodically checks battery stats and send it to local server. Interval is configurable.
This app was used to controll battery state on various android devices in company profiq, so maintenence was easier because of this app.
App survive device restart.

---

### **Technology**

Android

---

### **Year**

2015

---

### **Screenshot**

profiq fork  
<div align="center">
  <img src="./README/Screenshot_2018-08-11-23-17-11-050_com.example.iri.batterystats2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-11-23-18-55-576_com.android.settings.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

old test version  
<div align="center">
  <img src="./README/oldForkScreen.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
