- [**Description**](#description)
- [**Technology**](#technology)
- [**Year**](#year)
- [**Build**](#build)
  - [**Build from scratch**](#build-from-scratch)
  - [**Prebuilded solution**](#prebuilded-solution)
- [**Screenshot**](#screenshot)
- [**Doc**](#doc)

Getting Started: Building for Android:
    - <https://trac.pjsip.org/repos/wiki/Getting-Started/Android>
    - <https://trac.pjsip.org/repos/wiki/Getting-Started/Android#create-own-app>

My experience and setup:
    - <https://docs.google.com/document/d/1w-NtQnQKGMFc0Ek6W0wR49uZ0zjTpLkKdZfCkoxg6Xs/edit?usp=sharing>

### **Description**

Build, configuration, improvement PJSIP library in android for custom implementation
of VoIP communication. Part of for TACR (Technology Agency of the Czech Republic) project

---

### **Technology**

Android

---

### **Year**

2018

---

### **Build**

#### **Build from scratch**

<https://trac.pjsip.org/repos/wiki/Getting-Started/Android#Tryingoursampleapplicationandcreatingyourown>

I am using Cygdrive

```
Iri@JZ /cygdrive/d/Skola/VSB/SIP/pjproject-master
$ cd pjsip-apps/src/swig
```

Download android-ndk for linux

```
export ANDROID_NDK_ROOT=../linux/android-ndk-r18/
cp configure-android configure-android.bak
sed -i 's/\r$//' configure-android
./configure-android
```

**IT FAILED**
_many errors reported, no suitable solution found

trying it with Windows:
set ANDROID_NDK_ROOT=D:\Android\ndk-bundle  
(if you did bak file, restore it now)  
probably not possible too  

**Conclusion**
 **need Linux for that, didn’t make it work with Windows**

#### **Prebuilded solution**

I am using Vagrant and this builder: <https://github.com/VoiSmart/pjsip-android-builder>

```
git clone https://github.com/gotev/pjsip-android-builder  
cd pjsip-android-builder  
vagrant up  
```

<div align="center">
  <img src="./README/1.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
  
End with error for me - solved with commented part

```
  # config.vm.provision "shell", inline: <<-SHELL
  #  cd /pjsip-android-builder
  #  ./prepare-build-system
  #SHELL
```

in Vagantfile  

Then I needed to install everything in system this way:  

```
sudo apt-get install -y git
git clone https://github.com/alexbbb/pjsip-android-builder
cd pjsip-android-builder
./prepare-build-system
```

build doesn’t work due to missing swig - see error in log. Probably due to
non-working url or something. Will fix it with installing swig:  

```
sudo apt-get install swig
```

then run

```
./build
```

<div align="center">
  <img src="./README/2.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

```
./configure
make dep && make clean && make
```

copy files from Valgrid  

```
scp -r -P 2200 vagrant@127.0.0.1:/pjsip-android-builder ./buildFromValgrid
```

**newest NDK 17 is not working. Using NDK 13b**  

```
curl https://dl.google.com/android/repository/android-ndk-r13b-linux-x86_64.zip --output android-ndk-r13b-linux-x86_64.zip

unzip android-ndk-r13b-linux-x86_64.zip ndk-r13b

export ANDROID_NDK_ROOT=/home/vagrant/android-ndk-r13b

curl https://codeload.github.com/pjsip/pjproject/zip/2.8 --output 2_8.zip

unzip 2_8.zip

cd pjproject-2.8/pjlib/include/pj/

nano config_site.h
**#define PJ_CONFIG_ANDROID 1
#include <pj/config_site_sample.h>**

./configure-android
```

<div align="center">
  <img src="./README/3.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

```
make dep && make clean && make
```

<div align="center">
  <img src="./README/4.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

```
cd pjsip-apps/src/swig/
make
```

<div align="center">
  <img src="./README/5.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

```
sudo apt-get install p7zip-full
7z a pjproject-2.8.zip pjproject-2.8/*
scp -P 2200 vagrant@127.0.0.1:~/pjproject-2.8.zip ./build_from_v_2.zip
```

download: <https://gitlab.com/Znoj-Mobile/pjproject-2.8/tree/master>  
import to Android Studio: <https://gitlab.com/Znoj-Mobile/pjproject-2.8/tree/master/pjsip-apps/src/swig/java/android>  

<div align="center">
  <img src="./README/6.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

---

### **Screenshot**

<div align="center">
  <img src="./README/Screenshot_16-290_org.pjsip.pjsua2.app.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_26-954_org.pjsip.pjsua2.app.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_33-945_org.pjsip.pjsua2.app.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_52-504_org.pjsip.pjsua2.app.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

---

### **Doc**  

<div align="center">
  <img src="./README/VoIP%20-%20pjsip.pdf" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">VoIP - pjsip.pdf</img>
</div>
