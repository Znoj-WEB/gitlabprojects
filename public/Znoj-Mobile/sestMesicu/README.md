### **Description**

Android game created for my girlfriend. It was part of a puzzle and after each level there was clue - key to specific part of the puzzle.

---

### **Technology**

Android

---

### **Year**

2015

---

### **Screenshots**

fullHD screenshots in folder *fullHD_screenshots*  

#### Game

<div align="center">
  <img src="./README/Screenshot_2018-08-12-20-15-51-477_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-12-20-15-51-477_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-12-20-15-56-716_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-12-20-15-56-716_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-12-20-16-00-054_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-12-20-16-00-054_com.example.iri.sest</img>
</div>

#### Menu

<div align="center">
  <img src="./README/Screenshot_2018-08-12-20-13-57-742_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-12-20-15-12-936_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-12-20-17-52-515_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-12-20-14-34-015_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Level rewards examples

<div align="center">
  <img src="./README/Screenshot_2018-08-12-20-17-37-829_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-21-35-113_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

#### Levels

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-17-433_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-17-433_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-21-116_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-21-116_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-24-566_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-24-566_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-27-840_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-27-840_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-30-995_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-30-995_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-34-098_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-34-098_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-37-439_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-37-439_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-40-587_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-40-587_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-43-677_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-43-677_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-46-983_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-46-983_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-50-108_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-50-108_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-53-362_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-53-362_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-56-552_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-56-552_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-19-59-917_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-19-59-917_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-20-02-860_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-20-02-860_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-20-06-189_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-20-06-189_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-20-09-319_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-20-09-319_com.example.iri.sest</img>
</div>

<div align="center">
  <img src="./README/Screenshot_2018-08-13-20-20-12-638_com.example.iri.sest.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;">Screenshot_2018-08-13-20-20-12-638_com.example.iri.sest</img>
</div>
