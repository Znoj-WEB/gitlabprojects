### **Description**

Android app implemented to break md5 hashes using brute force attack implemented in C++

---

### **Technology**

Android, NDK

---

### **Year**

2015

---

### **Screenshot**

<div align="center">
  <img src="./README/Screenshot_2015-05-06-13-56-40.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/Screenshot_2015-05-06-13-58-19.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
