### **Description**

App listing partitions with used and free space in android devices

---

### **Technology**

Android

---

### **Year**

2015

---

### **Screenshot**

<div align="center">
  <img src="./README/Screenshot_2018-08-12-18-01-15-157_com.example.iri.partitions.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
