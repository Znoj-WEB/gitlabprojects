### **Description**

Concept of an app for VKH Ostrava displaying the same content as the web page.

---

### **Technology**

Android

---

### **Year**

2015

---

### **Screenshots**

<div align="center">
  <img src="./README/vkhApp.jpg" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>

<div align="center">
  <img src="./README/androidStudioScreen.png" align="center" style="max-width:956px; max-height:600px; margin:5px 0 10px 0;"></img>
</div>
